import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { MomentModule } from 'angular2-moment';

//Main Component
import { MyApp } from './app.component';

//Pages
//Auth Pages
import { LoginPage } from '../pages/account/login/login';
import { ForgotPasswordPage } from '../pages/account/forgot-password/forgot-password';
import { ResetPasswordPage } from '../pages/account/reset-password/reset-password';
import { SignupPage } from '../pages/account/signup/signup';
import { AccountVerificationPage } from '../pages/account/account-verification/account-verification';
import { NewVerificationTokenPage } from '../pages/account/new-verification-token/new-verification-token';
import { WelcomePage } from '../pages/account/welcome/welcome';
//Home Page
import { HomePage } from '../pages/home/home';
//Interact Pages
import { InteractionsPage } from '../pages/interact/interactions/interactions';
import { InteractionMessagesPage } from '../pages/interact/interaction-messages/interaction-messages';
import { ImageModalPage } from '../pages/interact/interaction-messages/image-modal/image-modal';
import { NewInteractionMessagePage } from '../pages/interact/new-interaction-message/new-interaction-message';
import { MessageFormsModal } from '../pages/interact/new-interaction-message/message-forms/message-forms';
//Photos Pages
import { AlbumsPage } from '../pages/photos/albums/albums'
import { GalleryPage } from '../pages/photos/gallery/gallery'
import { SliderPage } from '../pages/photos/gallery/slider/slider'
//Booking Pages
import { SelectServicePage  } from '../pages/booking/book/select-service/select-service'
import { SelectResourceModalPage  } from '../pages/booking/book/search-availability/select-resource-modal/select-resource-modal'
import { SelectQuantityModalPage  } from '../pages/booking/book/search-availability/select-quantity-modal/select-quantity-modal'
import { SearchAvailabilityPage } from '../pages/booking/book/search-availability/search-availability'
import { ConfirmBookingPage  } from '../pages/booking/book/confirm-booking/confirm-booking'
import { ListBookingPage  } from '../pages/booking/manage-booking/list-booking/list-booking'
import { ViewBookingDetailsPage  } from '../pages/booking/manage-booking/view-booking-details/view-booking-details'
//Static Pages
import { FAQPage } from '../pages/faq/faq'
import { LocationPage } from '../pages/location/location'
import { InfoPage } from '../pages/info/info'
import { ListPage } from '../pages/list/list'

//Services
//App related, starting with autogenerted angular sdk for loopback
import { SDKBrowserModule } from '../shared/lbsdk/index';
//App related services
import { DeviceInfoCollector, ErrorToaster, PushHandler, NetworkCheck,
  StatusBarSettings, SpinnerWrapper, AppConfigInfo, InteractService, StaticSectionService,
  BookingService, GalleryService, CameraService, FileService, AppNavService } from '../shared/services';
  import { Network } from '@ionic-native/network';
  import { SplashScreen } from '@ionic-native/splash-screen';
  import { Camera } from '@ionic-native/camera';
  import { Push } from '@ionic-native/push';
  import { Device } from '@ionic-native/device';
  import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
  import { File } from '@ionic-native/file';
  import { SocialSharing } from '@ionic-native/social-sharing';
  import { StatusBar } from '@ionic-native/status-bar';
  import { LaunchNavigator } from '@ionic-native/launch-navigator';

  //Config
  import { ClientConfig, ThemeColorConfig } from '../shared/config'

  //Directives
  import { EqualValidator } from '../shared/directives/equal-validator.directive';

  //Pipes
  import { SafeHtmlPipe } from '../shared/pipes/safeHtml.pipe';

  //Translate
  import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
  import { TranslateHttpLoader } from '@ngx-translate/http-loader';


// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

/**
 * The Pages array lists all of the pages we want to use in our app.
 * We then take these pages and inject them into our NgModule so Angular
 * can find them. As you add and remove pages, make sure to keep this list up to date.
 */
let pages = [
  MyApp,
  LoginPage,
  ForgotPasswordPage,
  ResetPasswordPage,
  AccountVerificationPage,
  NewVerificationTokenPage,
  SignupPage,
  WelcomePage,
  HomePage,
  InteractionsPage,
  InteractionMessagesPage,
  ImageModalPage,
  NewInteractionMessagePage,
  MessageFormsModal,
  AlbumsPage,
  GalleryPage,
  SliderPage,
  SelectServicePage,
  SelectResourceModalPage,
  SelectQuantityModalPage,
  SearchAvailabilityPage,
  ConfirmBookingPage,
  ListBookingPage,
  ViewBookingDetailsPage,
  FAQPage,
  LocationPage,
  InfoPage,
  ListPage
];

//similarly add directives and pipes here
let directives = [
  EqualValidator
];
let pipes = [
  SafeHtmlPipe
];

export function declarations() {
  let declarations = [];
  //merge pages, pipes and directives and return as declarations
  for (let page of pages) {
    declarations.push(page);
  }
  for (let directive of directives) {
    declarations.push(directive);
  }
  for (let pipe of pipes) {
    declarations.push(pipe);
  }
  return declarations;
}

export function entryComponents() {
  return pages;
}

export function providers() {
  return [
    Network, SplashScreen, Push, Device, FileTransfer, FileTransferObject, File, SocialSharing,
    StatusBar, LaunchNavigator, Camera, ClientConfig, ThemeColorConfig,
    DeviceInfoCollector, NetworkCheck, ErrorToaster, PushHandler,
    StatusBarSettings, SpinnerWrapper, AppConfigInfo, InteractService, StaticSectionService,
    BookingService, GalleryService, CameraService, FileService, AppNavService,
    // Keep this to enable Ionic's runtime error handling during development
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ];
}

@NgModule({
  declarations: declarations(),
  imports: [
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    SDKBrowserModule.forRoot(),
    MomentModule
  ],
  bootstrap: [IonicApp],
  entryComponents: entryComponents(),
  providers: providers()
})
export class AppModule { }
