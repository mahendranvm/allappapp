import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';

import { WelcomePage } from '../pages/account/welcome/welcome';
import { LoginPage } from '../pages/account/login/login';
import { HomePage } from '../pages/home/home';
import { InteractionsPage  } from '../pages/interact/interactions/interactions'
import { GalleryPage  } from '../pages/photos/gallery/gallery'
import { AlbumsPage  } from '../pages/photos/albums/albums'
import { SelectServicePage  } from '../pages/booking/book/select-service/select-service'
import { SearchAvailabilityPage  } from '../pages/booking/book/search-availability/search-availability'
import { ListBookingPage  } from '../pages/booking/manage-booking/list-booking/list-booking'
import { ViewBookingDetailsPage } from '../pages/booking/manage-booking/view-booking-details/view-booking-details'
import { FAQPage } from '../pages/faq/faq'
import { LocationPage } from '../pages/location/location'
import { InfoPage } from '../pages/info/info'
import { ListPage } from '../pages/list/list'

import { TranslateService } from '@ngx-translate/core';
import { LoopBackAuth, AppuserApi } from '../shared/lbsdk';
import { PushHandler, ErrorToaster, NetworkCheck, StatusBarSettings, AppConfigInfo, AppNavService, SpinnerWrapper } from '../shared/services';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;

  @ViewChild(Nav) navCtrl: Nav;

  constructor(translate: TranslateService,
              platform: Platform,
              networkCheck: NetworkCheck,
              private splashScreen: SplashScreen,
              private pushHandler: PushHandler,
              private errorToaster: ErrorToaster,
              statusBarSettings: StatusBarSettings,
              loopBackAuth: LoopBackAuth,
              private appConfig : AppConfigInfo,
              private appNavService: AppNavService,
              private spinnerWrapper: SpinnerWrapper,
              private appuserApi: AppuserApi) {

                // Set the default language for translation strings, and the current language.
                translate.setDefaultLang('en');
                translate.use('en')

                // Okay, so the platform is ready and our plugins are available.
                platform.ready().then(() => {
                  //pass page references to push handler
                  this.passPageReferenceToService();

                  //get app config info
                  this.appConfig.loadAppSettings().subscribe ((resp)=>{
                    //set root page
                    if (loopBackAuth.getAccessTokenId() && loopBackAuth.getCurrentUserId()) this.rootPage = HomePage;
                    else this.rootPage = WelcomePage;
                    //Set StatusBar color
                    statusBarSettings.setStatusBarColor();
                    //hide splashscreen
                    this.splashScreen.hide();
                    //Network check
                    networkCheck.checkForNetwork(true);
                    //Initialize push
                    this.pushHandler.initialize();
                  }, (err)=> {
                    this.rootPage = WelcomePage; //TODO: should this be an error page with an error message, retry button and a auto-retry counter?
                    this.appConfig.appAllSections = [];
                    this.appConfig.appInfoLoadErr = true;
                    this.splashScreen.hide();
                  })
                });
  }

  logout () {
    this.appuserApi.logout().subscribe(()=>{},()=>{});
    //go to login page
    this.navCtrl.setRoot(WelcomePage)
  }

  openPage (section) {
    this.spinnerWrapper.show();
    if (section.module=="Interact" || section.module=="Push") this.appNavService.openSection ({InteractionsPage: InteractionsPage}, section, this.navCtrl, null);
    else if (section.module=="Photos") this.appNavService.openSection ({GalleryPage: GalleryPage, AlbumsPage: AlbumsPage}, section, this.navCtrl, null);
    else if (section.module=="Booking") this.appNavService.openSection ({SelectServicePage: SelectServicePage, SearchAvailabilityPage: SearchAvailabilityPage}, section, this.navCtrl, null);
    else if (section.module=="Manage Booking") this.appNavService.openSection ({ListBookingPage: ListBookingPage, ViewBookingDetailsPage: ViewBookingDetailsPage}, section, this.navCtrl, null);
    else if (section.module=="FAQ") this.appNavService.openSection({FAQPage: FAQPage}, section, this.navCtrl, null);
    else if (section.module=="Location") this.appNavService.openSection({LocationPage: LocationPage}, section, this.navCtrl, null);
    else if (section.module=="Info") this.appNavService.openSection({InfoPage: InfoPage}, section, this.navCtrl, null);
    else if (section.module=="List") this.appNavService.openSection({ListPage: ListPage}, section, this.navCtrl, null);
  }

  passPageReferenceToService () {

    this.errorToaster.loginReference = LoginPage;

    this.appConfig.loginReference = LoginPage;
    this.appConfig.navCtrl = this.navCtrl;

    this.pushHandler.page.HomePage = HomePage;
    this.pushHandler.page.InteractionsPage = InteractionsPage;
    this.pushHandler.page.GalleryPage = GalleryPage;
    this.pushHandler.page.AlbumsPage = AlbumsPage;
    this.pushHandler.page.SelectServicePage = SelectServicePage;
    this.pushHandler.page.SearchAvailabilityPage = SearchAvailabilityPage;
    this.pushHandler.page.ListBookingPage = ListBookingPage;
    this.pushHandler.page.ViewBookingDetailsPage = ViewBookingDetailsPage;
    this.pushHandler.page.ListBookingPage = ListBookingPage;
    this.pushHandler.page.FAQPage = FAQPage;
    this.pushHandler.page.LocationPage = LocationPage;
    this.pushHandler.page.InfoPage = InfoPage;
    this.pushHandler.page.ListPage = ListPage;
    this.pushHandler.navCtrl = this.navCtrl;
  }

}
