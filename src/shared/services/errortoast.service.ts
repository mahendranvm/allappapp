import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';
import { ToastController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

/**
 * This service has functions to display a toast with error message - to be used during all server calls
 *
 */
 @Injectable()
export class ErrorToaster {
  // Our translated text strings
  private networkErrorString: string;
  private unknownErrorString: string;
  private auth401ErrorString: string;

  //handle to login page (to be used for forwarding to login on 401 error)
  public loginReference: any = null;

  //constructor
  constructor(private toastCtrl: ToastController,
              private network: Network,
              private translateService: TranslateService) {
    this.translateService.use('en');
    this.translateService.get(['NO_INTERNET', 'UNKNOWN_ERROR', 'AUTH_401_ERROR']).subscribe((value) => {
      this.networkErrorString = value['NO_INTERNET'];
      this.unknownErrorString = value['UNKNOWN_ERROR'];
      this.auth401ErrorString = value ['AUTH_401_ERROR']
    });
  }

  public toastError (err: any, inputErrorString: string = this.unknownErrorString, navCtrl?: any): void {
    let toastMessage: string;
    //check for custom error message
    if (this.network.type && this.network.type === 'none') {
      toastMessage = this.networkErrorString;
    } else if (err && err.statusCode == 401 && this.loginReference && navCtrl) {
      toastMessage = this.auth401ErrorString;
    } else if (err && err.details && err.details.appErrMsg) {
      //if not check for custom error message
      toastMessage = err.details.appErrMsg;
    } else if (inputErrorString && inputErrorString != ''){
      //else use the default Error string for toast message
      toastMessage = inputErrorString;
    }
    // Display Error Message
    let toast = this.toastCtrl.create({
      message: toastMessage,
      duration: 4000,
      position: 'top'
    });
    toast.present();
    //forward to login page on 401 error
    if (err && err.statusCode == 401 && this.loginReference && navCtrl) {
      navCtrl.push(this.loginReference, {popBack: true});
    }
  }

}
