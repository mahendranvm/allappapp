import { Injectable } from '@angular/core';
import { AppuserApi, LoopBackAuth } from '../../shared/lbsdk/services';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/timer';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import { FileService, AppConfigInfo } from '../services'



/**
 * Interact Service
 * Bookmark based refresh and infinite scroll. Interactions use last message date as bookmark and messages use createdon.
 * Load More will return results in DESC order such that it can appended to existing list and refresh will return results in ASC order to be prepended
 * Check to remove duplicate entry from current list will be performed before appending or prepending the arrays.
 * An observable timer will be used to refresh periodically
 */
@Injectable()
export class InteractService {


  constructor (private appuserApi: AppuserApi,
               private loopbackAuth: LoopBackAuth,
               private fileService: FileService,
               private appConfigInfo: AppConfigInfo) {

  }

  public getStandardMessageFormFields (): Array<any> {
    return [
        {fieldname: "Message", fieldtype: "String", fieldrequired: false, validvalues: []},
        {fieldname: "Image", fieldtype: "Image", fieldrequired: false, validvalues: []}
      ];
  }

  public createNewInteraction (simpleMessageInd: boolean, sectionid:string, title: string, formfields: any) {
    //check if simple message
    if (simpleMessageInd) {
      //simple message will always have a string field and optional image field, so add it to messagefields
      //we use this separate variable "messagefields" since firstmessageform is a copy of standard message template that will have a placeholder for image
      let messagefields: Array<any> = [formfields[0]];
      let fullfilekey: string = null;
      //check if image exists
      if (formfields[1].fileurl) {
        //if there is an image, get putObject URL, upload image then create interaction
        //create file name = timestamp + random number
        formfields[1].filename = (new Date()).getTime() + (Math.floor(1000000 + Math.random() * 9000000)).toString() +".jpg";
        //form fullfilekey
        fullfilekey = this.appConfigInfo.realm + "/" + sectionid + "/" + this.loopbackAuth.getCurrentUserId() + "/" + formfields[1].filename;
        //get putobject signedurl from lb server
        return this.appuserApi.getPutSignedUrlForInteractionMessage(this.loopbackAuth.getCurrentUserId(), null, sectionid, null, fullfilekey, "image/jpeg")
        //Upload image to amazon using the signedurl in success
        .flatMap((resp)=>{ return this.fileService.uploadFileToS3(resp.success, formfields[1].fileurl, formfields[1].filename, "image/jpeg")})
        //on success, create interaction
        .flatMap((resp)=>{
          let imageField = this.getStandardMessageFormFields()[1];
          imageField.fieldvalue = fullfilekey;
          messagefields.push(imageField);
          return this.appuserApi.createInteraction(this.loopbackAuth.getCurrentUserId(), null, sectionid, title, false, null, messagefields)
          .catch((error) => {
            //on failure to create interaction, delete image uploaded to S3
            this.appuserApi.removePics (this.loopbackAuth.getCurrentUserId(), null, sectionid, [{Key: fullfilekey}]).subscribe((resp) => {}, (err) => {});
            return Observable.throw(error);
          });
         })
       } else {
        //if image does not exist, call server
        return this.appuserApi.createInteraction(this.loopbackAuth.getCurrentUserId(), null,
         sectionid, title, false, null, messagefields);
       }
     } else {
      //if this is a form based message, call server directly with given values
      //note - difference from createInteraction calls above is the value for message parameter
      //here message parameter is same as formfields, where as above we recreated depending on if image is present or not
      return this.appuserApi.createInteraction(this.loopbackAuth.getCurrentUserId(), null,
       sectionid, title, false, null, formfields);
     }
  }

  public createNewInteractionMessage (simpleMessageInd: boolean, sectionid:string, interactionid: string, formfields: any) {
    //check if simple message
    if (simpleMessageInd) {
      //simple message will always have a string field and optional image field, so add it to messagefields
      //we use this separate variable "messagefields" since firstmessageform is a copy of standard message template that will have a placeholder for image
      let messagefields: Array<any> = [formfields[0]];
      let fullfilekey: string = null;
      //check if image exists
      if (formfields[1].fileurl) {
        //if there is an image, get putObject URL, upload image then create interaction
        //create file name = timestamp + random number
        formfields[1].filename = (new Date()).getTime() + (Math.floor(1000000 + Math.random() * 9000000)).toString() +".jpg";
        //form fullfilekey
        fullfilekey = this.appConfigInfo.realm + "/" + sectionid + "/" + interactionid + "/" + this.loopbackAuth.getCurrentUserId() + "/" + formfields[1].filename;
        //get putobject signedurl from lb server
        return this.appuserApi.getPutSignedUrlForInteractionMessage(this.loopbackAuth.getCurrentUserId(), null, sectionid, interactionid, fullfilekey, "image/jpeg")
        //Upload image to amazon using the signedurl in success
        .flatMap((resp)=>{ return this.fileService.uploadFileToS3(resp.success, formfields[1].fileurl, formfields[1].filename, "image/jpeg")})
        //on success, create interaction
        .flatMap((resp)=>{
          let imageField = this.getStandardMessageFormFields()[1];
          imageField.fieldvalue = fullfilekey;
          messagefields.push(imageField);
          return this.appuserApi.createInteractionMessage(this.loopbackAuth.getCurrentUserId(), null, sectionid, interactionid, messagefields)
          .catch((error) => {
            //on failure to create interaction, delete image uploaded to S3
            this.appuserApi.removePics (this.loopbackAuth.getCurrentUserId(), null, sectionid, [{Key: fullfilekey}]).subscribe((resp) => {}, (err) => {});
            return Observable.throw(error);
          });
         })
      } else {
        //if image does not exist, call server
        return this.appuserApi.createInteractionMessage (this.loopbackAuth.getCurrentUserId(), null,
         sectionid, interactionid, messagefields);
      }
    } else {
      //if this is a form based message, call server directly with given values
      //note - difference from createInteraction calls above is the value for message parameter
      //here message parameter is same as formfields, where as above we recreated depending on if image is present or not
      return this.appuserApi.createInteractionMessage(this.loopbackAuth.getCurrentUserId(), null,
       sectionid, interactionid, formfields);
    }
  }

  public loadMoreInteractionMesages (interaction: any, messages: Array<any>) {
    //let bookmarkdate be the createdon of the last message - returned messages will be ordered by createdon in DESC order
    let bookmarkdate = Date.now();
    if (messages.length > 0) {
      bookmarkdate = messages[messages.length-1].createdon;
    }
    //contact server
    return this.appuserApi.getInteractionMessagesV2(this.loopbackAuth.getCurrentUserId(), null, interaction.sectionid,  interaction.id, 'Load More', bookmarkdate).map((resp) => {
      //note - bookmarkdate interaction will always be returned
      //it is possible to have 2 interactions with same lastmessagedate if both messages are created at same time
      //in this case, inifinite scroll will never be complete as returned result will always have 2 recrods. but this is non critical and chances are minimal.
      if (resp.success && resp.success.interactionmessages && resp.success.interactionmessages.length) this.appendToArray(messages, resp.success.interactionmessages);
      return resp;
    });
  }

  public refreshInteractionMesages (interaction: any, messages: Array<any>) {
    //let bookmarkdate be the createdon of the first message - returned messages will be ordered by createdon in ASC order
    let bookmarkdate = '1/1/2016';
    if (messages.length > 0) {
      bookmarkdate = messages[0].createdon;
    }
    //contact server
    return this.appuserApi.getInteractionMessagesV2(this.loopbackAuth.getCurrentUserId(), null, interaction.sectionid, interaction.id, 'Refresh', bookmarkdate).flatMap((resp) => {
      if (resp.success && resp.success.interactionmessages && resp.success.limit
        && resp.success.interactionmessages.length >= resp.success.limit) {
          //since we are splicing overlaps in messages, doing it after checking if recursion is required than before.
          this.spliceOverlap(messages, resp.success.interactionmessages);
          this.prependToArray(messages, resp.success.interactionmessages);
          return this.refreshInteractionMesages (interaction, messages);
        }
      else {
        //since we are splicing overlaps in messages, doing it after checking if recursion is required than before.
        this.spliceOverlap(messages, resp.success.interactionmessages);
        this.prependToArray(messages, resp.success.interactionmessages);
        return Observable.of(resp);
      }
    });
  }

  public loadMoreInteractions (sectionid: string, interactions: Array<any>) {
    //let bookmarkdate be lastmessagedate of the last interaction - interactions will be ordered by lastmessagedate in DESC order
    let bookmarkdate = Date.now();
    if (interactions.length > 0) {
      bookmarkdate = interactions[interactions.length-1].lastmessagedate;
    }
    //contact server
    return this.appuserApi.getInteractionsV2(this.loopbackAuth.getCurrentUserId(), null, sectionid,  'Load More', bookmarkdate).map((resp) => {
      //append to view only if result has more than one response (bookmarkdate interaction will always be returned)
      //it is possible to have 2 interactions with same lastmessagedate if both messages are created at same time
      //in this case, inifinite scroll will never be complete as result will always have 2 recrods. but this is non critical and chances are minimal.
      if (resp.success && resp.success.interactions) this.appendToArray(interactions, resp.success.interactions);
      return resp;
    });
  }

  public refreshInteractions (sectionid: string, interactions: Array<any>) {
    //bookmark for rehresh will be the last message date of the top interaction
    let bookmarkdate = '1/1/2016';
    if (interactions.length > 0) {
      bookmarkdate = interactions[0].lastmessagedate;;
    }
    return this.appuserApi.getInteractionsV2(this.loopbackAuth.getCurrentUserId(), null, sectionid,  'Refresh', bookmarkdate).flatMap((resp) => {
      if (resp.success && resp.success.interactions) this.prependToArray(interactions, resp.success.interactions);
      if (resp.success && resp.success.interactions && resp.success.limit
        && resp.success.interactions.length >= resp.success.limit) return this.refreshInteractions (sectionid, interactions);
      else return Observable.of(resp);
    });
  }

  public autoRefreshInteractions (sectionid: string, interactions: Array<any>) {
    return Observable.timer(this.appConfigInfo.refreshEvery.interactions,this.appConfigInfo.refreshEvery.interactions).subscribe((t)=>{
      this.refreshInteractions(sectionid, interactions).subscribe((res)=>{
        //do nothing, interactions array will be updated already
      }, (err)=>{
        //do nothing, not critical error
      });
    })
  }

  public autoRefreshInteractionMessages (interaction: any, messages: Array<any>) {
    return Observable.timer(this.appConfigInfo.refreshEvery.interactionMessages,this.appConfigInfo.refreshEvery.interactionMessages).subscribe((t)=>{
      this.refreshInteractionMesages(interaction, messages).subscribe((res)=>{
        //do nothing, interactions array will be updated already
      }, (err)=>{
        //do nothing, not critical error
      });
    })
  }

  private appendToArray (base: Array<any>, append: Array<any>) {
    //remove duplicate and push
    for (var i=0; i<append.length; i++) {
      for (var j=0; j<base.length; j++) {
        if (append[i].id === base[j].id) {
          base.splice(j,1);
          break;
        }
      }
      base.push(append[i]);
    }
  }

  private spliceOverlap (base: Array<any>, prepend: Array<any>) {
    //when prepending to array, if first item of base and prepend arrays are the same, we dont have to replace it
    //replacing is not a ciritcal error, but in cases of interaction messages with images, this causes a flicker, particularly during auto refresh
    //so not removing as duplicate is good for interaction messages
    //but in case of interactions, not replacing might result in a not-uptodate unreadcount and last message, as last message date coult have changed
    //but given that this is a rare case, and also, changes to last message and last message dates for other items in the array are not kept uptodate, we can avoid replacing for interactions as well
    if (prepend[0] && base[0] && prepend[0].id === base[0].id) prepend.splice(0,1);

  }

  private prependToArray (base: Array<any>, prepend: Array<any>) {
    //remove duplicate and push
    for (var i=0; i<prepend.length; i++) {
      for (var j=0; j<base.length; j++) {
        if (prepend[i].id === base[j].id) {
          base.splice(j,1);
          break;
        }
      }
      base.unshift(prepend[i]);
    }
  }

  public deleteInteraction (interaction: any, interactions: Array<any>) {
    return this.appuserApi.deleteInteraction(this.loopbackAuth.getCurrentUserId(), null, interaction.sectionid, interaction.id).map((resp) => {
      if (interactions.indexOf(interaction) != -1) interactions.splice(interactions.indexOf(interaction), 1);
      return resp;
    });
  }

  public deleteInteractionMessage (interaction: any, interactionMessage: any, messages: Array<any>) {
    return this.appuserApi.deleteInteractionMessage(this.loopbackAuth.getCurrentUserId(), null, interaction.sectionid, interaction.id, interactionMessage.id).map((resp) => {
      if (messages.indexOf(interactionMessage) != -1) messages.splice(messages.indexOf(interactionMessage), 1);
      return resp;
    });
  }

  public markAsRead (sectionid, interaction, messages) {
    let unreadCount = interaction.myunreadcount || 0;
    let lastreadmessagedate = new Date('1/1/2000'); //dummy date
    let updateonbookmark = new Date('1/1/2000'); //dummy date
    if (interaction.interactionusertrackers && interaction.interactionusertrackers[0]) updateonbookmark = interaction.interactionusertrackers[0].updatedon;
    if (messages[0] && messages[0].createdon) lastreadmessagedate = messages[0].createdon;
    if (unreadCount > 0) this.appuserApi.updateInteractionUserTracker(this.loopbackAuth.getCurrentUserId(), null, interaction.id, sectionid, unreadCount, lastreadmessagedate, updateonbookmark).subscribe((res)=>{
      //if unreadcount responds successfully, it means unreadcount in db was reduced what was sent as reduceby
      //so we can do the same here
      interaction.myunreadcount = 0;
    }, (err)=>{
      //do nothing
    });
  }
}
