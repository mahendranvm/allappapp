import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { SpinnerWrapper, GalleryService, InteractService, ErrorToaster, BookingService, StaticSectionService } from '../services';


/*
 * Service for opening a section
 *
 */
@Injectable()
export class AppNavService {

  constructor (public galleryService: GalleryService,
               public bookingService: BookingService,
               public interactService: InteractService,
               public staticSectionService: StaticSectionService,
               public spinnerWrapper: SpinnerWrapper,
               public errorToaster: ErrorToaster,
               public translateService: TranslateService) {

                this.translateService.get("LOAD_FAIL_ERROR").subscribe((value) => {
                  this.loadFailErrorString = value;
                });

  }

  //TODO: navigation to interact and push interactions are particularly slow. need to identify why.

  private loadFailErrorString: string;

  openSection (page:any, section:any, navCtrl:any, gotoBookingId?:string, setPages?:boolean, successMessage?:string) {

    if (section.module=="Interact" || section.module=="Push") {
      let interactions: Array<any> = [];
      this.interactService.loadMoreInteractions (section.id, interactions).subscribe((resp)=>{
        if(!setPages) navCtrl.push(page.InteractionsPage, {section: section, interactions: interactions});
        else {
          navCtrl.setPages([
            {page: page.HomePage},
            {page: page.InteractionsPage, params: {section: section, interactions: interactions}}
          ]);
        }
      }, (err)=>{
        this.spinnerWrapper.hide()
        this.errorToaster.toastError(err, this.loadFailErrorString, navCtrl);
      });
    }

    if (section.module=="Photos") {
      this.galleryService.getAlbums (section.id).subscribe((resp)=>{
        let albums: Array<any> = [];
        if (resp && resp.success) albums = resp.success;
        //open gallery page directly if there is only one album, else open albums page
        if (albums.length && albums.length==1) {
          if(!setPages) navCtrl.push(page.GalleryPage, {section: section, album: albums[0]});
          else {
            navCtrl.setPages([
              {page: page.HomePage},
              {page: page.GalleryPage, params: {section: section, album: albums[0]}}
            ]);
          }
        }
        else {
          if(!setPages) navCtrl.push(page.AlbumsPage, {section: section, albums: albums});
          else {
            navCtrl.setPages([
              {page: page.HomePage},
              {page: page.AlbumsPage, params: {section: section, albums: albums}}
            ]);
          }
        }
      }, (err)=>{
        this.spinnerWrapper.hide();
        this.errorToaster.toastError(err, this.loadFailErrorString, navCtrl);
      });
    }

    if (section.module=="Booking") {
      this.bookingService.getSectionServiceInfo (section).subscribe ((resp)=>{
        let sectiondetail = resp.success; //use section detail as section going forward as it will contain apptimezone details that is normally not stored as part of section
        //now check if this section has more than one service associated with it, if so direct to select a service page else call next step
        if (sectiondetail && sectiondetail.activeservicedetails && sectiondetail.activeservicedetails.length == 1) {
            //calling select service page if multiple services are present. note we use sectiondetail as section.
            if(!setPages) navCtrl.push(page.SearchAvailabilityPage, {section: sectiondetail, service: sectiondetail.activeservicedetails[0], availPgTitle: sectiondetail.name});
            else {
              navCtrl.setPages([
                {page: page.HomePage},
                {page: page.SearchAvailabilityPage, params: {section: sectiondetail, service: sectiondetail.activeservicedetails[0], availPgTitle: sectiondetail.name}}
              ]);
            }
        } else if (sectiondetail && sectiondetail.activeservicedetails && sectiondetail.activeservicedetails.length != 1) {
            //calling select service page if multiple services are present. note we use sectiondetail as section.
            if(!setPages) navCtrl.push(page.SelectServicePage, {section: sectiondetail});
            else {
              navCtrl.setPages([
                {page: page.HomePage},
                {page: page.SelectServicePage, params: {section: sectiondetail}}
              ]);
            }
        } else {
          this.spinnerWrapper.hide();
          this.errorToaster.toastError(null, this.loadFailErrorString, navCtrl);
        }
      }, (err)=> {
        this.spinnerWrapper.hide();
        this.errorToaster.toastError(err, this.loadFailErrorString, navCtrl);
      });
    }

    if (section.module=="Manage Booking") {
      let bookings: Array<any> = [];
      //first load all bookings
      this.bookingService.viewMyBookings (false).subscribe((resp)=>{
        if (resp && resp.success) bookings = resp.success;
        //if no target booking if, go to the manage booking page
        if (!gotoBookingId) {
          this.spinnerWrapper.hide();
          if(!setPages) navCtrl.push(page.ListBookingPage, {section: section, bookings: bookings});
          else {
            navCtrl.setPages([
              {page: page.HomePage},
              {page: page.ListBookingPage, params: {section: section, bookings: bookings}}
            ]);
          }
        } else { //if not, go to view booking details page
          let match = false;
          //check if there is a particular booking confirmation that needs to be opened. if so open it by default.
          bookings.forEach ((booking) => {
            if (booking.id == gotoBookingId) {
              match = true;
              this.spinnerWrapper.hide();
              navCtrl.setPages([
                {page: page.HomePage},
                {page: page.ListBookingPage, params: {section: section, bookings: bookings}},
                {page: page.ViewBookingDetailsPage, params: {section: section, booking: booking, bookings: bookings, history: false, successMessage: successMessage}}
              ]);
            }
          });
          //if no match found, go to list page
          if (!match) {
            this.spinnerWrapper.hide();
            if(!setPages) navCtrl.push(page.ListBookingPage, {section: section, bookings: bookings});
            else {
              navCtrl.setPages([
                {page: page.HomePage},
                {page: page.ListBookingPage, params: {section: section, bookings: bookings}}
              ]);
            }
          }
        }
      }, (err)=>{
        this.spinnerWrapper.hide();
        this.errorToaster.toastError(err, this.loadFailErrorString, navCtrl);
      });
    }

    if (section.module=="FAQ") {
      this.staticSectionService.getStaticSectionData (section.id).subscribe((resp)=>{
        let faqs: Array<any> = [];
        if (resp && resp.success && resp.success.length && resp.success[0].sectiondata) faqs = resp.success[0].sectiondata;
        //open faq page
        if(!setPages) navCtrl.push(page.FAQPage, {section: section, faqs: faqs});
        else {
          navCtrl.setPages([
            {page: page.HomePage},
            {page: page.FAQPage, params: {section: section, faqs: faqs}}
          ]);
        }
      }, (err)=>{
        this.spinnerWrapper.hide();
        this.errorToaster.toastError(err, this.loadFailErrorString, navCtrl);
      });
    }

    if (section.module=="Location") {
      this.staticSectionService.getStaticSectionData (section.id).subscribe((resp)=>{
        let locations: Array<any> = [];
        if (resp && resp.success && resp.success.length && resp.success[0].sectiondata) locations = resp.success[0].sectiondata;
        //open location page
        if(!setPages) navCtrl.push(page.LocationPage, {section: section, locations: locations});
        else {
          navCtrl.setPages([
            {page: page.HomePage},
            {page: page.LocationPage, params: {section: section, locations: locations}}
          ]);
        }
      }, (err)=>{
        this.spinnerWrapper.hide();
        this.errorToaster.toastError(err, this.loadFailErrorString, navCtrl);
      });
    }

    if (section.module=="Info") {
      this.staticSectionService.getStaticSectionData (section.id).subscribe((resp)=>{
        let infosectiondata: Array<any> = [];
        if (resp && resp.success && resp.success.length && resp.success[0].sectiondata) infosectiondata = resp.success[0].sectiondata;
        //open location page
        if(!setPages) navCtrl.push(page.InfoPage, {section: section, infosectiondata: infosectiondata});
        else {
          navCtrl.setPages([
            {page: page.HomePage},
            {page: page.InfoPage, params: {section: section, infosectiondata: infosectiondata}}
          ]);
        }
      }, (err)=>{
        this.spinnerWrapper.hide();
        this.errorToaster.toastError(err, this.loadFailErrorString, navCtrl);
      });
    }

    if (section.module=="List") {
      this.staticSectionService.getStaticSectionData (section.id).subscribe((resp)=>{
        let listdata: any = {};
        if (resp && resp.success && resp.success.length && resp.success[0].sectiondata[0]) listdata = resp.success[0].sectiondata[0];
        //open list page
        if(!setPages) navCtrl.push(page.ListPage, {section: section, listdata: listdata});
        else {
          navCtrl.setPages([
            {page: page.HomePage},
            {page: page.ListPage, params: {section: section, listdata: listdata}}
          ]);
        }
      }, (err)=>{
        this.spinnerWrapper.hide();
        this.errorToaster.toastError(err, this.loadFailErrorString, navCtrl);
      });
    }

  }


}
