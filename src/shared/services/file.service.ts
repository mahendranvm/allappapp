import { Injectable } from '@angular/core';
import { Platform, normalizeURL } from 'ionic-angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { SocialSharing } from '@ionic-native/social-sharing';

declare var cordova: any;
declare var window: any;

/**
 * File and File Share service
 *
 */
@Injectable()
export class FileService {

  private androidPicFolderName: string = 'Pictures'

  //constructor
  constructor (private platform: Platform,
               private transfer: FileTransfer,
               private file: File,
               private socialSharing: SocialSharing) {
  }



  public savePicToGallery (url, filekey) {
    if (this.platform.is("android")) {
      return this.savePicToGalleryAndroid (url, filekey);
    } else if (this.platform.is("ios")) {
      return this.savePicToGalleryIOS (url);
    }
  }

  //save File for Android
  private savePicToGalleryAndroid (url, filekey) {
    //target path in device
    let targetPath = this.file.externalRootDirectory + this.androidPicFolderName + '/' + Date.now() + filekey.split("/").pop();
    //fileTransfer instance
    const fileTransfer: FileTransferObject = this.transfer.create();

    //saving in external root directory as this needs to be used by media scanner for showing up in Gallery
    //typically there is a pictures folder in there already that will be used
    return this.file.checkDir(this.file.externalRootDirectory, this.androidPicFolderName)
    .catch((err)=> { return this.file.createDir(this.file.externalRootDirectory, this.androidPicFolderName, false)})
    .then ((resp)=> { return fileTransfer.download(url, targetPath) })
    .then ((entry)=> { return this.scanMedia (entry)})
  }

  //function to scan file uri to media
  private scanMedia (entry) {
    return new Promise ((resolve, reject) =>{
      if (cordova.plugins.MediaScannerPlugin) {
        cordova.plugins.MediaScannerPlugin.scanFile(entry.toURL(), resolve, reject);
      } else {
        reject ("No Media Scanner available");
      }
    })
  }

  //save File for IOS
  private savePicToGalleryIOS (url) {
    return new Promise ((resolve, reject) =>{
      if (window && window.plugins && window.plugins.socialsharing) {
        return window.plugins.socialsharing.saveToPhotoAlbum([url],resolve, reject);
      } else {
        reject ("Failed to Save, Plugin not available")
      }
    })
  }

  //download file to cache
  public downloadToCache (url, filekey) {
    //target path
    let targetPath = null;
    //fileTransfer instance
    const fileTransfer: FileTransferObject = this.transfer.create();
    //derive target path. in this case, we will use a standard file name of "tempsharecache" followed by the extension in filekey
    //using same file name will ensure that the file is overwritten evertime something is downloaded using this function, effectively making it like a temporary storage place
    if (this.platform.is("android") && this.file.externalCacheDirectory) {
      targetPath = this.file.externalCacheDirectory + "tempsharecache." + filekey.split(".").pop();
    } else if (this.platform.is("ios") && this.file.documentsDirectory) {
      targetPath = this.file.documentsDirectory + "tempsharecache." + filekey.split(".").pop();
    }
    //download file and store in target directory
    return fileTransfer.download(url, targetPath)
  }

  //download file to media folder
  public downloadToDataDirectory (url, filekey) {
    //target path
    let targetPath = null;
    //fileTransfer instance
    const fileTransfer: FileTransferObject = this.transfer.create();
    //derive target path
    if (this.platform.is("android") && this.file.dataDirectory) {
      targetPath = this.file.dataDirectory + filekey;
    } else if (this.platform.is("ios") && this.file.syncedDataDirectory) {
      targetPath = this.file.syncedDataDirectory + filekey;
    }
    //download file and store in target path
    return fileTransfer.download(url, targetPath)
  }

  public checkDataDirectory (filekey) {

    let path: any = null;

    if (this.platform.is("android") && this.file.dataDirectory) {
      path = this.file.dataDirectory;
    } else if (this.platform.is("ios") && this.file.syncedDataDirectory) {
      path = this.file.syncedDataDirectory;
    }

    return this.file.checkFile(path, filekey)
    .then(res=>{
      //this.file.getFile(path, filekey, {create: true, exclusive: false})
      return  this.file.resolveLocalFilesystemUrl (path + filekey)
      .then (fe=>{
        return normalizeURL(fe.toURL());
      })
      //return path+filekey
    })
  }

  //social sharing
  public socialShareFromCache (url) {
    return this.socialSharing.share(null, null, url, null);

  }

  //upload image
  public uploadFileToS3 (server, filePath, fileName, filetype) {
    let options:FileUploadOptions = {
      fileName: fileName,
      httpMethod: 'PUT',
      headers: {'Content-Type': filetype},
      mimeType: filetype,
      //encodeURI: false,
      chunkedMode: false
    }

    const fileTransfer: FileTransferObject = this.transfer.create();

    return fileTransfer.upload(filePath, server, options);
  }

}
