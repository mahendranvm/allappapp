import { Injectable } from '@angular/core';
import { LoopBackAuth } from '../../shared/lbsdk/services';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../../pages/account/login/login';

/**
 * This service has functions to retrieve current device info, compare with what is stored in server, and resend to server if something has changed.
 *
 */
@Injectable()
export class PageAuthCheck {
//TODO: redirect to login of http failures..
  //constructor
  constructor(private loopbackAuth: LoopBackAuth, public navCtrl: NavController) {

  }

  //check
  public check (forPage?: string) {
    //check if auth and id are present
    if (!this.loopbackAuth.getAccessTokenId() || !this.loopbackAuth.getCurrentUserId()) {
      //go to login page
      this.navCtrl.push(LoginPage);
    }
  }

}
