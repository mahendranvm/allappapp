import { Injectable } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar';
import { ThemeColorConfig } from '../config';
import { AppConfigInfo } from '../services';

/**
 * This service has functions to set the background color and foreground text color of statusbar on phone
 *
 */
@Injectable()
export class StatusBarSettings {

  //constructor
  constructor (private themeColorConfig: ThemeColorConfig,
               private statusBar: StatusBar,
               private appConfig: AppConfigInfo) {

  }

  //check if internet connection is available and display a toast if not
  public setStatusBarColor () {
    //backgroud color will be set to darker shade of app theme color
    this.statusBar.backgroundColorByHexString(this.themeColorConfig.themeColors[this.appConfig.appThemeOptions.navbarcolor].dark);
    //read from config colors and set statusbar style accordingly
    this.statusBar.show();

    if (this.themeColorConfig.themeColors[this.appConfig.appThemeOptions.navbarcolor].defaultstatusbar === 'light') {
      this.statusBar.styleLightContent();
    } else {
      this.statusBar.styleDefault();
    }
  }

  public setStatusBarDefaultStyle () {
    this.statusBar.styleDefault();
  }

  public hideStatusBar () {
    this.statusBar.hide();
  }

  public showStatusBar () {
    this.statusBar.show();
  }

}
