import { Injectable } from '@angular/core';
import { AppuserApi, LoopBackAuth } from '../../shared/lbsdk/services';
import { TranslateService } from '@ngx-translate/core';
import { AppConfigInfo } from '../services'

/**
 * Booking Service
 *
 */
@Injectable()
export class BookingService {

  private anyString: string = 'Any';
  constructor (public appuserApi: AppuserApi,
               public translateService: TranslateService,
               public appConfigInfo: AppConfigInfo,
               public loopBackAuth: LoopBackAuth) {
       //get any string
       this.translateService.get(['BOOKING_ANY']).subscribe((value) => {
         this.anyString = value['BOOKING_ANY'];
       })
  }

  public getAnyR () {
    return {rid:this.anyString, srid: this.anyString, name: this.anyString}
  }

  public getSectionServiceInfo (section: any) {
    return this.appuserApi.getBookingSectionWithServiceDetails (this.loopBackAuth.getCurrentUserId(), this.appConfigInfo.realm, section.id);
  }

  public searchForDate (queryDate:Date, service:any, validSRsAvailability:Array<any>, selectFromRs: Array<any>, userSelectedRs: Array<any>, timeslots: Array<any>) {
    //get time zone free query date
    let tzFreeQueryDate = new Date();
    tzFreeQueryDate.setUTCFullYear(queryDate.getFullYear());
    tzFreeQueryDate.setUTCMonth(queryDate.getMonth());
    tzFreeQueryDate.setUTCDate(queryDate.getDate());
    tzFreeQueryDate.setUTCHours(0,0,0,0);

    //convert userSelectedRs into array of R IDs
    let userSelectedRsIDOnly = [];
    userSelectedRs.forEach((selection)=>{
      userSelectedRsIDOnly.push(selection.rid);
    })

    return this.appuserApi.searchSRCalendarForDate(this.loopBackAuth.getCurrentUserId(), null, service.sectionid, null, null, null, service.id, tzFreeQueryDate).map ((resp)=>{
      validSRsAvailability.length = 0; //emptying the array
      if (resp && resp.success && resp.success.length>0) {
        Array.prototype.push.apply(validSRsAvailability, resp.success);
      }
      this.refreshSelectFromRs(selectFromRs, service, validSRsAvailability, userSelectedRsIDOnly);
      this.constructTimeslots(service, validSRsAvailability, timeslots);
      this.refreshTimeslotAvailability(service, userSelectedRsIDOnly, timeslots);
      return resp;
    })
  }

  //refresh the selectfromsrs
  public refreshSelectFromRs (selectFromRs:Array<any>, service:any, validSRsAvailability:Array<any>, userSelectedRsIDOnly:Array<string>) {
    selectFromRs.length=0;//emptying the array
    if (service.srperbooking > 1) {
      //for srperbooking > 1, one res can be selected only once, regardless of the sr's remaining availability
      for (let i=0; i<validSRsAvailability.length; i++) {
        let matchfound = false;
        for (let j=0; j<userSelectedRsIDOnly.length; j++) {
          if (userSelectedRsIDOnly[j]==validSRsAvailability[i].resourceid){
            matchfound = true;
            break;
          }
        }
        if (!matchfound) {
          //push into selectFromRs, if the resource was not already available
          let alreadypresent = false;
          for (let j=0; j<selectFromRs.length; j++) {
            if (selectFromRs[j].rid==validSRsAvailability[i].appresource.id) {
              alreadypresent = true;
              break;
            }
          }
          if (!alreadypresent) {
            selectFromRs.push({
              name: validSRsAvailability[i].appresource.name,
              title: validSRsAvailability[i].appresource.title,
              rid: validSRsAvailability[i].appresource.id
            });
          }
        }
      }
    } else {
      //for srperbooking == 1, res can be selected as long as it's srs have at least one capacity left
      for (let i=0; i<validSRsAvailability.length; i++) {
        //push into selectFromRs, if the resource was not already available
        let alreadypresent = false;
        for (let j=0; j<selectFromRs.length; j++) {
          if (selectFromRs[j].rid==validSRsAvailability[i].appresource.id) {
            alreadypresent = true;
            break;
          }
        }
        if (!alreadypresent) {
          selectFromRs.push({
            name: validSRsAvailability[i].appresource.name,
            title: validSRsAvailability[i].appresource.title,
            rid: validSRsAvailability[i].appresource.id
          });
        }
      }
    }
  }

  /*
  constructtimeslots gets srcalendar data and contstructs timeslots based on set quantity
  only construction of timeslots with details on availability by sr in each timeslot happens here
  no logic to check availability/unavailability happens here - thats done in refreshtimeslotavailability
  */
  public constructTimeslots (service:any, validSRsAvailability:Array<any>, timeslots:Array<any>) {
    timeslots.length=0; //empty the array
    //loop through each sr's availability
    for (let i=0; i<validSRsAvailability.length; i++) {
      //this is the number of continuous spots needed to be booked in the sr calendar for the selected service
      let contiguousspots = Math.ceil(service.duration/validSRsAvailability[i].bookingstartinterval);
      //now loop through each sr calendar spot
      for (let j=0; j<=validSRsAvailability[i].capacityschedule.length-contiguousspots;j++) {
        let contiguousspotsavailability = true;
        let mincapacityincontiguousspots = validSRsAvailability[i].capacityschedule[j].capacity;
        //check if contiguous spots to cover service duration are available for selected quantity
        for (let k=0; k<contiguousspots; k++) {
          if (validSRsAvailability[i].capacityschedule[j+k].capacity < 1
            || validSRsAvailability[i].capacityschedule[j+k].minfrommidnight != (validSRsAvailability[i].capacityschedule[j].minfrommidnight+(k*validSRsAvailability[i].bookingstartinterval))) {
            contiguousspotsavailability = false;
            break;
          } else {
            if (validSRsAvailability[i].capacityschedule[j+k].capacity < mincapacityincontiguousspots) {
              mincapacityincontiguousspots = validSRsAvailability[i].capacityschedule[j+k].capacity;
            }
          }
        }
        // if contiguous spots are available, add it to timeslots. else, move to next possible slot.
        if (contiguousspotsavailability) {
          let slotdetails = {};
          let time:any = "";
          let hours:any = Math.floor(validSRsAvailability[i].capacityschedule[j].minfrommidnight/60);
          let mins:any = validSRsAvailability[i].capacityschedule[j].minfrommidnight % 60
          if (mins<10) mins="0"+mins;
          if (hours == 0) time = "12:"+mins+" am";
          if (hours>0 && hours<12) time = hours+":"+mins+" am";
          if (hours==12) time = hours+":"+mins+" pm";
          if (hours>12) time = (hours-12)+":"+mins+" pm";
          slotdetails = {srid:validSRsAvailability[i].serviceresourceid, capacity:mincapacityincontiguousspots};
          //if no entry for timeslots were made, create one
          if (timeslots.length==0) {
            timeslots.push({time:time, minfrommidnight: validSRsAvailability[i].capacityschedule[j].minfrommidnight, available:true, details:[{rid: validSRsAvailability[i].resourceid, totalcapacity: mincapacityincontiguousspots, srdetails: [slotdetails]}]});
          } else {
            //check if an entry for this timeslot is already available
            for (let k=0; k<timeslots.length;k++) {
              //if available
              if (timeslots[k].time==time) {
                //check if an entry for this resource was added already
                let timeslotresmatch = false;
                for (let l=0; l<timeslots[k].details.length; l++) {
                  if (timeslots[k].details[l].rid==validSRsAvailability[i].resourceid) {
                    timeslotresmatch = true;
                    //if resource was already added to details of this timeslot, then simply append array
                    timeslots[k].details[l].srdetails.push (slotdetails);
                    //add to total capacity
                    timeslots[k].details[l].totalcapacity = timeslots[k].details[l].totalcapacity + mincapacityincontiguousspots;
                    //sort by srcapacity in desc order
                    timeslots[k].details[l].srdetails.sort((a, b)=> { return b.capacity - a.capacity });
                    break;
                  }
                }
                //if resource was not added already, then add now
                if (!timeslotresmatch) {
                  timeslots[k].details.push({rid: validSRsAvailability[i].resourceid, totalcapacity: mincapacityincontiguousspots, srdetails: [slotdetails]});
                }
                break;
              }
              //if timeslot was not already available, then add it
              if (k==timeslots.length-1) {
                timeslots.push({time:time, minfrommidnight: validSRsAvailability[i].capacityschedule[j].minfrommidnight, available:true, details:[{rid: validSRsAvailability[i].resourceid, totalcapacity: mincapacityincontiguousspots, srdetails: [slotdetails]}]});
                break;
              }
            }
          }
        }
      }
    }
    //now sort timeslots by time
    timeslots.sort((a, b)=> { return a.minfrommidnight - b.minfrommidnight })
  };

  /*
  //this function updates the timeslots.available indicator based on selected srs and set quantity
  //logic
  //first split userSelectedSRsIDOnly into selectedsrs and anysrcount
    //selectedsrs will be an array like [{srid, selcount, availability }] where selectcount is the number of times this srid was selected in userSelectedSRsIDOnly, availability false by default
    //note - selectcount will always be 1 for a srid in cases where srperbooking > 1. this will be ensured in selectserviceresource logic
  //now for each timeslot in temptimeslot (a copy of timeslot)
    //availableanysrcount = 0
    //availableanysrcapacity = 0
    //selectedsrs - set all availability as false
    //for each timeslot details
      //isthisselectedsr = false, remainininganycapacity = 0
      //for each selected sr
        //if selectedsr.srid == timeslotdetails.srid
          //isthisselectedsr = true
          //if timeslotdetails.capacity >= 1
            //set selectedsr.availability as true
            //set remainininganycapacity = timeslotdetails.capacity - 1
      //if isthisselectedsr == false
        //if timeslotdetails.capacity >= 1
          //anysrcount++
        //availableanysrcapacity = availableanysrcapacity +  timeslotdetails.capacity
      //else
        //availableanysrcapacity = availableanysrcapacity +  remainininganycapacity
    //end of for within details of timeslot
    //now check if all selectedsrs have availability set to true
      //if yes, then check if srperbooking > 1
        //if yes, check if availableanysrcount > anysrcount
          //if yes, then set timeslot availability as true
          //if not, set timeslot availability as false
        //if not and srperbooking = 1, check if availableanysrcapacity >= quantity
          //if yes, then set timeslot availability as true
          //if not, set timeslot availability as false
      //if not all sselectedsrs have availability, the set timeslot availability to false
  */
  public refreshTimeslotAvailability (service:any, userSelectedRsIDOnly:Array<string>, timeslots:Array<any>) {
    let selrs = [];
    let anyrcount = 0;

    //parse userSelectedRsIDOnly and get distinct list of selected res in selrs with a count of number of times the res is selected
    for (let i=0; i<userSelectedRsIDOnly.length; i++) {
      if (userSelectedRsIDOnly[i]==this.anyString) {
        anyrcount++;
      } else {
        //if a particular res has been selected, get that out into selrs
        let selcount = 1;
        let alreadycounted = false;
        for (let j=0; j<selrs.length; j++) {
          if (selrs[j].rid == userSelectedRsIDOnly[i]) {
            alreadycounted = true;
          }
        }
        if (!alreadycounted) {
          for (let j=i+1; j<userSelectedRsIDOnly.length; j++) {
            if (userSelectedRsIDOnly[j]==userSelectedRsIDOnly[i]) {
              selcount++;
            }
          }
          selrs.push({rid:userSelectedRsIDOnly[i], selcount:selcount, availability:false});
        }
      }
    };

    //in each available time slot
    for (let i=0; i<timeslots.length;i++){
      let availableanyrcount = 0;
      let availableanyrcapacity = 0;
      //here re-setting availability to false. for everytimeslot using this to check if the selr has availability in that timeslot.
      for (let j=0; j<selrs.length; j++) {
        selrs[j].availability = false;
      }
      //loop through each res details
      for (let j=0; j<timeslots[i].details.length; j++) {
        var isthisselectedr = false;
        var remainingcapacity = 0;
        //check if this res is part of selectedrs, if so check if sufficient capacity is available and accordingly update timeslot availability
        for (let k=0; k<selrs.length; k++) {
          if (timeslots[i].details[j].rid == selrs[k].rid) {
            isthisselectedr = true;
            //7/25/17 - just decided calendarbooking will always be true. so the else condition will not be required. retaining here for now.
            if (service.calendarbooking) {
              if (timeslots[i].details[j].totalcapacity >= selrs[k].selcount) {
                selrs[k].availability = true;
                remainingcapacity = timeslots[i].details[j].totalcapacity - selrs[k].selcount;
              }
              //if capacity is not available for the required count, then availability will remain false
            } else {
              //if calendarbooking is false, this is a request only booking
              //for such cases, selcount  is always 1 - so if selected sr is available, regardless of sr's available capacity, the timeslot is available for the sr
              //and remainingcapacity is always to the same timeslots[i].details[j].capacity without decreasing for selection
              if (timeslots[i].details[j].totalcapacity >= 1) {
                selrs[k].availability = true;
                remainingcapacity = timeslots[i].details[j].totalcapacity;
              }
            }
            break;
          }
        }
        //if the r was a user selected r, increase availableanyrcapacity by only remainingcapacity. if this is not selectedr then increase availableanysrcapacity by entire capacity
        //if not, and if capacity is available,
          //increase availableanyrcapacity by capacity for this r in this timeslot. and also increase availableanyrcount by 1.
        if (isthisselectedr) {
          //if srperbooking is 1, the same res can be selected again. so, add remainingcapacity to availableanysrcapacity. if srperbooking is > 1, one sr can be selected only once.
          if (service.srperbooking == 1) {
            availableanyrcapacity = availableanyrcapacity + remainingcapacity;
          }
        } else {
          if (timeslots[i].details[j].totalcapacity>=1) {
            availableanyrcapacity = availableanyrcapacity + timeslots[i].details[j].totalcapacity;
            availableanyrcount++;
          }
        }
      }

      //now check if all selectedrs had availability and if enough number of anyrs had enough availability
      var allselrsavailable = true;
      for (let j=0; j<selrs.length; j++) {
        if (!selrs[j].availability) {
          allselrsavailable = false;
          break;
        }
      }
      //if not all selectedrs had availability, then set availability for this timeslot as false
      if (!allselrsavailable) {
        timeslots[i].available = false;
      } else {
        //if all selrs have availability, check for enough anysr availability
        if (service.calendarbooking && service.srperbooking == 1) {
          //if availableanyrcapacity is greater than the remaining anyrs to be selected then timeslot is available (since 1 unit = 1 capacity)
          if (availableanyrcapacity < anyrcount) {
            timeslots[i].available = false;
          } else {
            timeslots[i].available = true;
          }
        } else if (!service.calendarbooking && service.srperbooking == 1) {
          //for no-calendarbooking, just need atleast one remaining capacity
          //in other words, if only one R was available, and quantity was 5, we will still show as available (this is really same as five customers booking same r)
          if (availableanyrcapacity < 1) {
            timeslots[i].available = false;
          } else {
            timeslots[i].available = true;
          }
        } else {
          //for srperbooking > 1, each selected r should be unique. so
          if (availableanyrcount < anyrcount) {
            timeslots[i].available = false;
          } else {
            timeslots[i].available = true;
          }
        }
      }
    }
    //now sort timeslots by time
    timeslots.sort((a, b)=> { return a.minfrommidnight - b.minfrommidnight })
  }

  //update userselectedsrs and userselectedsrnames according to quantity
  public alignUserSelectedRsToQuantity (userSelectedRs: Array<any>, quantity: number) {
    var currentlength = userSelectedRs.length;
    var requiredlength = quantity;
    if (currentlength != quantity) {
      if (currentlength < requiredlength) {
        for (let i=0; i<requiredlength-currentlength; i++) {
          userSelectedRs.push(this.getAnyR());
        }
      } else {
        for (let i=0; i<currentlength-requiredlength; i++) {
          userSelectedRs.pop();
        }
      }
    }
  }

  //no availability check
  public noAvailabilityCheck (timeslots:Array<any>): boolean {
    if (!timeslots || !timeslots.length || timeslots.length<=0) return false;
    else {
      let avail = false;
      timeslots.forEach((timeslot)=>{
        if (timeslot.available) avail = true;
      })
      return avail;
    }

  }

  //book service
  public bookonly (timeslot:any, userSelectedRsIDOnly:Array<string>, service:any, validSRsAvailability:Array<any>, queryDate:Date, quantity:number, serviceBy:string, sectionServiceLabel:string) {
    //from the userselectedrs, get selected rs selrs and the capacity details to pick for "any" choices
    let booksrs: Array<any> = [];
    let anysrcapacity: Array<any> = [];
    //for every sr in selected timeslot
    for (let i=0; i<timeslot.details.length; i++) {
      let selcount = 0
      //count the number of times the r has been selected by the user
      for (let j=0; j<userSelectedRsIDOnly.length; j++) {
        if (userSelectedRsIDOnly[j]==timeslot.details[i].rid) {
          selcount++;
        }
      }
      //if the r was not selcted by user at all, add entire capacity information to anyrcapacity pool
      if (selcount == 0) {
        //now loop through all the srdetails and add the capacity into anysrcapacity
        for (let j=0; j<timeslot.details[i].srdetails.length; j++) {
          anysrcapacity.push ({rid: timeslot.details[i].rid, srid:timeslot.details[i].srdetails[j].srid, remainingcapacity:timeslot.details[i].srdetails[j].capacity});
        }
      } else {
        //if res was selected by user, add only any excess capacity over and above user selection to anysrcapacity pool, in cases where srperbooking = 1 (i.e. distinct Rs are not required to provide this servicve)
        if (service.srperbooking == 1) {
          let tobereducedremainingcapacity = selcount;
          for (let j=0; j<timeslot.details[i].srdetails.length; j++) {
            //check if tobereducedremainingcapacity exists
            //if it doesnt, then add entire capacity
            if (tobereducedremainingcapacity <=0) anysrcapacity.push ({rid:timeslot.details[i].rid, srid:timeslot.details[i].srdetails[j].srid, remainingcapacity: timeslot.details[i].srdetails[j].capacity});
            //if it does and is greater than current srcapacity, then reduce tobereducedremainingcapacity by srcapacity. dont add anything to anysrcapacity. add everything to booksrs.
            else if (tobereducedremainingcapacity > 0 && timeslot.details[i].srdetails[j].capacity < tobereducedremainingcapacity) {
              tobereducedremainingcapacity = tobereducedremainingcapacity - timeslot.details[i].srdetails[j].capacity;
              booksrs.push ({rid:timeslot.details[i].rid, srid:timeslot.details[i].srdetails[j].srid, quantity: timeslot.details[i].srdetails[j].capacity});
            }
            //else, i.e. tobereducedremainingcapacity exists and is less than srcapacity, add this sr and tobereducedremainingcapacity to booksrs
            //then make tobereducedremainingcapacity 0 and add remaining srcapacity to anysrcapacity
            else {
              anysrcapacity.push ({rid:timeslot.details[i].rid, srid:timeslot.details[i].srdetails[j].srid, remainingcapacity: timeslot.details[i].srdetails[j].capacity - tobereducedremainingcapacity});
              booksrs.push ({rid:timeslot.details[i].rid, srid:timeslot.details[i].srdetails[j].srid, quantity: tobereducedremainingcapacity});
              tobereducedremainingcapacity = 0;
            }
          }
        } else {
          //if res was selected by user and in cases where srperbooking > 1 (i.e. distinct Rs ARE required to provide this servicve)
          //add nothing for this resource to anysrcapacity pool
          let tobereducedremainingcapacity = selcount;
          //loop through sr details and add as much as needed to booksrs pool
          for (let j=0; j<timeslot.details[i].srdetails.length; j++) {
            if (tobereducedremainingcapacity > timeslot.details[i].srdetails[j].capacity) {
              tobereducedremainingcapacity = tobereducedremainingcapacity - timeslot.details[i].srdetails[j].capacity;
              booksrs.push ({rid:timeslot.details[i].rid, srid:timeslot.details[i].srdetails[j].srid, quantity: timeslot.details[i].srdetails[j].capacity});
            } else {
              booksrs.push ({rid:timeslot.details[i].rid, srid:timeslot.details[i].srdetails[j].srid, quantity: tobereducedremainingcapacity});
              tobereducedremainingcapacity = 0;
              break;
            }
          }
        }
      }
    };

    //get anysr count
    var anyrcount = 0;
    for (let i=0; i<userSelectedRsIDOnly.length; i++) {
      if (userSelectedRsIDOnly[i]==this.anyString) {
        anyrcount++
      }
    }

    //for each anysr, first group by res..
    //grouping anysrcapacity by res
    let anyrcapacity = [];
    for (let j=0; j<anysrcapacity.length; j++) {
      let anyrmatchfound = false;
      for (let k=0; k<anyrcapacity.length; k++) {
        if (anysrcapacity[j].rid==anyrcapacity[k].rid) {
          anyrmatchfound = true;
          anyrcapacity[k].totalremainingcapacity = anyrcapacity[k].totalremainingcapacity + anysrcapacity[j].remainingcapacity;
          anyrcapacity[k].srdetails.push({srid: anysrcapacity[j].srid, remainingcapacity: anysrcapacity[j].remainingcapacity})
          break;
        }
      }
      if (!anyrmatchfound) {
        anyrcapacity.push({rid: anysrcapacity[j].rid, totalremainingcapacity: anysrcapacity[j].remainingcapacity, srdetails: [{srid: anysrcapacity[j].srid, remainingcapacity: anysrcapacity[j].remainingcapacity}]})
      }
    }

    //if srperbooking is 1, select the res with max remaining capacity from anyrcapacity pool and push to book for one unit for that r
    //if srperbooking is >1, select the res with max remaining capacity from anyrcapacity pool and push to book for one unit for that r. remove that res from anyr pool.
    for (let i=0; i<anyrcount; i++) {
      //now use array sort with compare function on total remaining capacity to get anyrcapacity sorted on totalremainingcapacity
      anyrcapacity.sort((a,b)=>{return b.totalremainingcapacity-a.totalremainingcapacity;});
      //select one unit from the max res in position 0 of anyrcapacity after sort.
      for (let j=0; j<anyrcapacity[0].srdetails.length; j++) {
        if (anyrcapacity[0].srdetails[j].remainingcapacity > 0) {
          //for the first sr of this max res, check if that sr is is already part of booksrs, increment booksrs.quantity, else push into booksrs
          var matchfound = false;
          for (let k=0; k<booksrs.length; k++) {
            if (booksrs[k].srid == anyrcapacity[0].srdetails[j].srid) {
              matchfound = true;
              booksrs[k].quantity = booksrs[k].quantity + 1;
              break;
            }
          }
          if (!matchfound) {
            booksrs.push({rid:anyrcapacity[0].rid, srid:anyrcapacity[0].srdetails[j].srid, quantity: 1});
          }
          //now adjust the remainingcapacity for this sr and totalremainingcapacity for the res
          anyrcapacity[0].srdetails[j].remainingcapacity = anyrcapacity[0].srdetails[j].remainingcapacity - 1;
          anyrcapacity[0].totalremainingcapacity = anyrcapacity[0].totalremainingcapacity - 1;
          break;
        }
      }
      //remove this resource from anyr pool if srperbooking > 1. same resource cannot be booked multiple times.
      if (service.srperbooking > 1) {
        anyrcapacity.splice(0,1);
      }
    }

    //use booksrs and capacityschedule from validsrsavailability to creating booking request
    let serviceresourcedetails: Array<any> = [];
    let assignedresources: string = '';
    //for every booksrs
    for (let i=0; i<booksrs.length; i++){
      //loop through and get the corresponding validsrsavailability record
      for (let j=0; j<validSRsAvailability.length; j++) {
        if (validSRsAvailability[j].serviceresourceid == booksrs[i].srid) {
          let changeincapacity: Array<any> = [];
          let contiguousspots = Math.ceil(service.duration/validSRsAvailability[j].bookingstartinterval);
          //loop through the capacityschedule for the sr and for each slot, set the booking capacity to 0 by default
          for (let k=0; k<validSRsAvailability[j].capacityschedule.length; k++) {
            changeincapacity.push({minfrommidnight: validSRsAvailability[j].capacityschedule[k].minfrommidnight, capacity: 0});
          }
          //now set contiguousspots starting from selected timeslot to requested capacity
          for (let k=0; k<changeincapacity.length; k++) {
            if (changeincapacity[k].minfrommidnight == timeslot.minfrommidnight) {
              for (let l=0; l<contiguousspots; l++) {
                changeincapacity[k+l].capacity = booksrs[i].quantity;
              }
              break;
            }
          }
          //push into serviceresourcedetails
          serviceresourcedetails.push ({
            serviceresourceid: validSRsAvailability[j].serviceresourceid,
            resourcename: validSRsAvailability[j].appresource.name,
            changeincapacity: changeincapacity
          });
          //set the assignedresources with names of the srs selected for booking
          if (assignedresources == '') {assignedresources = validSRsAvailability[j].appresource.name}
          else if (assignedresources != '') {
            //for loop to check if resource was selected already
            let resourceusedalready = false;
            for (let k=0; k<i; k++) {
              if (booksrs[i].rid == booksrs[k].rid) {
                resourceusedalready = true;
                break;
              }
            }
            //if resource was not selected already, then add name to assignedresources
            if (!resourceusedalready) assignedresources = assignedresources + ", " + validSRsAvailability[j].appresource.name
          }
          //break this loop and move on to next booksrs
          break;
        }
      }
    }

    let tzfreequerydate: Date = new Date();
    tzfreequerydate.setUTCFullYear(queryDate.getFullYear());
    tzfreequerydate.setUTCMonth(queryDate.getMonth());
    tzfreequerydate.setUTCDate(queryDate.getDate());
    tzfreequerydate.setUTCHours(0,0,0,0);

    //now call booking service
    return this.appuserApi.createBooking(this.loopBackAuth.getCurrentUserId(), this.appConfigInfo.realm, service.sectionid, null, serviceresourcedetails, service.id, service.name,
     service.calendarbooking, service.paytobook, tzfreequerydate, quantity, timeslot.time, timeslot.minfrommidnight, serviceBy, assignedresources, sectionServiceLabel, null);

  }

  //get bookings
  public viewMyBookings (history) {
    return this.appuserApi.viewMyBookings (this.loopBackAuth.getCurrentUserId(), this.appConfigInfo.realm, history).map((res)=>{
      if (res && res.success) {
        res.success.sort((a,b)=>{
          if (history && b.bookeddate > a.bookeddate) return 1;
          else if (!history && a.bookeddate > b.bookeddate) return 1;
          else return -1;
        });
      }
      return res;
    })
  }

  //cancel booking
  public cancelBooking (bookingid, reason) {
    return this.appuserApi.cancelBooking (this.loopBackAuth.getCurrentUserId(), null, null, null, bookingid, reason);
  }

}
