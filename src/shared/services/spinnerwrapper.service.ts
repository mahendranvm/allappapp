import { LoadingController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class SpinnerWrapper {

  private loader: any;
  private loadingString: string = 'Please wait...';

  constructor(private loadingCtrl: LoadingController, private translateService: TranslateService) {
    this.translateService.use('en');
    this.translateService.get('SPINNER_DEFAULT_MESSAGE').subscribe((value) => {
      this.loadingString = value;
    });
  }

  public show(spinner?: string, content: string = this.loadingString): any {
    this.loader = this.loadingCtrl.create({
      spinner: spinner,
      content: content
    });
    this.loader.present();
    return this.loader;
  }

  public hide () {
    this.loader.dismiss();
  }
}
