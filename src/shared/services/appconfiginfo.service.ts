import { Injectable } from '@angular/core';
import { LoopBackConfig, LoopBackAuth, AppuserApi } from '../lbsdk';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/timer';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/of';
import { ClientConfig, ThemeColorConfig } from '../config'
import { FileService } from '../services';
import { TranslateService } from '@ngx-translate/core';




/**
 * This service has functions to retrieve app sections and other related confif info.
 *
 */
@Injectable()
export class AppConfigInfo {
  /*
  > getAppSection function will be
    > function gets from local storage if present
      > function gets from remote server and refreshes local storage and property value as a side effect
    > function gets from remote server otherwise
      > function updates local storage as a side effect

  > load the appsections on successful resolution of getAppSection
  > set the appInfoLoadError to true in the reject of getAppSection

  > refer to this property in home page

  > there could be other triggers that refresh the property
  > eg: if last refresh date > 48 hours, get from remote server
  > eg: background push can be configured to call refresh
  > drwabacks
    > (not a drawback) what about initial load? - could be done in constructor. technically faster than calling method from page constructor
    > what about error? - this is where an additional property might be needed.
      > default to false. set to true during constructor intialization in reject callback
    > what about error during refreshes? - this need not be tracked. will keep showing old values.
  */

  public realm: string;
  public serverPath: string;
  public serverApiVersion: string;

  private appRealmInfo: any;
  public appAllSections: Array<any> = []; //contains all sections of this app
  public appPushSection: any; //contains a reference to the push section. this helps in handling push icon in home page.
  public appManageBookingSection: any; //contains a reference to the manage booking section. this is helpful in confirm booking page where we navigate to this page on successful booking.

  public autoRefreshSubscription: any = null;
  public badgeRefreshSubscription: any = null;

  public appThemeOptions: any = {};
  public refreshEvery: any = {};
  public tokenTTL: number = 9676800; //default 8 weeks in seconds
  private defaultAppThemeOptions: any = {
    "color" : "primary",
    "navbarcolor" : "primary",
    "homebgcolor" : "white"
  }

  public homeStyle: any = {};
  public homePartialStyle: any = {};
  public homePartialLogoStyle: any = {};

  public appInfoLoadErr: boolean = false;
  public appInfoLoadErrString: string = '';
  private storageKey: string = '$AppConfigInfo$';

  //handle to login page (to be used for forwarding to login on 401 error)
  public loginReference: any = null;
  //reference to navCtrl
  public navCtrl: any;
  public appBadgeRefreshFailCount: number = 0;

//step 1: get from local storage
  //if not present in local storage, get from file and repeat from step 1
//step 2: get from server
//step 3: if different that what is present in local storage, store in local storage


  //constructor
  constructor(private storage: Storage,
              private fileService: FileService,
              private translateService: TranslateService,
              private appuserApi: AppuserApi,
              private clientConfig: ClientConfig,
              private themeColorConfig: ThemeColorConfig,
              private loopbackAuth: LoopBackAuth) {
      //initial load from storage
      this.appInfoLoadErr = false;
      this.translateService.use('en');
      this.translateService.get("SECTION_LOAD_ERROR").subscribe((value) => {
        this.appInfoLoadErrString = value;
      });
      //get realm and server info
      this.realm = this.clientConfig.realm;
      this.serverPath = this.clientConfig.serverPath;
      this.serverApiVersion = this.clientConfig.serverApiVersion;
      //set the server address in LoopBackConfig
      LoopBackConfig.setBaseURL(this.serverPath);
      LoopBackConfig.setApiVersion(this.serverApiVersion);
  }

  //TODO: 1. dont hide splashscreen, call push or statusbar settings before loadAppSettings is complete
  //2. see if we can unsubscribe autorefresh if we get new values from refresh app
  //3. after login, we need to make sure app sections are loaded

  //scenario 1: On app load, First time entry - empty storage, no user id (behind splash screen, go to login)
  //scenario 2: On app load, First time entry - present storage, no user id (behind splash screen, load from storage, go to login)
  //scenario 3: On app load, Nth time entry - empty storage, present user id (behind splash screen, load from server, go to login)
  //scenario 4: On app load, Nth time entry - present storage, no user id
  //scenario 5: On app load, Nth time entry - empty storage, empty user id
  //scenario 6: On app load, Nth time entry - present storage, present user id
  //scenario 7: After successful login - present storage, present user id
  //scenario 8: After successful login - empty storage, present user id

  //On deploy time, load defaults for apprealmsections, tokenTTL, refreshEvery and themeoptions in clientConfig
  //On app load, get  apprealmsections, tokenTTL, refreshEvery and themeoptions from storage (if nothing in storage, store defaults from clientConfig to storage)
  //On refresh, if there is change in apprealmsections, tokenTTL or refreshEvery - then update in storage and in variables
  //on refresh, if there is change in themeoptions, check if there are image changes
    //if yes, store in newthemeoptions in storage and start download.
      //on successful download, move newthemeoptions to themeoptions in storage and update variables
      //on failed download, retry after X hours
    //if no, update themeoptions in storage and update variables

/*

  1. check if appinfo is present in local storage
  2. if present, then subscribe to auto refresh, set variable values and return (after return, initialize push and hide splah in app.component)
  3. if not present, then call server to get values. on successful return from server, set variable values and return (after return, initialize push and hide splah in app.component)
  4. if server call fails, then return with error (after error return, display error message in home screen and try again in XX minutes)

*/

  //get app section info from storage
  public loadAppSettings () {
    //this method is called onapp load and after login. so set appInfoLoadErr to false here and based on result reset it
    this.appInfoLoadErr = false;
    return Observable.fromPromise (this.storage.get(this.storageKey)).flatMap ((value)=>{
      if (value && value != '') {
        console.log(this.appRealmInfo.appconfig)
        this.appRealmInfo = JSON.parse(value);
        this.appAllSections = this.refreshSectionPointers(this.appRealmInfo.apprealmsections);
        this.appThemeOptions = this.appRealmInfo.appconfig?this.appRealmInfo.appconfig.themeoptions:this.defaultAppThemeOptions;
        this.refreshEvery = this.appRealmInfo.appconfig?this.appRealmInfo.appconfig.refreshevery:null;
        this.tokenTTL = this.appRealmInfo.appconfig?this.appRealmInfo.appconfig.tokenttl:this.tokenTTL;

        //create home page style
        this.formHomePageStyle();
        //refresh now...
        this.refreshApp();
        //..and also register to refresh every xx hours
        this.autoRefreshSubscription = Observable.timer(this.refreshEvery.sections,this.refreshEvery.sections).subscribe((t)=>{
          this.refreshApp();
        })
        return Observable.of(value);
      } else {
        //if not present, then get from server
        return this.appuserApi.getAppInfo(this.realm).map((resp) => {
          //sanitize and store
          this.appRealmInfo = resp.success;
          this.appAllSections = this.refreshSectionPointers(this.appRealmInfo.apprealmsections);
          this.appThemeOptions = this.appRealmInfo.appconfig?this.appRealmInfo.appconfig.themeoptions:this.defaultAppThemeOptions;
          this.refreshEvery = this.appRealmInfo.appconfig?this.appRealmInfo.appconfig.refreshevery:null;
          this.tokenTTL = this.appRealmInfo.appconfig?this.appRealmInfo.appconfig.tokenttl:this.tokenTTL;
          //store in storage before forming styles
          this.storage.set (this.storageKey, JSON.stringify(this.appRealmInfo));
          //create home page style
          this.formHomePageStyle();
          //..and also register to refresh every xx hours
          this.autoRefreshSubscription = Observable.timer(this.refreshEvery.sections,this.refreshEvery.sections).subscribe((t)=>{
            this.refreshApp();
          })
          return resp;
        }).catch((error) => {
          if (this.clientConfig.appRealmInfo) {
            //on failure load from client config
            this.appRealmInfo = this.clientConfig.appRealmInfo;
            this.appAllSections = this.refreshSectionPointers(this.appRealmInfo.apprealmsections);
            this.appThemeOptions = this.appRealmInfo.appconfig?this.appRealmInfo.appconfig.themeoptions:this.defaultAppThemeOptions;
            this.refreshEvery = this.appRealmInfo.appconfig?this.appRealmInfo.appconfig.refreshevery:null;
            this.tokenTTL = this.appRealmInfo.appconfig?this.appRealmInfo.appconfig.tokenttl:this.tokenTTL;
            //store in storage before forming styles
            this.storage.set (this.storageKey, JSON.stringify(this.appRealmInfo));
            //create home page style
            this.formHomePageStyle();
            //..and also register to refresh every xx hours
            this.autoRefreshSubscription = Observable.timer(this.refreshEvery.sections,this.refreshEvery.sections).subscribe((t)=>{
              this.refreshApp();
            })
            return Observable.of(this.appRealmInfo);
          } else {
            return Observable.throw(error);
          }
        });
      }
    });
  }

  //get app section info from server
  public refreshApp () {
    this.appuserApi.getAppInfo(this.realm).subscribe((resp) => {
      this.storage.get(this.storageKey).then((storedRealmInfo)=>{
        //update parameter and store if there is a change
        if (storedRealmInfo != JSON.stringify(resp.success)) {
          this.appInfoLoadErr = false; //reset appInfoLoadErr, this way even if first attempt was a failure in loadApp, this resets it
          this.appRealmInfo = resp.success;
          this.appAllSections = this.refreshSectionPointers(this.appRealmInfo.apprealmsections);
          this.appThemeOptions = this.appRealmInfo.appconfig?this.appRealmInfo.appconfig.themeoptions:this.defaultAppThemeOptions;
          this.refreshEvery = this.appRealmInfo.appconfig?this.appRealmInfo.appconfig.refreshevery:null;
          this.tokenTTL = this.appRealmInfo.appconfig?this.appRealmInfo.appconfig.tokenttl:this.tokenTTL;
          //store in storage before forming styles
          this.storage.set (this.storageKey, JSON.stringify(this.appRealmInfo));
          //create home page style
          this.formHomePageStyle();
          //re-subscribe to auto refresh based on new timing..
          if (this.autoRefreshSubscription) this.autoRefreshSubscription.unsubscribe();
          this.autoRefreshSubscription = Observable.timer(this.refreshEvery.sections,this.refreshEvery.sections).subscribe((t)=>{
            this.refreshApp();
          })
        } else {
          //console.log("nothing has changed..")
        }
      });
    }, (err) => {
      // do nothing as this is just a refresh attempt
    });
  }

  //add additional view sections and default icons as needed
  private refreshSectionPointers (appsections: any): any {
    let pushInd: boolean = false;
    let mgBookInd: boolean = false;
    for (let section of appsections) {
      if (section.module === 'Push') {
        this.appPushSection = section;
        pushInd = true;
      } else if (section.module === 'Manage Booking') {
        this.appManageBookingSection = section;
        mgBookInd = true;
      }
    }
    if (!pushInd) this.appPushSection = null;
    if (!mgBookInd) this.appManageBookingSection = null;
    return appsections;
  }

  //homepage style image download logic
  private getImageUrl (imgname: string): any {
    //check for image in data directory
    return this.fileService.checkDataDirectory(imgname)
    .then((url)=>{
      return url;
    }, (err)=>{
      //if not present in data directory,
      //check if image is available in img folder
      let url = null; //TODO - replace null with url for default image
      for (let i=0; i<this.clientConfig.assetImages.length; i++) {
        if (this.clientConfig.assetImages[i].name == imgname) {
          url = this.clientConfig.assetImages[i].path;
          break;
        }
      }
      //if not fount, initiate download and return default url
      if (!url) this.downloadImage (imgname);
      return url;
    })
  }

  private downloadImage (imgname: string) {
    //get signed url from server for the image to be downloaded
    this.appuserApi.getFromImageRepositorySignedUrl(this.realm, [this.realm + "/imagerepository/Large/" + imgname]).subscribe((res)=>{
      if (res.success && res.success.length) {
        //then download to data directory
        this.fileService.downloadToDataDirectory(res.success[0].url, imgname).then((fileEntry)=>{
          //on completion of successful download of image, refresh homepage style
          setTimeout(() => {this.formHomePageStyle()}, 3000);
          setTimeout(() => {this.formHomePageStyle()}, 30000);
          //the timeot above is because, without it, the styles are refreshed even before the images are fully written on disk, so adding above helps wait for a few seconds before refreshing styles
        },(err)=>{
          // do nothing - as everytime homepage style is formed, if image doesnt exist, an attempt to download will be made. so no need to retry.
        })
      } else {
        // do nothing - as everytime homepage style is formed, if image doesnt exist, an attempt to download will be made. so no need to retry.
      }
    }, (err) => {
      // do nothing - as everytime homepage style is formed, if image doesnt exist, an attempt to download will be made. so no need to retry.
    });
  }

  //form home page styles
  private formHomePageStyle (): void {
    console.log(this)
    //get home page background
    //TODO: we need to get list of available images in img forlder from client config
    //if not present, then we have to attempt to get image from app media folder
    //if not present, then we have to download the image to app media folder, while using default image
    if (this.appThemeOptions.homebgimage) {
      this.getImageUrl(this.appThemeOptions.homebgimage)
      .then((url) => {
        if (this.homeStyle ['background-image'] != 'url('+ url + ')') this.homeStyle ['background-image'] = 'url('+ url + ')';
      });
    } else {
      this.homeStyle ['background-image'] = null;
    }
    if (this.appThemeOptions.homebgcolor) this.homeStyle ['background-color'] = this.themeColorConfig.themeColors[this.appThemeOptions.homebgcolor].base;
    else this.homeStyle ['background-color'] = null;



    //get home partial style
    if (!this.appThemeOptions.homepartialheight) {
      this.homePartialStyle ['height'] = 0;
      this.homePartialStyle ['min-height'] = 0;
      this.homePartialStyle ['max-height'] = 0;
    } else {
      this.homePartialStyle ['height'] = this.appThemeOptions.homepartialheight*100 + "%";
      this.homePartialStyle ['min-height'] = this.appThemeOptions.homepartialheight*100 + "%";
      this.homePartialStyle ['max-height'] = this.appThemeOptions.homepartialheight*100 + "%";
    }

    if (this.appThemeOptions.homepartialbgimage) {
      this.getImageUrl(this.appThemeOptions.homepartialbgimage)
      .then((url)=> {
        if (this.homePartialStyle ['background-image'] != 'url('+ url + ')') this.homePartialStyle ['background-image'] = 'url('+ url + ')';
      });
    } else {
      this.homePartialStyle ['background-image'] = null;
    }
    if (this.appThemeOptions.homepartialbgcolor) this.homePartialStyle ['background-color'] = this.themeColorConfig.getrgba(this.appThemeOptions.homepartialbgcolor, this.appThemeOptions.homepartialbgopacity);
    else this.homePartialStyle ['background-color'] = null;

    //now get partial logo style
    if (this.appThemeOptions.homepartiallogo) {
      this.getImageUrl(this.appThemeOptions.homepartiallogo)
      .then ((url)=>{
        if (this.homePartialLogoStyle ['background-image'] != 'url('+ url + ')') this.homePartialLogoStyle ['background-image'] = 'url('+ url + ')';
      })
      //also set the height and width to that to be of its parent div, as it wont have any content
      this.homePartialLogoStyle ['height'] = '90%';
      this.homePartialLogoStyle ['min-height'] = '90%';
      this.homePartialLogoStyle ['max-height'] = '90%';
    } else {
      this.homePartialLogoStyle ['background-image'] = null;
      //if no logo, set the height and width to that to be of its parent div, as it wont have any content
      this.homePartialLogoStyle ['height'] = '0';
      this.homePartialLogoStyle ['min-height'] = '0';
      this.homePartialLogoStyle ['max-height'] = '0';
    }

    //content area style
    let homerowslength = 0;
    if (this.appThemeOptions.homerows) homerowslength = this.appThemeOptions.homerows.length;
    for (let i=0; i<homerowslength; i++) {
      let homerow = this.appThemeOptions.homerows[i];
      if (homerow.mode == 'tiles') {
        for (let j=0; j<homerow.cols.length; j++) {
          for (let k=0; k<homerow.cols[j].cells.length; k++) {

            //adding style elements if not already present - this will happen the first app is loaded, it will take whatevers in themeoptions and will not have  style elements
            if (!homerow.cols[j].cells[k].nameStyle) homerow.cols[j].cells[k].nameStyle = {};
            if (!homerow.cols[j].cells[k].tileImageStyle) homerow.cols[j].cells[k].tileImageStyle = {};
            if (!homerow.cols[j].cells[k].tileStyle) homerow.cols[j].cells[k].tileStyle = {};
            if (!homerow.cols[j].cells[k].badgeStyle) homerow.cols[j].cells[k].badgeStyle = {};
            //adding vertical align attribute
            if (homerow.cols[j].cells[k].tileimage && homerow.cols[j].cells[k].namevalign == 'Middle') homerow.cols[j].cells[k].namevalign = 'Bottom';
            if (homerow.cols[j].cells[k].namevalign == 'Top') {
              homerow.cols[j].cells[k].nameStyle['margin-bottom'] = 'auto';
              //homerow.cols[j].cells[k].badgeStyle['margin-bottom'] = 'auto';
            }
            else if (homerow.cols[j].cells[k].namevalign == 'Middle') {
              homerow.cols[j].cells[k].nameStyle['margin-top'] = 'auto'; homerow.cols[j].cells[k].nameStyle['margin-bottom'] = 'auto';
              //homerow.cols[j].cells[k].badgeStyle['margin-top'] = 'auto'; homerow.cols[j].cells[k].badgeStyle['margin-bottom'] = 'auto';
            }
            else {
              homerow.cols[j].cells[k].nameStyle['margin-top'] = 'auto';
              //homerow.cols[j].cells[k].badgeStyle['margin-top'] = 'auto';
            }
            //adding text-align related attributes
            if (homerow.cols[j].cells[k].namehalign == 'Left') {
              homerow.cols[j].cells[k].nameStyle['align-self'] = 'flex-start'; homerow.cols[j].cells[k].nameStyle['text-align'] = 'left';
              //homerow.cols[j].cells[k].badgeStyle['right'] = '0';
            }
            else if (homerow.cols[j].cells[k].namehalign == 'Right') {
              homerow.cols[j].cells[k].nameStyle['align-self'] = 'flex-end'; homerow.cols[j].cells[k].nameStyle['text-align'] = 'right';
              //homerow.cols[j].cells[k].badgeStyle['left'] = '0';
            }
            else {
              homerow.cols[j].cells[k].nameStyle['align-self'] = 'center'; homerow.cols[j].cells[k].nameStyle['text-align'] = 'center';
              //homerow.cols[j].cells[k].badgeStyle['right'] = '0';
            }
            //adding name font attributes
            if (homerow.cols[j].cells[k].namecolor) homerow.cols[j].cells[k].nameStyle['color'] = this.themeColorConfig.themeColors[homerow.cols[j].cells[k].namecolor].base;
            if (homerow.cols[j].cells[k].namefontweight) homerow.cols[j].cells[k].nameStyle['font-weight'] = homerow.cols[j].cells[k].namefontweight;
            if (homerow.cols[j].cells[k].namefontstyle) homerow.cols[j].cells[k].nameStyle['font-style'] = homerow.cols[j].cells[k].namefontstyle;
            if (homerow.cols[j].cells[k].namebgcolor) homerow.cols[j].cells[k].nameStyle['background-color'] = this.themeColorConfig.getrgba(homerow.cols[j].cells[k].namebgcolor, homerow.cols[j].cells[k].namebgopacity);

            //make a dedicated background if needed for name
            if (homerow.cols[j].cells[k].namebgfullwidth) homerow.cols[j].cells[k].nameStyle ['width'] = '100%';
            //ensure max-width and max-height is set so that long names dont get outside parent div
            //hide, clip and pad to accomodate longer names in smaller tiles
            homerow.cols[j].cells[k].nameStyle ['max-width'] = '100%';
            homerow.cols[j].cells[k].nameStyle ['max-height'] = '100%';
            homerow.cols[j].cells[k].nameStyle ['overflow'] = 'hidden';
            homerow.cols[j].cells[k].nameStyle ['text-overflow'] = 'clip';
            //homerow.cols[j].cells[k].nameStyle ['padding'] = '2.5%';

            //if section name is one line only, then reset text overflow and text wrap
            if (!homerow.cols[j].cells[k].namewrap) {
              homerow.cols[j].cells[k].nameStyle ['text-overflow'] = 'ellipsis';
              homerow.cols[j].cells[k].nameStyle ['white-space'] = 'nowrap';
            } else {
              homerow.cols[j].cells[k].nameStyle ['word-wrap'] = 'break-word';
            }
            //adding top Image and/or bottom Image
            if(homerow.cols[j].cells[k].tileimage) {
              //if tile image is present, then title cannot be longer than 50% of the tile and name vertical alignment cannot be middle (move to bototm if middle)
              homerow.cols[j].cells[k].nameStyle ['max-height'] = '50%';
              this.getImageUrl(homerow.cols[j].cells[k].tileimage)
              .then ((url)=>{
                if (homerow.cols[j].cells[k].tileImageStyle['background-image'] != 'url('+ url + ')') homerow.cols[j].cells[k].tileImageStyle['background-image'] = 'url('+ url + ')';
              })
            } else {
              homerow.cols[j].cells[k].tileImageStyle = null;
            }
            //adding tile background color
            if (homerow.cols[j].cells[k].bgcolor) homerow.cols[j].cells[k].tileStyle['background-color'] = this.themeColorConfig.getrgba(homerow.cols[j].cells[k].bgcolor, homerow.cols[j].cells[k].bgopacity)

            //adding tile background image
            if (homerow.cols[j].cells[k].bgimage) {
              this.getImageUrl(homerow.cols[j].cells[k].bgimage)
              .then ((url)=>{
                if (homerow.cols[j].cells[k].tileStyle['background-image'] != 'url('+ url + ')') homerow.cols[j].cells[k].tileStyle['background-image'] = 'url('+ url + ')';
              })
            }
            //get section info based on section id
            for (let x=0; x<this.appAllSections.length; x++) {
              if (this.appAllSections[x].id == homerow.cols[j].cells[k].sectionid) {
                homerow.cols[j].cells[k].section = this.appAllSections[x];
                break;
              }
            }
            this.getBadgeInfo();

          }
        }
      }
    }
  }

  //get badge data for home page
  public getBadgeInfo () {
    if (this.loopbackAuth.getCurrentUserId()) {
      this.appuserApi.getAppBadgeInfo(this.loopbackAuth.getCurrentUserId(), this.realm).subscribe((resp) => {
        this.appBadgeRefreshFailCount = 0;
        //load the badge info into realmsection details
        if (resp && resp.success) {
          for (let i=0; i<this.appAllSections.length; i++) {
            let iMatchFound = false;
            for (let j=0; j<resp.success.length; j++) {
              if (this.appAllSections[i].id == resp.success[j]._id) {
                this.appAllSections[i].notificationbadgevalue = resp.success[j].unreadintcount;
                iMatchFound = true;
                break;
              }
            }
            if (!iMatchFound) this.appAllSections[i].notificationbadgevalue = null;
          }
        }
      }, (err)=>{
        //forward to login page on 401 error
        if (err && err.statusCode == 401 && this.loginReference && this.navCtrl) {
          this.appBadgeRefreshFailCount++;
          if (this.appBadgeRefreshFailCount==3) {
            this.navCtrl.push(this.loginReference, {popBack: true});
          }
        }
      });
    }
  }

  //unsubscribe to refresh badge Info
  public unsubscribeAutoRefreshBadgeInfo () {
    //reset failure counter
    this.appBadgeRefreshFailCount = 0;
    //re-subscribe to auto refresh based on new timing..
    if (this.badgeRefreshSubscription) this.badgeRefreshSubscription.unsubscribe();
  }

  //subscribe to refresh badge Info
  public subscribeAutoRefreshBadgeInfo () {
    //re-subscribe to auto refresh based on new timing..
    this.unsubscribeAutoRefreshBadgeInfo();
    this.badgeRefreshSubscription = Observable.timer(0,this.refreshEvery.homepagebadges).subscribe((t)=>{
      this.getBadgeInfo();
    })
  }


}
