import { Injectable } from '@angular/core';
import { AppuserApi, LoopBackAuth } from '../../shared/lbsdk/services';
import { Storage } from '@ionic/storage';
import { Device } from '@ionic-native/device';
import { AppConfigInfo } from '../services';


/**
 * This service has functions to retrieve current device info, compare with what is stored in server, and resend to server if something has changed.
 *
 */
@Injectable()
export class DeviceInfoCollector {

  //constructor
  constructor(private storage: Storage,
              private device: Device,
              private appuserApi: AppuserApi,
              private appConfigInfo: AppConfigInfo,
              private loopbackAuth: LoopBackAuth) {

  }

  //keys for all device related info that will be stored locally and in server
  private storageKey = '$DeviceInfo$';
  //push token
  private deviceToken: string = '';

  //get current Device and User Details
  private getCurrentDeviceInfo (): any {
    let currentDeviceInfo: any = {};
    //Device Info
    currentDeviceInfo.devicecordova = this.device.cordova;
    currentDeviceInfo.devicemodel = this.device.model;
    currentDeviceInfo.deviceplatform = this.device.platform? this.device.platform.toUpperCase():"";
    currentDeviceInfo.deviceuuid = this.device.uuid;
    currentDeviceInfo.deviceversion = this.device.version;
    currentDeviceInfo.devicemanufacturer = this.device.manufacturer;
    currentDeviceInfo.deviceisvirtual = this.device.isVirtual;
    currentDeviceInfo.deviceserial = this.device.serial;
    //Push Token
    currentDeviceInfo.devicetoken = this.deviceToken;
    //User Info
    currentDeviceInfo.userid = this.loopbackAuth.getCurrentUserId();
    //if there is a need to send reg info to server, and if the server call fails for some reason..
    //..the indicator below in will be set to false in storedata. See err handler section of storeDeviceInfo server call.
    currentDeviceInfo.storageattemptsuccess = 'Yes';
    return currentDeviceInfo;
  }

  //compare stored data with current details
  public compareAndStoreDeviceInfo (): void {
    //get stored Device Info
    this.storage.get(this.storageKey)
    .then((storedDeviceInfo)=>{
          let currentDeviceInfo: any;
          let currentDeviceInfoJson: any;
          let currentUserId: string = this.loopbackAuth.getCurrentUserId();
          //get stringified current Device Info
          currentDeviceInfo = this.getCurrentDeviceInfo ();
          currentDeviceInfoJson = JSON.stringify(currentDeviceInfo);
          //if there is a difference and if there exists a valid current user id and a valid current device token
          if (currentDeviceInfo && storedDeviceInfo != currentDeviceInfoJson && currentUserId && currentUserId != '' && this.deviceToken && this.deviceToken != '') {
            //call the storeDeviceID function in server to register new details
            this.appuserApi.storeDeviceInfo(currentUserId, this.appConfigInfo.realm, currentDeviceInfo).subscribe((resp) => {
              //on successful callback, update stored data with new data
              this.storage.set (this.storageKey, currentDeviceInfoJson);
            }, (err) => {
              //on error, ensure stored data has storageattemptsuccess as false - this will ensure an attempt next time
              //note - error could be due to invalid authtoken - if so, app will redirect to login page eventually
              storedDeviceInfo = JSON.parse(storedDeviceInfo);
              storedDeviceInfo.storageattemptsuccess = "No";
              //stringify again and store
              storedDeviceInfo = JSON.stringify(storedDeviceInfo);
              this.storage.set (this.storageKey, storedDeviceInfo);
            });
          }
    })
    .catch((err) => console.log("Failed to complete Device Info collection:", err));
  }

  //method to set devicetoken
  public setDeviceToken(token: string = ''): void {
    if (token && token != '') {
      this.deviceToken = token;
      this.compareAndStoreDeviceInfo();
    }
  }

}
