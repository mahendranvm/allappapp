import { Injectable } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { normalizeURL} from 'ionic-angular';

/**
 * Camera Service
 *
 */
@Injectable()
export class CameraService {

  //constructor
  constructor (private camera: Camera) {
  }

  public options: CameraOptions = {
    quality: 90,
    destinationType: this.camera.DestinationType.FILE_URI, //Camera.DestinationType.FILE_URI = 1
    sourceType: this.camera.PictureSourceType.CAMERA, //Camera.PictureSourceType.CAMERA = 1
    allowEdit: false,
    encodingType: this.camera.EncodingType.JPEG, //Camera.EncodingType.JPEG = 0
    targetWidth: 960,
    targetHeight: 720,
    mediaType: this.camera.MediaType.PICTURE, //Camera.MediaType.PICTURE = 0
    saveToPhotoAlbum: true
    //, popoverOptions: CameraPopoverOptions
    //, correctOrientation:true
  };

  public getPicture () {
    return new Promise ((resolve, reject) =>{
      this.camera.getPicture(this.options).then((imageURI) => {
        return resolve(normalizeURL(imageURI));
      }, (err) => {
        return reject(err);
      });
    });
  }



}
