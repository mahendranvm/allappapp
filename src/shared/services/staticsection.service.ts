import { Injectable } from '@angular/core';
import { AppuserApi, LoopBackAuth } from '../../shared/lbsdk/services';
import { FileService } from '../services';
import { ClientConfig } from '../config'
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/of';


/**
 * Gallery Service
 *
 */
@Injectable()
export class StaticSectionService {

  constructor (private appuserApi: AppuserApi, private fileService: FileService,
    private clientConfig: ClientConfig, private loopBackAuth: LoopBackAuth) {

  }

  public getStaticSectionData (sectionid: string) {
    return this.appuserApi.getStaticSectionData (this.loopBackAuth.getCurrentUserId(), null, sectionid);
  }

  //homepage style image download logic
  public getImageUrl (imgname: string): any {
    //check for image in data directory
    return Observable.fromPromise(this.fileService.checkDataDirectory(imgname))
    .map ((url)=>{
      return url;
    })
    .catch ((error) => {
      //if not present in data directory,
      //check if image is available in img folder
      let url = null; //TODO - replace null with url for default image
      for (let i=0; i<this.clientConfig.assetImages.length; i++) {
        if (this.clientConfig.assetImages[i].name == imgname) {
          url = this.clientConfig.assetImages[i].path;
          break;
        }
      }
      //if not found, get signed url and initiate download
      if (url) {
        return Observable.of(url);
      } else {
        //get signed url from server for the image to be downloaded
        return this.appuserApi.getFromImageRepositorySignedUrl(this.clientConfig.realm, [this.clientConfig.realm + "/imagerepository/Large/" + imgname])
        .map((res)=>{
          if (res.success && res.success.length) {
            //initiate download to data directory, return the received signed url
            this.fileService.downloadToDataDirectory(res.success[0].url, imgname);
            return res.success[0].url;
          } else {
            return null;
          }
        });
      }
    });
  }


}
