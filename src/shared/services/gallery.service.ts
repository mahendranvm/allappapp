import { Injectable } from '@angular/core';
import { AppuserApi, LoopBackAuth } from '../../shared/lbsdk/services';

/**
 * Gallery Service
 *
 */
@Injectable()
export class GalleryService {



  constructor (private appuserApi: AppuserApi,
    private loopBackAuth: LoopBackAuth) {

  }

  public getAlbums (sectionid: string) {
    return this.appuserApi.getAlbums (this.loopBackAuth.getCurrentUserId(), null, sectionid);
  }

  public getPicKeys (sectionid: string, albumid: string) {
    return this.appuserApi.listObjectsAWS (this.loopBackAuth.getCurrentUserId(), null, sectionid, albumid);
  }

  public getSignedUrls (sectionid: string, albumid: string, keys: Array<string>) {
    return this.appuserApi.getSignedUrlAWS (this.loopBackAuth.getCurrentUserId(), null, sectionid, albumid, keys);
  }

  public loadMoreUrls(pagination: any, pics: Array<any>, picsDisp: Array<any>, sectionid: string, albumid: string) {
    let keys = [];
    let max = pagination.bookmarkIndex + pagination.paginationLimit;
    if (max>pics.length) max = pics.length;

    for (let i = pagination.bookmarkIndex; i<max; i++) {
      keys.push(pics[i].Key);
      keys.push(pics[i].appGalleryPicKey);
    }

    //call get signed urls service
    return this.getSignedUrls (sectionid, albumid, keys).map((resp)=>{
      //get the signed urls
      resp.success.forEach((image)=>{
        for (let i = pagination.bookmarkIndex; i<max; i++) {
          if (pics[i].Key == image.key) pics[i].src = image.url;
          if (pics[i].appGalleryPicKey == image.key) pics[i].thumb = image.url;
        }
      })
      //add pics with proper urls to the diplay pics list
      for (let i = pagination.bookmarkIndex; i<max; i++) {
        if (pics[i].src && pics[i].thumb) picsDisp.push(pics[i]);
      }
      //reset bookmark
      pagination.bookmarkIndex = pagination.bookmarkIndex + pagination.paginationLimit;
    });
  }

}
