import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';
import { ToastController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

/**
 * This service has functions to check for Internet connectivity
 *
 */
@Injectable()
export class NetworkCheck {

  private errorString: string;

  //constructor
  constructor (private toastCtrl: ToastController, private translateService: TranslateService, private network: Network) {
    this.translateService.use('en');
    this.translateService.get('NO_INTERNET').subscribe((value) => {
      this.errorString = value;
    });
  }

  //check if internet connection is available and display a toast if not
  public checkForNetwork (showToast: boolean = true): boolean {
    if (this.network.type && this.network.type === 'none') {
      //calling the error toaster with no internet info
      let toast = this.toastCtrl.create({
        message: this.errorString,
        duration: 5000,
        position: 'top'
      });
      toast.present();
      return false;
    } else if (this.network.type){
      return true;
    } else {
      return false;
    }
  }

}
