import { Injectable } from '@angular/core';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { ThemeColorConfig } from '../config';
import { DeviceInfoCollector, AppConfigInfo, AppNavService, SpinnerWrapper } from '../services';


/**
 * This service has functions to retrieve current device info, compare with what is stored in server, and resend to server if something has changed.
 *
 */
@Injectable()
export class PushHandler {

  //constructor
  constructor (private themeColorConfig: ThemeColorConfig,
               private push: Push,
               private deviceInfoCollector: DeviceInfoCollector,
               private appNavService: AppNavService,
               private spinnerWrapper: SpinnerWrapper,
               private appConfig: AppConfigInfo) {
  }

  //push instance
  public pushObject: PushObject;

  //reference to page components
  public page: any = {};

  //reference to navCtrl
  public navCtrl: any;

  //init values for push plugin
  public pushoptions: PushOptions = {
      android: {
          senderID  : "1017939246077",
          icon      : "notificon", //bell icon will be the default notification icon. in addition, the app icon will be used as large icon to be shown on lock screens
          //iconColor : this.themeColorConfig.themeColors[this.appConfig.appThemeOptions.color].dark,
          sound     : true,
          vibrate   : true,
          clearNotifications: false,
          forceShow : true //this will show notifications in tray even if app is in foreground and event handler will be called only when user clicks on the notification
      },
      ios: {
          alert: true,
          sound: true
      },
      windows: {

      }
  }

  //Initialize and register for events
  public initialize () {
    try {
      //set iconColor
      this.pushoptions.android.iconColor = this.themeColorConfig.themeColors[this.appConfig.appThemeOptions.color].dark;
      //initialize
      this.pushObject = this.push.init(this.pushoptions);
      //register
      this.pushObject.on('registration').subscribe(registration => this.onRegistration(registration));
      //receive notification
      this.pushObject.on('notification').subscribe(notification => this.onNotification(notification));
      //receive error
      this.pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
    } catch (err) {
      console.log ("Push Init Failed")
    }
  }

  //on registration event handler
  private onRegistration (data: any): void {
    //after push token is generated, store token and other device info
    this.deviceInfoCollector.setDeviceToken(data.registrationId);
  }

  //on notification event handler
  private onNotification (data: any): void {
    //forward to target page
    //no need to check if data.additionalData.foreground is true or not, as we are setting content available to 0 by default..
    //..this means, this event will not be fired without user clicking the message open
    if (data && data.additionalData && !data.additionalData.foreground && data.additionalData.apptargetsectionid && data.additionalData.apptargetsectionid!=""
     && data.additionalData.apptargetsectionmodule && data.additionalData.apptargetsectionmodule!="") {
      //check and open appropriate section
      this.pushGoToTarget(data.additionalData.apptargetsectionid);
    }
    //finish - important to finish for ios if content available is set.
    //currently we are not setting content available, but in future we might set if needed for background processing (eg. refreshing inbox in interact module for new messages)

    let notId = '';
    if (data && data.additionalData && data.additionalData.notId) notId = data.additionalData.notId
    //this.push.finish(() => {}, () => {}, notId);
  }


  pushGoToTarget (targetSectionId) {
    let targetSection = null;
    this.appConfig.appAllSections.forEach((appsection)=>{
      if (appsection.id == targetSectionId) targetSection = appsection;
    })
    if (targetSection) {
      this.spinnerWrapper.show()
      if (targetSection.module=="Interact" || targetSection.module=="Push") this.appNavService.openSection (this.page, targetSection, this.navCtrl, null, true);
      else if (targetSection.module=="Photos") this.appNavService.openSection ({HomePage: this.page.HomePage, GalleryPage: this.page.GalleryPage, AlbumsPage: this.page.AlbumsPage}, targetSection, this.navCtrl, null, true);
      else if (targetSection.module=="Booking") this.appNavService.openSection ({HomePage: this.page.HomePage, SelectServicePage: this.page.SelectServicePage, SearchAvailabilityPage: this.page.SearchAvailabilityPage}, targetSection, this.navCtrl, null, true);
      else if (targetSection.module=="Manage Booking") this.appNavService.openSection ({HomePage: this.page.HomePage, ListBookingPage: this.page.ListBookingPage, ViewBookingDetailsPage: this.page.ViewBookingDetailsPage}, targetSection, this.navCtrl, null, true);
      else if (targetSection.module=="FAQ") this.appNavService.openSection ({HomePage: this.page.HomePage, FAQPage: this.page.FAQPage}, targetSection, this.navCtrl, null, true);
      else if (targetSection.module=="Location") this.appNavService.openSection ({HomePage: this.page.HomePage, LocationPage: this.page.LocationPage}, targetSection, this.navCtrl, null, true);
      else if (targetSection.module=="Info") this.appNavService.openSection ({HomePage: this.page.HomePage, InfoPage: this.page.InfoPage}, targetSection, this.navCtrl, null, true);
      else if (targetSection.module=="List") this.appNavService.openSection ({HomePage: this.page.HomePage, ListPage: this.page.ListPage}, targetSection, this.navCtrl, null, true);
    }
  }


}
