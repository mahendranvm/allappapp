/* tslint:disable */
import { Injectable, Inject, Optional } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { SDKModels } from './SDKModels';
import { BaseLoopBackApi } from '../core/base.service';
import { LoopBackConfig } from '../../lb.config';
import { LoopBackAuth } from '../core/auth.service';
import { LoopBackFilter, SDKToken, AccessToken } from '../../models/BaseModels';
import { ErrorHandler } from '../core/error.service';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Appuser } from '../../models/Appuser';


/**
 * Api services for the `Appuser` model.
 */
@Injectable()
export class AppuserApi extends BaseLoopBackApi {

  constructor(
    @Inject(HttpClient) protected http: HttpClient,
    @Inject(SDKModels) protected models: SDKModels,
    @Inject(LoopBackAuth) protected auth: LoopBackAuth,
    @Optional() @Inject(ErrorHandler) protected errorHandler: ErrorHandler
  ) {
    super(http,  models, auth, errorHandler);
  }

  /**
   * Login a user with username/email and password.
   *
   * @param {string} include Related objects to include in the response. See the description of return value for more details.
   *   Default value: `user`.
   *
   *  - `rememberMe` - `boolean` - Whether the authentication credentials
   *     should be remembered in localStorage across app/browser restarts.
   *     Default: `true`.
   *
   * @param {object} data Request data.
   *
   * This method expects a subset of model properties as request parameters.
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * The response body contains properties of the AccessToken created on login.
   * Depending on the value of `include` parameter, the body may contain additional properties:
   *
   *   - `user` - `U+007BUserU+007D` - Data of the currently logged in user. (`include=user`)
   *
   *
   */
  public login(credentials: any, include: any = 'user', rememberMe: boolean = true, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/login";
    let _routeParams: any = {};
    let _postBody: any = {
      credentials: credentials
    };
    let _urlParams: any = {};
    if (typeof include !== 'undefined' && include !== null) _urlParams.include = include;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders)
      .pipe(
        map(
        (response: any) => {
          response.ttl = parseInt(response.ttl);
          response.rememberMe = rememberMe;
          this.auth.setToken(response);
          return response;
        }
      )
      );
      return result;

  }

  /**
   * Logout a user with access token.
   *
   * @param {object} data Request data.
   *
   * This method does not accept any data. Supply an empty object.
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * This method returns no data.
   */
  public logout(customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/logout";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
       _urlParams.access_token = this.auth.getAccessTokenId();
    this.auth.clear();
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Trigger user's identity verification with configured verifyOptions
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   * This method does not accept any data. Supply an empty object.
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * This method returns no data.
   */
  public verify(id: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/verify";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Change a user's password.
   *
   * @param {object} data Request data.
   *
   *  - `oldPassword` – `{string}` -
   *
   *  - `newPassword` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * This method returns no data.
   */
  public changePassword(oldPassword: any, newPassword: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/change-password";
    let _routeParams: any = {};
    let _postBody: any = {
      data: {
        oldPassword: oldPassword,
        newPassword: newPassword
      }
    };
    let _urlParams: any = {};
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Reset user's password via a password-reset token.
   *
   * @param {object} data Request data.
   *
   *  - `newPassword` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * This method returns no data.
   */
  public setPassword(newPassword: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/reset-password";
    let _routeParams: any = {};
    let _postBody: any = {
      data: {
        newPassword: newPassword
      }
    };
    let _urlParams: any = {};
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get admin app
   *
   * @param {any} id appuser id
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public c_getAdminAppInfo(id: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/c_getAdminAppInfo";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get all apps
   *
   * @param {any} id appuser id
   *
   * @param {string} appid
   *
   * @param {string} appname
   *
   * @param {any} appstatus
   *
   * @param {number} limit
   *
   * @param {number} skip
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public c_getAllApps(id: any, appid: any = {}, appname: any = {}, appstatus: any = {}, limit: any = {}, skip: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/c_getAllApps";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof appid !== 'undefined' && appid !== null) _urlParams.appid = appid;
    if (typeof appname !== 'undefined' && appname !== null) _urlParams.appname = appname;
    if (typeof appstatus !== 'undefined' && appstatus !== null) _urlParams.appstatus = appstatus;
    if (typeof limit !== 'undefined' && limit !== null) _urlParams.limit = limit;
    if (typeof skip !== 'undefined' && skip !== null) _urlParams.skip = skip;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get App Logs
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{any}` -
   */
  public c_getAppLogs(id: any, realm: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/c_getAppLogs";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Create Subscription Plan
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `subscriptionPlanInfo` – `{object}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public c_createSubscriptionPlan(id: any, subscriptionPlanInfo: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/c_createSubscriptionPlan";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof subscriptionPlanInfo !== 'undefined' && subscriptionPlanInfo !== null) _urlParams.subscriptionPlanInfo = subscriptionPlanInfo;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * View Subscription Plans
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   * This method does not accept any data. Supply an empty object.
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{any}` -
   */
  public getSubscriptionPlans(id: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/getSubscriptionPlans";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Update Subscription Plan
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `subscriptionPlanId` – `{string}` -
   *
   *  - `subscriptionPlanInfo` – `{object}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public c_updateSubscriptionPlan(id: any, subscriptionPlanId: any, subscriptionPlanInfo: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/c_updateSubscriptionPlan";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof subscriptionPlanId !== 'undefined' && subscriptionPlanId !== null) _urlParams.subscriptionPlanId = subscriptionPlanId;
    if (typeof subscriptionPlanInfo !== 'undefined' && subscriptionPlanInfo !== null) _urlParams.subscriptionPlanInfo = subscriptionPlanInfo;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Remove Subscription Plan
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `subscriptionPlanId` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public c_removeSubscriptionPlan(id: any, subscriptionPlanId: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/c_removeSubscriptionPlan";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof subscriptionPlanId !== 'undefined' && subscriptionPlanId !== null) _urlParams.subscriptionPlanId = subscriptionPlanId;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Update App Status
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `subscriptiontype` – `{string}` -
   *
   *  - `trialenddate` – `{date}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public c_updateSubscriptionCycle(id: any, realm: any, subscriptiontype: any, trialenddate: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/c_updateSubscriptionCycle";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof subscriptiontype !== 'undefined' && subscriptiontype !== null) _urlParams.subscriptiontype = subscriptiontype;
    if (typeof trialenddate !== 'undefined' && trialenddate !== null) _urlParams.trialenddate = trialenddate;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Update App Status
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `status` – `{string}` -
   *
   *  - `trialenddate` – `{date}` -
   *
   *  - `changereason` – `{string}` -
   *
   *  - `autorenewalchange` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public c_updateAppStatus(id: any, realm: any, status: any, trialenddate: any = {}, changereason: any = {}, autorenewalchange: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/c_updateAppStatus";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof status !== 'undefined' && status !== null) _urlParams.status = status;
    if (typeof trialenddate !== 'undefined' && trialenddate !== null) _urlParams.trialenddate = trialenddate;
    if (typeof changereason !== 'undefined' && changereason !== null) _urlParams.changereason = changereason;
    if (typeof autorenewalchange !== 'undefined' && autorenewalchange !== null) _urlParams.autorenewalchange = autorenewalchange;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get all deleted apps
   *
   * @param {any} id appuser id
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{any}` -
   */
  public c_getAllDeletedApps(id: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/c_getAllDeletedApps";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Update App Status
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public c_deleteApp(id: any, realm: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/c_deleteApp";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Update push keys for an app
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `apnsbundleid` – `{string}` -
   *
   *  - `gcmsenderapikey` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public c_updatePushSettings(id: any, realm: any, apnsbundleid: any = {}, gcmsenderapikey: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/c_updatePushSettings";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof apnsbundleid !== 'undefined' && apnsbundleid !== null) _urlParams.apnsbundleid = apnsbundleid;
    if (typeof gcmsenderapikey !== 'undefined' && gcmsenderapikey !== null) _urlParams.gcmsenderapikey = gcmsenderapikey;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Create new User
   *
   * @param {object} data Request data.
   *
   *  - `credentials` – `{object}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public createUser(credentials: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/createUser";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof credentials !== 'undefined' && credentials !== null) _urlParams.credentials = credentials;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Confirm email verification token
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `email` – `{string}` -
   *
   *  - `token` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public confirmToken(realm: any, email: any, token: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/confirmToken";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof email !== 'undefined' && email !== null) _urlParams.email = email;
    if (typeof token !== 'undefined' && token !== null) _urlParams.token = token;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Resend email verification token
   *
   * @param {string} realm
   *
   * @param {string} email
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public resendVerify(realm: any, email: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/resendVerify";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof email !== 'undefined' && email !== null) _urlParams.email = email;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Password Reset Request
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `email` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public pwdResetreq(realm: any, email: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/pwdResetreq";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof email !== 'undefined' && email !== null) _urlParams.email = email;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Password Reset
   *
   * @param {object} data Request data.
   *
   *  - `credentials` – `{object}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public pwdReset(credentials: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/pwdReset";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof credentials !== 'undefined' && credentials !== null) _urlParams.credentials = credentials;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Update First and Last Name
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `newfirstname` – `{string}` -
   *
   *  - `newlastname` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public updateProfile(id: any, newfirstname: any, newlastname: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/updateProfile";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof newfirstname !== 'undefined' && newfirstname !== null) _urlParams.newfirstname = newfirstname;
    if (typeof newlastname !== 'undefined' && newlastname !== null) _urlParams.newlastname = newlastname;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Update First and Last Name
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `token` – `{string}` -
   *
   *  - `justsignedin` – `{boolean}` -
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_validateOrSetGoogleToken(id: any, token: any, justsignedin: any, realm: any, sectionid: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_validateOrSetGoogleToken";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof token !== 'undefined' && token !== null) _urlParams.token = token;
    if (typeof justsignedin !== 'undefined' && justsignedin !== null) _urlParams.justsignedin = justsignedin;
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Publish App Request
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `subscriptiontype` – `{string}` -
   *
   *  - `planitems` – `{any}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_requestToSubscribe(id: any, realm: any, subscriptiontype: any, planitems: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_requestToSubscribe";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof subscriptiontype !== 'undefined' && subscriptiontype !== null) _urlParams.subscriptiontype = subscriptiontype;
    if (typeof planitems !== 'undefined' && planitems !== null) _urlParams.planitems = planitems;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Update Card
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `stripeToken` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_changeSubscriptionCard(id: any, realm: any, stripeToken: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_changeSubscriptionCard";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof stripeToken !== 'undefined' && stripeToken !== null) _urlParams.stripeToken = stripeToken;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Flip Auto Renewal
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `subscriptiontype` – `{string}` -
   *
   *  - `change` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_changeAutoRenewal(id: any, realm: any, subscriptiontype: any, change: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_changeAutoRenewal";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof subscriptiontype !== 'undefined' && subscriptiontype !== null) _urlParams.subscriptiontype = subscriptiontype;
    if (typeof change !== 'undefined' && change !== null) _urlParams.change = change;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get Latest Subscription
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_getSubscriptionFromStripe(id: any, realm: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getSubscriptionFromStripe";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get Invoice Preview
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @param {string} customerid
   *
   * @param {string} subscriptionid
   *
   * @param {any} planitems
   *
   * @param {number} prorationdate
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_getInvoicePreview(id: any, realm: any, customerid: any, subscriptionid: any, planitems: any, prorationdate: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getInvoicePreview";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof customerid !== 'undefined' && customerid !== null) _urlParams.customerid = customerid;
    if (typeof subscriptionid !== 'undefined' && subscriptionid !== null) _urlParams.subscriptionid = subscriptionid;
    if (typeof planitems !== 'undefined' && planitems !== null) _urlParams.planitems = planitems;
    if (typeof prorationdate !== 'undefined' && prorationdate !== null) _urlParams.prorationdate = prorationdate;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Downgrade to a different set of Subscription plan
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `subscriptiontype` – `{string}` -
   *
   *  - `planitems` – `{any}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_downgradeSubscriptionPlan(id: any, realm: any, subscriptiontype: any, planitems: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_downgradeSubscriptionPlan";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof subscriptiontype !== 'undefined' && subscriptiontype !== null) _urlParams.subscriptiontype = subscriptiontype;
    if (typeof planitems !== 'undefined' && planitems !== null) _urlParams.planitems = planitems;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Upgrade to a different set of Subscription Plans
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `subscriptiontype` – `{string}` -
   *
   *  - `planitems` – `{any}` -
   *
   *  - `prorationdate` – `{number}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_upgradeSubscriptionPlan(id: any, realm: any, subscriptiontype: any, planitems: any, prorationdate: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_upgradeSubscriptionPlan";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof subscriptiontype !== 'undefined' && subscriptiontype !== null) _urlParams.subscriptiontype = subscriptiontype;
    if (typeof planitems !== 'undefined' && planitems !== null) _urlParams.planitems = planitems;
    if (typeof prorationdate !== 'undefined' && prorationdate !== null) _urlParams.prorationdate = prorationdate;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get Last Invoice
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_getLastInvoice(id: any, realm: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getLastInvoice";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Pay Last Invoice
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `invoiceid` – `{string}` -
   *
   *  - `subscriptiontype` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_payNow(id: any, realm: any, invoiceid: any, subscriptiontype: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_payNow";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof invoiceid !== 'undefined' && invoiceid !== null) _urlParams.invoiceid = invoiceid;
    if (typeof subscriptiontype !== 'undefined' && subscriptiontype !== null) _urlParams.subscriptiontype = subscriptiontype;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get all Invoices
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @param {string} startingafter
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_getAllInvoices(id: any, realm: any, startingafter: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getAllInvoices";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof startingafter !== 'undefined' && startingafter !== null) _urlParams.startingafter = startingafter;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get Invoice
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @param {string} invoiceid
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_getInvoice(id: any, realm: any, invoiceid: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getInvoice";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof invoiceid !== 'undefined' && invoiceid !== null) _urlParams.invoiceid = invoiceid;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Store Device Info
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `deviceinfo` – `{object}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public storeDeviceInfo(id: any, realm: any, deviceinfo: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/storeDeviceInfo";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof deviceinfo !== 'undefined' && deviceinfo !== null) _urlParams.deviceinfo = deviceinfo;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Send Push
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `broadcast` – `{boolean}` -
   *
   *  - `pushtousers` – `{any}` -
   *
   *  - `pushmessage` – `{object}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_sendNotification(id: any, realm: any, sectionid: any, broadcast: any = {}, pushtousers: any = {}, pushmessage: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_sendNotification";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof broadcast !== 'undefined' && broadcast !== null) _urlParams.broadcast = broadcast;
    if (typeof pushtousers !== 'undefined' && pushtousers !== null) _urlParams.pushtousers = pushtousers;
    if (typeof pushmessage !== 'undefined' && pushmessage !== null) _urlParams.pushmessage = pushmessage;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Push Status
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @param {string} sectionid
   *
   * @param {number} limit
   *
   * @param {number} skip
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_getPushStatus(id: any, realm: any, sectionid: any, limit: any = {}, skip: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getPushStatus";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof limit !== 'undefined' && limit !== null) _urlParams.limit = limit;
    if (typeof skip !== 'undefined' && skip !== null) _urlParams.skip = skip;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Create Service
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `name` – `{string}` -
   *
   *  - `description` – `{string}` -
   *
   *  - `duration` – `{number}` -
   *
   *  - `contributionfactor` – `{number}` -
   *
   *  - `advancebookingperiod` – `{number}` -
   *
   *  - `srperbooking` – `{number}` -
   *
   *  - `multiunitbooking` – `{boolean}` -
   *
   *  - `maxunitsperbooking` – `{number}` -
   *
   *  - `selectresource` – `{boolean}` -
   *
   *  - `resourcelabel` – `{string}` -
   *
   *  - `calendarbooking` – `{boolean}` -
   *
   *  - `paytobook` – `{string}` -
   *
   *  - `allowcancel` – `{boolean}` -
   *
   *  - `start` – `{date}` -
   *
   *  - `end` – `{date}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_createService(id: any, realm: any, sectionid: any, name: any, description: any = {}, duration: any, contributionfactor: any, advancebookingperiod: any = {}, srperbooking: any = {}, multiunitbooking: any = {}, maxunitsperbooking: any = {}, selectresource: any = {}, resourcelabel: any = {}, calendarbooking: any = {}, paytobook: any = {}, allowcancel: any = {}, start: any = {}, end: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_createService";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof name !== 'undefined' && name !== null) _urlParams.name = name;
    if (typeof description !== 'undefined' && description !== null) _urlParams.description = description;
    if (typeof duration !== 'undefined' && duration !== null) _urlParams.duration = duration;
    if (typeof contributionfactor !== 'undefined' && contributionfactor !== null) _urlParams.contributionfactor = contributionfactor;
    if (typeof advancebookingperiod !== 'undefined' && advancebookingperiod !== null) _urlParams.advancebookingperiod = advancebookingperiod;
    if (typeof srperbooking !== 'undefined' && srperbooking !== null) _urlParams.srperbooking = srperbooking;
    if (typeof multiunitbooking !== 'undefined' && multiunitbooking !== null) _urlParams.multiunitbooking = multiunitbooking;
    if (typeof maxunitsperbooking !== 'undefined' && maxunitsperbooking !== null) _urlParams.maxunitsperbooking = maxunitsperbooking;
    if (typeof selectresource !== 'undefined' && selectresource !== null) _urlParams.selectresource = selectresource;
    if (typeof resourcelabel !== 'undefined' && resourcelabel !== null) _urlParams.resourcelabel = resourcelabel;
    if (typeof calendarbooking !== 'undefined' && calendarbooking !== null) _urlParams.calendarbooking = calendarbooking;
    if (typeof paytobook !== 'undefined' && paytobook !== null) _urlParams.paytobook = paytobook;
    if (typeof allowcancel !== 'undefined' && allowcancel !== null) _urlParams.allowcancel = allowcancel;
    if (typeof start !== 'undefined' && start !== null) _urlParams.start = start;
    if (typeof end !== 'undefined' && end !== null) _urlParams.end = end;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Create Resource
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `name` – `{string}` -
   *
   *  - `title` – `{string}` -
   *
   *  - `description` – `{string}` -
   *
   *  - `start` – `{date}` -
   *
   *  - `end` – `{date}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_createResource(id: any, realm: any, sectionid: any, name: any, title: any = {}, description: any = {}, start: any = {}, end: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_createResource";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof name !== 'undefined' && name !== null) _urlParams.name = name;
    if (typeof title !== 'undefined' && title !== null) _urlParams.title = title;
    if (typeof description !== 'undefined' && description !== null) _urlParams.description = description;
    if (typeof start !== 'undefined' && start !== null) _urlParams.start = start;
    if (typeof end !== 'undefined' && end !== null) _urlParams.end = end;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Link Services with a Resource
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `resourceid` – `{string}` -
   *
   *  - `serviceids` – `{any}` -
   *
   *  - `start` – `{date}` -
   *
   *  - `end` – `{date}` -
   *
   *  - `bookingstartinterval` – `{number}` -
   *
   *  - `capacityschedule` – `{object}` -
   *
   *  - `mode` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_setServiceResourceAvailability(id: any, realm: any, sectionid: any, resourceid: any, serviceids: any, start: any = {}, end: any = {}, bookingstartinterval: any = {}, capacityschedule: any = {}, mode: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_setServiceResourceAvailability";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof resourceid !== 'undefined' && resourceid !== null) _urlParams.resourceid = resourceid;
    if (typeof serviceids !== 'undefined' && serviceids !== null) _urlParams.serviceids = serviceids;
    if (typeof start !== 'undefined' && start !== null) _urlParams.start = start;
    if (typeof end !== 'undefined' && end !== null) _urlParams.end = end;
    if (typeof bookingstartinterval !== 'undefined' && bookingstartinterval !== null) _urlParams.bookingstartinterval = bookingstartinterval;
    if (typeof capacityschedule !== 'undefined' && capacityschedule !== null) _urlParams.capacityschedule = capacityschedule;
    if (typeof mode !== 'undefined' && mode !== null) _urlParams.mode = mode;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get all services for this realm and section
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @param {string} sectionid
   *
   * @param {string} mgbooksectionid
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{any}` -
   */
  public a_getAllServices(id: any, realm: any, sectionid: any, mgbooksectionid: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getAllServices";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof mgbooksectionid !== 'undefined' && mgbooksectionid !== null) _urlParams.mgbooksectionid = mgbooksectionid;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get all resources for this realm
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @param {string} sectionid
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{any}` -
   */
  public a_getAllResources(id: any, realm: any, sectionid: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getAllResources";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get all serviceresources, resources and services for this realm
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @param {string} sectionid
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_getAllServiceResourcesWithResourceAndService(id: any, realm: any, sectionid: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getAllServiceResourcesWithResourceAndService";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get availability for given ServiceResources for a given date
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `mgbooksectionid` – `{string}` -
   *
   *  - `apptimezone` – `{string}` -
   *
   *  - `serviceresourceids` – `{any}` -
   *
   *  - `serviceid` – `{string}` -
   *
   *  - `tzfreequerydate` – `{date}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{any}` -
   */
  public searchSRCalendarForDate(id: any, realm: any = {}, sectionid: any, mgbooksectionid: any = {}, apptimezone: any = {}, serviceresourceids: any = {}, serviceid: any = {}, tzfreequerydate: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/searchSRCalendarForDate";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof mgbooksectionid !== 'undefined' && mgbooksectionid !== null) _urlParams.mgbooksectionid = mgbooksectionid;
    if (typeof apptimezone !== 'undefined' && apptimezone !== null) _urlParams.apptimezone = apptimezone;
    if (typeof serviceresourceids !== 'undefined' && serviceresourceids !== null) _urlParams.serviceresourceids = serviceresourceids;
    if (typeof serviceid !== 'undefined' && serviceid !== null) _urlParams.serviceid = serviceid;
    if (typeof tzfreequerydate !== 'undefined' && tzfreequerydate !== null) _urlParams.tzfreequerydate = tzfreequerydate;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get availability for given ServiceResources for a given date - for admin use
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `mgbooksectionid` – `{string}` -
   *
   *  - `serviceresourceids` – `{any}` -
   *
   *  - `serviceid` – `{string}` -
   *
   *  - `tzfreequerydate` – `{date}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{any}` -
   */
  public a_searchSRCalendarForDate(id: any, realm: any, sectionid: any, mgbooksectionid: any = {}, serviceresourceids: any = {}, serviceid: any = {}, tzfreequerydate: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_searchSRCalendarForDate";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof mgbooksectionid !== 'undefined' && mgbooksectionid !== null) _urlParams.mgbooksectionid = mgbooksectionid;
    if (typeof serviceresourceids !== 'undefined' && serviceresourceids !== null) _urlParams.serviceresourceids = serviceresourceids;
    if (typeof serviceid !== 'undefined' && serviceid !== null) _urlParams.serviceid = serviceid;
    if (typeof tzfreequerydate !== 'undefined' && tzfreequerydate !== null) _urlParams.tzfreequerydate = tzfreequerydate;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Book this ServiceResource, service, date and quantity provided
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `mgbooksectionid` – `{string}` -
   *
   *  - `serviceresourcedetails` – `{any}` -
   *
   *  - `serviceid` – `{string}` -
   *
   *  - `servicename` – `{string}` -
   *
   *  - `calendarbooking` – `{boolean}` -
   *
   *  - `paytobook` – `{string}` -
   *
   *  - `tzfreequerydate` – `{date}` -
   *
   *  - `quantity` – `{number}` -
   *
   *  - `bookedtime` – `{string}` -
   *
   *  - `bookedminfrommidnight` – `{number}` -
   *
   *  - `requestedresources` – `{string}` -
   *
   *  - `assignedresources` – `{string}` -
   *
   *  - `sectionservicelabel` – `{string}` -
   *
   *  - `apptimezone` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public createBooking(id: any, realm: any, sectionid: any, mgbooksectionid: any = {}, serviceresourcedetails: any, serviceid: any, servicename: any, calendarbooking: any, paytobook: any, tzfreequerydate: any, quantity: any, bookedtime: any, bookedminfrommidnight: any, requestedresources: any, assignedresources: any, sectionservicelabel: any = {}, apptimezone: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/createBooking";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof mgbooksectionid !== 'undefined' && mgbooksectionid !== null) _urlParams.mgbooksectionid = mgbooksectionid;
    if (typeof serviceresourcedetails !== 'undefined' && serviceresourcedetails !== null) _urlParams.serviceresourcedetails = serviceresourcedetails;
    if (typeof serviceid !== 'undefined' && serviceid !== null) _urlParams.serviceid = serviceid;
    if (typeof servicename !== 'undefined' && servicename !== null) _urlParams.servicename = servicename;
    if (typeof calendarbooking !== 'undefined' && calendarbooking !== null) _urlParams.calendarbooking = calendarbooking;
    if (typeof paytobook !== 'undefined' && paytobook !== null) _urlParams.paytobook = paytobook;
    if (typeof tzfreequerydate !== 'undefined' && tzfreequerydate !== null) _urlParams.tzfreequerydate = tzfreequerydate;
    if (typeof quantity !== 'undefined' && quantity !== null) _urlParams.quantity = quantity;
    if (typeof bookedtime !== 'undefined' && bookedtime !== null) _urlParams.bookedtime = bookedtime;
    if (typeof bookedminfrommidnight !== 'undefined' && bookedminfrommidnight !== null) _urlParams.bookedminfrommidnight = bookedminfrommidnight;
    if (typeof requestedresources !== 'undefined' && requestedresources !== null) _urlParams.requestedresources = requestedresources;
    if (typeof assignedresources !== 'undefined' && assignedresources !== null) _urlParams.assignedresources = assignedresources;
    if (typeof sectionservicelabel !== 'undefined' && sectionservicelabel !== null) _urlParams.sectionservicelabel = sectionservicelabel;
    if (typeof apptimezone !== 'undefined' && apptimezone !== null) _urlParams.apptimezone = apptimezone;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Admin Create Booking
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `mgbooksectionid` – `{string}` -
   *
   *  - `serviceresourcedetails` – `{any}` -
   *
   *  - `serviceid` – `{string}` -
   *
   *  - `servicename` – `{string}` -
   *
   *  - `calendarbooking` – `{boolean}` -
   *
   *  - `paytobook` – `{string}` -
   *
   *  - `tzfreequerydate` – `{date}` -
   *
   *  - `quantity` – `{number}` -
   *
   *  - `bookedtime` – `{string}` -
   *
   *  - `bookedminfrommidnight` – `{number}` -
   *
   *  - `requestedresources` – `{string}` -
   *
   *  - `assignedresources` – `{string}` -
   *
   *  - `sectionservicelabel` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_createBooking(id: any, realm: any, sectionid: any, mgbooksectionid: any = {}, serviceresourcedetails: any, serviceid: any, servicename: any, calendarbooking: any, paytobook: any, tzfreequerydate: any, quantity: any, bookedtime: any, bookedminfrommidnight: any, requestedresources: any, assignedresources: any, sectionservicelabel: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_createBooking";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof mgbooksectionid !== 'undefined' && mgbooksectionid !== null) _urlParams.mgbooksectionid = mgbooksectionid;
    if (typeof serviceresourcedetails !== 'undefined' && serviceresourcedetails !== null) _urlParams.serviceresourcedetails = serviceresourcedetails;
    if (typeof serviceid !== 'undefined' && serviceid !== null) _urlParams.serviceid = serviceid;
    if (typeof servicename !== 'undefined' && servicename !== null) _urlParams.servicename = servicename;
    if (typeof calendarbooking !== 'undefined' && calendarbooking !== null) _urlParams.calendarbooking = calendarbooking;
    if (typeof paytobook !== 'undefined' && paytobook !== null) _urlParams.paytobook = paytobook;
    if (typeof tzfreequerydate !== 'undefined' && tzfreequerydate !== null) _urlParams.tzfreequerydate = tzfreequerydate;
    if (typeof quantity !== 'undefined' && quantity !== null) _urlParams.quantity = quantity;
    if (typeof bookedtime !== 'undefined' && bookedtime !== null) _urlParams.bookedtime = bookedtime;
    if (typeof bookedminfrommidnight !== 'undefined' && bookedminfrommidnight !== null) _urlParams.bookedminfrommidnight = bookedminfrommidnight;
    if (typeof requestedresources !== 'undefined' && requestedresources !== null) _urlParams.requestedresources = requestedresources;
    if (typeof assignedresources !== 'undefined' && assignedresources !== null) _urlParams.assignedresources = assignedresources;
    if (typeof sectionservicelabel !== 'undefined' && sectionservicelabel !== null) _urlParams.sectionservicelabel = sectionservicelabel;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Cancel Booking and Update Calendar
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `apptimezone` – `{string}` -
   *
   *  - `bookingid` – `{string}` -
   *
   *  - `reason` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public cancelBooking(id: any, realm: any = {}, sectionid: any = {}, apptimezone: any = {}, bookingid: any, reason: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/cancelBooking";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof apptimezone !== 'undefined' && apptimezone !== null) _urlParams.apptimezone = apptimezone;
    if (typeof bookingid !== 'undefined' && bookingid !== null) _urlParams.bookingid = bookingid;
    if (typeof reason !== 'undefined' && reason !== null) _urlParams.reason = reason;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Cancel Booking and Update Calendar
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `bookingid` – `{string}` -
   *
   *  - `reason` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_cancelBooking(id: any, realm: any, sectionid: any, bookingid: any, reason: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_cancelBooking";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof bookingid !== 'undefined' && bookingid !== null) _urlParams.bookingid = bookingid;
    if (typeof reason !== 'undefined' && reason !== null) _urlParams.reason = reason;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * View my bookings
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `history` – `{boolean}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{any}` -
   */
  public viewMyBookings(id: any, realm: any, history: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/viewMyBookings";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof history !== 'undefined' && history !== null) _urlParams.history = history;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get all booking
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @param {string} sectionid
   *
   * @param {boolean} history
   *
   * @param {number} limit
   *
   * @param {number} skip
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{any}` -
   */
  public a_getAllBooking(id: any, realm: any, sectionid: any, history: any = {}, limit: any = {}, skip: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getAllBooking";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof history !== 'undefined' && history !== null) _urlParams.history = history;
    if (typeof limit !== 'undefined' && limit !== null) _urlParams.limit = limit;
    if (typeof skip !== 'undefined' && skip !== null) _urlParams.skip = skip;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get bookable sections
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @param {string} sectionid
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{any}` -
   */
  public a_getBookableSections(id: any, realm: any, sectionid: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getBookableSections";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Update calendar entry for ServiceResource
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `serviceresourceid` – `{string}` -
   *
   *  - `dates` – `{date}` -
   *
   *  - `changeincapacity` – `{any}` -
   *
   *  - `changetype` – `{string}` -
   *
   *  - `bookingstartinterval` – `{number}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_updateServiceResourceCalendarCapacity(id: any, realm: any, sectionid: any, serviceresourceid: any, dates: any, changeincapacity: any, changetype: any, bookingstartinterval: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_updateServiceResourceCalendarCapacity";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof serviceresourceid !== 'undefined' && serviceresourceid !== null) _urlParams.serviceresourceid = serviceresourceid;
    if (typeof dates !== 'undefined' && dates !== null) _urlParams.dates = dates;
    if (typeof changeincapacity !== 'undefined' && changeincapacity !== null) _urlParams.changeincapacity = changeincapacity;
    if (typeof changetype !== 'undefined' && changetype !== null) _urlParams.changetype = changetype;
    if (typeof bookingstartinterval !== 'undefined' && bookingstartinterval !== null) _urlParams.bookingstartinterval = bookingstartinterval;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Update Service
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `serviceid` – `{string}` -
   *
   *  - `newname` – `{string}` -
   *
   *  - `newdescription` – `{string}` -
   *
   *  - `newduration` – `{number}` -
   *
   *  - `newcontributionfactor` – `{number}` -
   *
   *  - `newadvancebookingperiod` – `{number}` -
   *
   *  - `newsrperbooking` – `{number}` -
   *
   *  - `newmultiunitbooking` – `{boolean}` -
   *
   *  - `newmaxunitsperbooking` – `{number}` -
   *
   *  - `newselectresource` – `{boolean}` -
   *
   *  - `newresourcelabel` – `{string}` -
   *
   *  - `newcalendarbooking` – `{boolean}` -
   *
   *  - `newpaytobook` – `{string}` -
   *
   *  - `newallowcancel` – `{boolean}` -
   *
   *  - `newstart` – `{date}` -
   *
   *  - `newend` – `{date}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_updateService(id: any, realm: any, sectionid: any, serviceid: any, newname: any = {}, newdescription: any = {}, newduration: any = {}, newcontributionfactor: any = {}, newadvancebookingperiod: any = {}, newsrperbooking: any = {}, newmultiunitbooking: any = {}, newmaxunitsperbooking: any = {}, newselectresource: any = {}, newresourcelabel: any = {}, newcalendarbooking: any = {}, newpaytobook: any = {}, newallowcancel: any = {}, newstart: any = {}, newend: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_updateService";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof serviceid !== 'undefined' && serviceid !== null) _urlParams.serviceid = serviceid;
    if (typeof newname !== 'undefined' && newname !== null) _urlParams.newname = newname;
    if (typeof newdescription !== 'undefined' && newdescription !== null) _urlParams.newdescription = newdescription;
    if (typeof newduration !== 'undefined' && newduration !== null) _urlParams.newduration = newduration;
    if (typeof newcontributionfactor !== 'undefined' && newcontributionfactor !== null) _urlParams.newcontributionfactor = newcontributionfactor;
    if (typeof newadvancebookingperiod !== 'undefined' && newadvancebookingperiod !== null) _urlParams.newadvancebookingperiod = newadvancebookingperiod;
    if (typeof newsrperbooking !== 'undefined' && newsrperbooking !== null) _urlParams.newsrperbooking = newsrperbooking;
    if (typeof newmultiunitbooking !== 'undefined' && newmultiunitbooking !== null) _urlParams.newmultiunitbooking = newmultiunitbooking;
    if (typeof newmaxunitsperbooking !== 'undefined' && newmaxunitsperbooking !== null) _urlParams.newmaxunitsperbooking = newmaxunitsperbooking;
    if (typeof newselectresource !== 'undefined' && newselectresource !== null) _urlParams.newselectresource = newselectresource;
    if (typeof newresourcelabel !== 'undefined' && newresourcelabel !== null) _urlParams.newresourcelabel = newresourcelabel;
    if (typeof newcalendarbooking !== 'undefined' && newcalendarbooking !== null) _urlParams.newcalendarbooking = newcalendarbooking;
    if (typeof newpaytobook !== 'undefined' && newpaytobook !== null) _urlParams.newpaytobook = newpaytobook;
    if (typeof newallowcancel !== 'undefined' && newallowcancel !== null) _urlParams.newallowcancel = newallowcancel;
    if (typeof newstart !== 'undefined' && newstart !== null) _urlParams.newstart = newstart;
    if (typeof newend !== 'undefined' && newend !== null) _urlParams.newend = newend;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Update Resource Info
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `resourceid` – `{string}` -
   *
   *  - `newname` – `{string}` -
   *
   *  - `newtitle` – `{string}` -
   *
   *  - `newdescription` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_updateResourceInfo(id: any, realm: any, sectionid: any, resourceid: any, newname: any = {}, newtitle: any = {}, newdescription: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_updateResourceInfo";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof resourceid !== 'undefined' && resourceid !== null) _urlParams.resourceid = resourceid;
    if (typeof newname !== 'undefined' && newname !== null) _urlParams.newname = newname;
    if (typeof newtitle !== 'undefined' && newtitle !== null) _urlParams.newtitle = newtitle;
    if (typeof newdescription !== 'undefined' && newdescription !== null) _urlParams.newdescription = newdescription;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Update Resource Start or End
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `resourceid` – `{string}` -
   *
   *  - `newdate` – `{date}` -
   *
   *  - `startorend` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_updateResourceStartOrEnd(id: any, realm: any, sectionid: any, resourceid: any, newdate: any, startorend: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_updateResourceStartOrEnd";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof resourceid !== 'undefined' && resourceid !== null) _urlParams.resourceid = resourceid;
    if (typeof newdate !== 'undefined' && newdate !== null) _urlParams.newdate = newdate;
    if (typeof startorend !== 'undefined' && startorend !== null) _urlParams.startorend = startorend;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Update Service Resource Start or End
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `serviceresourceid` – `{string}` -
   *
   *  - `newdate` – `{date}` -
   *
   *  - `startorend` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_updateServiceResourceStartOrEnd(id: any, realm: any, sectionid: any, serviceresourceid: any, newdate: any, startorend: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_updateServiceResourceStartOrEnd";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof serviceresourceid !== 'undefined' && serviceresourceid !== null) _urlParams.serviceresourceid = serviceresourceid;
    if (typeof newdate !== 'undefined' && newdate !== null) _urlParams.newdate = newdate;
    if (typeof startorend !== 'undefined' && startorend !== null) _urlParams.startorend = startorend;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Create a new message form
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `usermodkey` – `{string}` -
   *
   *  - `name` – `{string}` -
   *
   *  - `fields` – `{any}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_attachMessageForm(id: any, realm: any, sectionid: any, usermodkey: any, name: any = {}, fields: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_attachMessageForm";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof usermodkey !== 'undefined' && usermodkey !== null) _urlParams.usermodkey = usermodkey;
    if (typeof name !== 'undefined' && name !== null) _urlParams.name = name;
    if (typeof fields !== 'undefined' && fields !== null) _urlParams.fields = fields;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Create a new interaction
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `title` – `{string}` -
   *
   *  - `broadcastind` – `{boolean}` -
   *
   *  - `receiverids` – `{any}` -
   *
   *  - `message` – `{any}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public createInteraction(id: any, realm: any = {}, sectionid: any, title: any, broadcastind: any = {}, receiverids: any = {}, message: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/createInteraction";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof title !== 'undefined' && title !== null) _urlParams.title = title;
    if (typeof broadcastind !== 'undefined' && broadcastind !== null) _urlParams.broadcastind = broadcastind;
    if (typeof receiverids !== 'undefined' && receiverids !== null) _urlParams.receiverids = receiverids;
    if (typeof message !== 'undefined' && message !== null) _urlParams.message = message;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Create a new interaction message
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `interactionid` – `{string}` -
   *
   *  - `message` – `{any}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public createInteractionMessage(id: any, realm: any = {}, sectionid: any, interactionid: any, message: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/createInteractionMessage";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof interactionid !== 'undefined' && interactionid !== null) _urlParams.interactionid = interactionid;
    if (typeof message !== 'undefined' && message !== null) _urlParams.message = message;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Create a signed url for interaction message image
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @param {string} sectionid
   *
   * @param {string} interactionid
   *
   * @param {string} fullkey
   *
   * @param {string} imgtype
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public getPutSignedUrlForInteractionMessage(id: any, realm: any = {}, sectionid: any, interactionid: any = {}, fullkey: any, imgtype: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/getPutSignedUrlForInteractionMessage";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof interactionid !== 'undefined' && interactionid !== null) _urlParams.interactionid = interactionid;
    if (typeof fullkey !== 'undefined' && fullkey !== null) _urlParams.fullkey = fullkey;
    if (typeof imgtype !== 'undefined' && imgtype !== null) _urlParams.imgtype = imgtype;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get interactions
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @param {string} sectionid
   *
   * @param {number} limit
   *
   * @param {number} skip
   *
   * @param {number} clienttotalcount
   *
   * @param {string} paginationmode
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public getInteractions(id: any, realm: any = {}, sectionid: any, limit: any, skip: any, clienttotalcount: any = {}, paginationmode: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/getInteractions";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof limit !== 'undefined' && limit !== null) _urlParams.limit = limit;
    if (typeof skip !== 'undefined' && skip !== null) _urlParams.skip = skip;
    if (typeof clienttotalcount !== 'undefined' && clienttotalcount !== null) _urlParams.clienttotalcount = clienttotalcount;
    if (typeof paginationmode !== 'undefined' && paginationmode !== null) _urlParams.paginationmode = paginationmode;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get interaction messages
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @param {string} sectionid
   *
   * @param {string} interactionid
   *
   * @param {number} limit
   *
   * @param {number} skip
   *
   * @param {number} clienttotalcount
   *
   * @param {string} paginationmode
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public getInteractionMessages(id: any, realm: any = {}, sectionid: any, interactionid: any, limit: any, skip: any, clienttotalcount: any = {}, paginationmode: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/getInteractionMessages";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof interactionid !== 'undefined' && interactionid !== null) _urlParams.interactionid = interactionid;
    if (typeof limit !== 'undefined' && limit !== null) _urlParams.limit = limit;
    if (typeof skip !== 'undefined' && skip !== null) _urlParams.skip = skip;
    if (typeof clienttotalcount !== 'undefined' && clienttotalcount !== null) _urlParams.clienttotalcount = clienttotalcount;
    if (typeof paginationmode !== 'undefined' && paginationmode !== null) _urlParams.paginationmode = paginationmode;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Update interaction unread count tracker
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `interactionid` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `reduceby` – `{number}` -
   *
   *  - `lastreadmessagedate` – `{date}` -
   *
   *  - `updateonbookmark` – `{date}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public updateInteractionUserTracker(id: any, realm: any = {}, interactionid: any, sectionid: any, reduceby: any, lastreadmessagedate: any, updateonbookmark: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/updateInteractionUserTracker";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof interactionid !== 'undefined' && interactionid !== null) _urlParams.interactionid = interactionid;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof reduceby !== 'undefined' && reduceby !== null) _urlParams.reduceby = reduceby;
    if (typeof lastreadmessagedate !== 'undefined' && lastreadmessagedate !== null) _urlParams.lastreadmessagedate = lastreadmessagedate;
    if (typeof updateonbookmark !== 'undefined' && updateonbookmark !== null) _urlParams.updateonbookmark = updateonbookmark;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Delete interaction
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `interactionid` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public deleteInteraction(id: any, realm: any = {}, sectionid: any, interactionid: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/deleteInteraction";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof interactionid !== 'undefined' && interactionid !== null) _urlParams.interactionid = interactionid;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Delete interaction message
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `interactionid` – `{string}` -
   *
   *  - `interactionmessageid` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public deleteInteractionMessage(id: any, realm: any = {}, sectionid: any, interactionid: any, interactionmessageid: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/deleteInteractionMessage";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof interactionid !== 'undefined' && interactionid !== null) _urlParams.interactionid = interactionid;
    if (typeof interactionmessageid !== 'undefined' && interactionmessageid !== null) _urlParams.interactionmessageid = interactionmessageid;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get interaction message v2
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `paginationmode` – `{string}` -
   *
   *  - `bookmarkdate` – `{date}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public getInteractionsV2(id: any, realm: any = {}, sectionid: any, paginationmode: any, bookmarkdate: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/getInteractionsV2";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof paginationmode !== 'undefined' && paginationmode !== null) _urlParams.paginationmode = paginationmode;
    if (typeof bookmarkdate !== 'undefined' && bookmarkdate !== null) _urlParams.bookmarkdate = bookmarkdate;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get interaction message v2
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `interactionid` – `{string}` -
   *
   *  - `paginationmode` – `{string}` -
   *
   *  - `bookmarkdate` – `{date}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public getInteractionMessagesV2(id: any, realm: any = {}, sectionid: any, interactionid: any, paginationmode: any, bookmarkdate: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/getInteractionMessagesV2";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof interactionid !== 'undefined' && interactionid !== null) _urlParams.interactionid = interactionid;
    if (typeof paginationmode !== 'undefined' && paginationmode !== null) _urlParams.paginationmode = paginationmode;
    if (typeof bookmarkdate !== 'undefined' && bookmarkdate !== null) _urlParams.bookmarkdate = bookmarkdate;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get interaction message attachment download url
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @param {string} sectionid
   *
   * @param {string} interactionid
   *
   * @param {string} interactionmessageid
   *
   * @param {string} usermodkey
   *
   * @param {string} fullfilekey
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_getSignedUrlForAttachment(id: any, realm: any, sectionid: any, interactionid: any, interactionmessageid: any, usermodkey: any, fullfilekey: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getSignedUrlForAttachment";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof interactionid !== 'undefined' && interactionid !== null) _urlParams.interactionid = interactionid;
    if (typeof interactionmessageid !== 'undefined' && interactionmessageid !== null) _urlParams.interactionmessageid = interactionmessageid;
    if (typeof usermodkey !== 'undefined' && usermodkey !== null) _urlParams.usermodkey = usermodkey;
    if (typeof fullfilekey !== 'undefined' && fullfilekey !== null) _urlParams.fullfilekey = fullfilekey;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Create a new album
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `albumname` – `{string}` -
   *
   *  - `broadcastind` – `{boolean}` -
   *
   *  - `receiverids` – `{any}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_createAlbum(id: any, realm: any, sectionid: any, albumname: any, broadcastind: any = {}, receiverids: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_createAlbum";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof albumname !== 'undefined' && albumname !== null) _urlParams.albumname = albumname;
    if (typeof broadcastind !== 'undefined' && broadcastind !== null) _urlParams.broadcastind = broadcastind;
    if (typeof receiverids !== 'undefined' && receiverids !== null) _urlParams.receiverids = receiverids;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Update an existing album
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `albumid` – `{string}` -
   *
   *  - `albumname` – `{string}` -
   *
   *  - `broadcastind` – `{boolean}` -
   *
   *  - `receiverids` – `{any}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_updateAlbum(id: any, realm: any, sectionid: any, albumid: any, albumname: any, broadcastind: any = {}, receiverids: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_updateAlbum";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof albumid !== 'undefined' && albumid !== null) _urlParams.albumid = albumid;
    if (typeof albumname !== 'undefined' && albumname !== null) _urlParams.albumname = albumname;
    if (typeof broadcastind !== 'undefined' && broadcastind !== null) _urlParams.broadcastind = broadcastind;
    if (typeof receiverids !== 'undefined' && receiverids !== null) _urlParams.receiverids = receiverids;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Remove a new album
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `albumid` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_removeAlbum(id: any, realm: any, sectionid: any, albumid: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_removeAlbum";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof albumid !== 'undefined' && albumid !== null) _urlParams.albumid = albumid;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get URL for adding a new pic
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @param {string} sectionid
   *
   * @param {string} filekeyprefix
   *
   * @param {any} sizeoptions
   *
   * @param {string} imgkey
   *
   * @param {string} imgtype
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{any}` -
   */
  public a_uploadPicUrl(id: any, realm: any, sectionid: any, filekeyprefix: any, sizeoptions: any, imgkey: any, imgtype: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_uploadPicUrl";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof filekeyprefix !== 'undefined' && filekeyprefix !== null) _urlParams.filekeyprefix = filekeyprefix;
    if (typeof sizeoptions !== 'undefined' && sizeoptions !== null) _urlParams.sizeoptions = sizeoptions;
    if (typeof imgkey !== 'undefined' && imgkey !== null) _urlParams.imgkey = imgkey;
    if (typeof imgtype !== 'undefined' && imgtype !== null) _urlParams.imgtype = imgtype;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get albums in section
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @param {string} sectionid
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public getAlbums(id: any, realm: any = {}, sectionid: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/getAlbums";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * List pics in an album
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @param {string} sectionid
   *
   * @param {string} albumid
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public listObjectsAWS(id: any, realm: any = {}, sectionid: any, albumid: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/listObjectsAWS";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof albumid !== 'undefined' && albumid !== null) _urlParams.albumid = albumid;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get url for pics in an album
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `albumid` – `{string}` -
   *
   *  - `fullkeys` – `{any}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public getSignedUrlAWS(id: any, realm: any = {}, sectionid: any, albumid: any, fullkeys: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/getSignedUrlAWS";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof albumid !== 'undefined' && albumid !== null) _urlParams.albumid = albumid;
    if (typeof fullkeys !== 'undefined' && fullkeys !== null) _urlParams.fullkeys = fullkeys;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Remove pics from an album
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `fullkeys` – `{any}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public removePics(id: any, realm: any = {}, sectionid: any, fullkeys: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/removePics";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof fullkeys !== 'undefined' && fullkeys !== null) _urlParams.fullkeys = fullkeys;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Create a new section in the app
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionname` – `{string}` -
   *
   *  - `appmodule` – `{string}` -
   *
   *  - `usermodulename` – `{string}` -
   *
   *  - `usermodkey` – `{string}` -
   *
   *  - `details` – `{object}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_addAppSection(id: any, realm: any, sectionname: any, appmodule: any, usermodulename: any, usermodkey: any, details: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_addAppSection";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionname !== 'undefined' && sectionname !== null) _urlParams.sectionname = sectionname;
    if (typeof appmodule !== 'undefined' && appmodule !== null) _urlParams.appmodule = appmodule;
    if (typeof usermodulename !== 'undefined' && usermodulename !== null) _urlParams.usermodulename = usermodulename;
    if (typeof usermodkey !== 'undefined' && usermodkey !== null) _urlParams.usermodkey = usermodkey;
    if (typeof details !== 'undefined' && details !== null) _urlParams.details = details;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Update an existing section in the app
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `sectionname` – `{string}` -
   *
   *  - `appmodule` – `{string}` -
   *
   *  - `usermodulename` – `{string}` -
   *
   *  - `usermodkey` – `{string}` -
   *
   *  - `details` – `{object}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_updateAppSection(id: any, realm: any, sectionid: any, sectionname: any, appmodule: any, usermodulename: any, usermodkey: any, details: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_updateAppSection";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof sectionname !== 'undefined' && sectionname !== null) _urlParams.sectionname = sectionname;
    if (typeof appmodule !== 'undefined' && appmodule !== null) _urlParams.appmodule = appmodule;
    if (typeof usermodulename !== 'undefined' && usermodulename !== null) _urlParams.usermodulename = usermodulename;
    if (typeof usermodkey !== 'undefined' && usermodkey !== null) _urlParams.usermodkey = usermodkey;
    if (typeof details !== 'undefined' && details !== null) _urlParams.details = details;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Remove an existing section in the app
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `change` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_enableDisableAppSection(id: any, realm: any, sectionid: any, change: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_enableDisableAppSection";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof change !== 'undefined' && change !== null) _urlParams.change = change;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Remove an existing section in the app
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `sectionname` – `{string}` -
   *
   *  - `appmodule` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_removeAppSection(id: any, realm: any, sectionid: any, sectionname: any, appmodule: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_removeAppSection";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof sectionname !== 'undefined' && sectionname !== null) _urlParams.sectionname = sectionname;
    if (typeof appmodule !== 'undefined' && appmodule !== null) _urlParams.appmodule = appmodule;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Add a new role
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `usermodkey` – `{string}` -
   *
   *  - `archiveind` – `{boolean}` -
   *
   *  - `viewname` – `{string}` -
   *
   *  - `dynamicquery` – `{object}` -
   *
   *  - `limit` – `{number}` -
   *
   *  - `skip` – `{number}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_getCustomEntityData(id: any, realm: any, sectionid: any, usermodkey: any, archiveind: any = {}, viewname: any = {}, dynamicquery: any = {}, limit: any = {}, skip: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getCustomEntityData";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof usermodkey !== 'undefined' && usermodkey !== null) _urlParams.usermodkey = usermodkey;
    if (typeof archiveind !== 'undefined' && archiveind !== null) _urlParams.archiveind = archiveind;
    if (typeof viewname !== 'undefined' && viewname !== null) _urlParams.viewname = viewname;
    if (typeof dynamicquery !== 'undefined' && dynamicquery !== null) _urlParams.dynamicquery = dynamicquery;
    if (typeof limit !== 'undefined' && limit !== null) _urlParams.limit = limit;
    if (typeof skip !== 'undefined' && skip !== null) _urlParams.skip = skip;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Add a new role
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `relatedsectiondata` – `{any}` -
   *
   *  - `fielddata` – `{any}` -
   *
   *  - `usermodkey` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_addCustomEntityData(id: any, realm: any, sectionid: any, relatedsectiondata: any = {}, fielddata: any, usermodkey: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_addCustomEntityData";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof relatedsectiondata !== 'undefined' && relatedsectiondata !== null) _urlParams.relatedsectiondata = relatedsectiondata;
    if (typeof fielddata !== 'undefined' && fielddata !== null) _urlParams.fielddata = fielddata;
    if (typeof usermodkey !== 'undefined' && usermodkey !== null) _urlParams.usermodkey = usermodkey;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Add a new role
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `entityid` – `{string}` -
   *
   *  - `relatedsectiondata` – `{any}` -
   *
   *  - `fielddata` – `{any}` -
   *
   *  - `usermodkey` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_editCustomEntityData(id: any, realm: any, sectionid: any, entityid: any, relatedsectiondata: any = {}, fielddata: any, usermodkey: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_editCustomEntityData";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof entityid !== 'undefined' && entityid !== null) _urlParams.entityid = entityid;
    if (typeof relatedsectiondata !== 'undefined' && relatedsectiondata !== null) _urlParams.relatedsectiondata = relatedsectiondata;
    if (typeof fielddata !== 'undefined' && fielddata !== null) _urlParams.fielddata = fielddata;
    if (typeof usermodkey !== 'undefined' && usermodkey !== null) _urlParams.usermodkey = usermodkey;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Add a new role
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `entityid` – `{string}` -
   *
   *  - `usermodkey` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_deleteCustomEntityData(id: any, realm: any, sectionid: any, entityid: any, usermodkey: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_deleteCustomEntityData";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof entityid !== 'undefined' && entityid !== null) _urlParams.entityid = entityid;
    if (typeof usermodkey !== 'undefined' && usermodkey !== null) _urlParams.usermodkey = usermodkey;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Add a new role
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `entityid` – `{string}` -
   *
   *  - `usermodkey` – `{string}` -
   *
   *  - `archiveind` – `{boolean}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_archiveUpdateCustomEntityData(id: any, realm: any, sectionid: any, entityid: any, usermodkey: any, archiveind: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_archiveUpdateCustomEntityData";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof entityid !== 'undefined' && entityid !== null) _urlParams.entityid = entityid;
    if (typeof usermodkey !== 'undefined' && usermodkey !== null) _urlParams.usermodkey = usermodkey;
    if (typeof archiveind !== 'undefined' && archiveind !== null) _urlParams.archiveind = archiveind;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Add a new attachment
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `filename` – `{string}` -
   *
   *  - `filetype` – `{string}` -
   *
   *  - `usermodkey` – `{string}` -
   *
   *  - `entityid` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_addCEDraftAttachment(id: any, realm: any, sectionid: any, filename: any, filetype: any = {}, usermodkey: any, entityid: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_addCEDraftAttachment";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof filename !== 'undefined' && filename !== null) _urlParams.filename = filename;
    if (typeof filetype !== 'undefined' && filetype !== null) _urlParams.filetype = filetype;
    if (typeof usermodkey !== 'undefined' && usermodkey !== null) _urlParams.usermodkey = usermodkey;
    if (typeof entityid !== 'undefined' && entityid !== null) _urlParams.entityid = entityid;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Add a new attachment
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `sourceattachments` – `{any}` -
   *
   *  - `usermodkey` – `{string}` -
   *
   *  - `entityid` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{any}` -
   */
  public a_copyCEDraftAttachment(id: any, realm: any, sectionid: any, sourceattachments: any, usermodkey: any, entityid: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_copyCEDraftAttachment";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof sourceattachments !== 'undefined' && sourceattachments !== null) _urlParams.sourceattachments = sourceattachments;
    if (typeof usermodkey !== 'undefined' && usermodkey !== null) _urlParams.usermodkey = usermodkey;
    if (typeof entityid !== 'undefined' && entityid !== null) _urlParams.entityid = entityid;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Discard a new attachment
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `delids` – `{any}` -
   *
   *  - `usermodkey` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_discardCEDraftAttachment(id: any, realm: any, sectionid: any, delids: any, usermodkey: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_discardCEDraftAttachment";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof delids !== 'undefined' && delids !== null) _urlParams.delids = delids;
    if (typeof usermodkey !== 'undefined' && usermodkey !== null) _urlParams.usermodkey = usermodkey;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Download attachment
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `entityid` – `{string}` -
   *
   *  - `fieldcmdbname` – `{string}` -
   *
   *  - `attachmentid` – `{string}` -
   *
   *  - `usermodkey` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_downloadCustomEntityAttachment(id: any, realm: any, sectionid: any, entityid: any, fieldcmdbname: any, attachmentid: any, usermodkey: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_downloadCustomEntityAttachment";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof entityid !== 'undefined' && entityid !== null) _urlParams.entityid = entityid;
    if (typeof fieldcmdbname !== 'undefined' && fieldcmdbname !== null) _urlParams.fieldcmdbname = fieldcmdbname;
    if (typeof attachmentid !== 'undefined' && attachmentid !== null) _urlParams.attachmentid = attachmentid;
    if (typeof usermodkey !== 'undefined' && usermodkey !== null) _urlParams.usermodkey = usermodkey;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Add a new role
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `customentityviews` – `{any}` -
   *
   *  - `usermodkey` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_updateCustomEntityViews(id: any, realm: any, sectionid: any, customentityviews: any, usermodkey: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_updateCustomEntityViews";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof customentityviews !== 'undefined' && customentityviews !== null) _urlParams.customentityviews = customentityviews;
    if (typeof usermodkey !== 'undefined' && usermodkey !== null) _urlParams.usermodkey = usermodkey;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Post Custom Entity to another section
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `entityid` – `{string}` -
   *
   *  - `viewname` – `{string}` -
   *
   *  - `posttosectionid` – `{string}` -
   *
   *  - `title` – `{string}` -
   *
   *  - `broadcastind` – `{boolean}` -
   *
   *  - `receiverids` – `{any}` -
   *
   *  - `usermodkey` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_shareCustomEntityViewForComments(id: any, realm: any, sectionid: any, entityid: any, viewname: any, posttosectionid: any, title: any, broadcastind: any = {}, receiverids: any = {}, usermodkey: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_shareCustomEntityViewForComments";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof entityid !== 'undefined' && entityid !== null) _urlParams.entityid = entityid;
    if (typeof viewname !== 'undefined' && viewname !== null) _urlParams.viewname = viewname;
    if (typeof posttosectionid !== 'undefined' && posttosectionid !== null) _urlParams.posttosectionid = posttosectionid;
    if (typeof title !== 'undefined' && title !== null) _urlParams.title = title;
    if (typeof broadcastind !== 'undefined' && broadcastind !== null) _urlParams.broadcastind = broadcastind;
    if (typeof receiverids !== 'undefined' && receiverids !== null) _urlParams.receiverids = receiverids;
    if (typeof usermodkey !== 'undefined' && usermodkey !== null) _urlParams.usermodkey = usermodkey;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Add a new role
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `name` – `{string}` -
   *
   *  - `realm` – `{string}` -
   *
   *  - `rights` – `{any}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_addRole(id: any, name: any, realm: any, rights: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_addRole";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof name !== 'undefined' && name !== null) _urlParams.name = name;
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof rights !== 'undefined' && rights !== null) _urlParams.rights = rights;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Remove role
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `roleid` – `{string}` -
   *
   *  - `realm` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_removeRole(id: any, roleid: any, realm: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_removeRole";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof roleid !== 'undefined' && roleid !== null) _urlParams.roleid = roleid;
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Edit role
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `roleid` – `{string}` -
   *
   *  - `name` – `{string}` -
   *
   *  - `realm` – `{string}` -
   *
   *  - `rights` – `{any}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_editRole(id: any, roleid: any, name: any, realm: any, rights: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_editRole";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof roleid !== 'undefined' && roleid !== null) _urlParams.roleid = roleid;
    if (typeof name !== 'undefined' && name !== null) _urlParams.name = name;
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof rights !== 'undefined' && rights !== null) _urlParams.rights = rights;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get roles
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{any}` -
   */
  public a_getRoles(id: any, realm: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getRoles";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Add staff
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `staffemail` – `{string}` -
   *
   *  - `roleid` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_addStaff(id: any, realm: any, staffemail: any, roleid: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_addStaff";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof staffemail !== 'undefined' && staffemail !== null) _urlParams.staffemail = staffemail;
    if (typeof roleid !== 'undefined' && roleid !== null) _urlParams.roleid = roleid;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Remove staff
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `staffid` – `{string}` -
   *
   *  - `realm` – `{string}` -
   *
   *  - `staffemail` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_removeStaff(id: any, staffid: any, realm: any, staffemail: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_removeStaff";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof staffid !== 'undefined' && staffid !== null) _urlParams.staffid = staffid;
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof staffemail !== 'undefined' && staffemail !== null) _urlParams.staffemail = staffemail;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Edit staff
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `staffid` – `{string}` -
   *
   *  - `realm` – `{string}` -
   *
   *  - `staffemail` – `{string}` -
   *
   *  - `roleid` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_editStaff(id: any, staffid: any, realm: any, staffemail: any, roleid: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_editStaff";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof staffid !== 'undefined' && staffid !== null) _urlParams.staffid = staffid;
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof staffemail !== 'undefined' && staffemail !== null) _urlParams.staffemail = staffemail;
    if (typeof roleid !== 'undefined' && roleid !== null) _urlParams.roleid = roleid;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get staff and roles
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_getStaffAndRoles(id: any, realm: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getStaffAndRoles";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Accept staff invite
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `staffid` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_acceptStaffInvitation(id: any, staffid: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_acceptStaffInvitation";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof staffid !== 'undefined' && staffid !== null) _urlParams.staffid = staffid;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Send reminder staff invite
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `staffid` – `{string}` -
   *
   *  - `realm` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_reminderStaffInvitation(id: any, staffid: any, realm: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_reminderStaffInvitation";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof staffid !== 'undefined' && staffid !== null) _urlParams.staffid = staffid;
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Add a new app
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `appname` – `{string}` -
   *
   *  - `apptimezone` – `{string}` -
   *
   *  - `adminsitemanagestaff` – `{boolean}` -
   *
   *  - `adminsitemanagecustomerapp` – `{boolean}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_createApp(id: any, appname: any, apptimezone: any, adminsitemanagestaff: any, adminsitemanagecustomerapp: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_createApp";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof appname !== 'undefined' && appname !== null) _urlParams.appname = appname;
    if (typeof apptimezone !== 'undefined' && apptimezone !== null) _urlParams.apptimezone = apptimezone;
    if (typeof adminsitemanagestaff !== 'undefined' && adminsitemanagestaff !== null) _urlParams.adminsitemanagestaff = adminsitemanagestaff;
    if (typeof adminsitemanagecustomerapp !== 'undefined' && adminsitemanagecustomerapp !== null) _urlParams.adminsitemanagecustomerapp = adminsitemanagecustomerapp;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Update an existing app
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `newappname` – `{string}` -
   *
   *  - `newapptimezone` – `{string}` -
   *
   *  - `newappconfig` – `{object}` -
   *
   *  - `newadminsitemanagestaff` – `{boolean}` -
   *
   *  - `newadminsitemanagecustomerapp` – `{boolean}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_updateApp(id: any, realm: any, newappname: any = {}, newapptimezone: any = {}, newappconfig: any = {}, newadminsitemanagestaff: any = {}, newadminsitemanagecustomerapp: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_updateApp";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof newappname !== 'undefined' && newappname !== null) _urlParams.newappname = newappname;
    if (typeof newapptimezone !== 'undefined' && newapptimezone !== null) _urlParams.newapptimezone = newapptimezone;
    if (typeof newappconfig !== 'undefined' && newappconfig !== null) _urlParams.newappconfig = newappconfig;
    if (typeof newadminsitemanagestaff !== 'undefined' && newadminsitemanagestaff !== null) _urlParams.newadminsitemanagestaff = newadminsitemanagestaff;
    if (typeof newadminsitemanagecustomerapp !== 'undefined' && newadminsitemanagecustomerapp !== null) _urlParams.newadminsitemanagecustomerapp = newadminsitemanagecustomerapp;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Request to Publish an existing app
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `status` – `{string}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_updateAppStatus(id: any, realm: any, status: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_updateAppStatus";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof status !== 'undefined' && status !== null) _urlParams.status = status;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get my apps
   *
   * @param {any} id appuser id
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{any}` -
   */
  public a_getMyApps(id: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getMyApps";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get my app detail
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{any}` -
   */
  public a_getMyAppDetail(id: any, realm: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getMyAppDetail";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Mark App for Deletion
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_markAppForDeletion(id: any, realm: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_markAppForDeletion";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Undo App deletion
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_undoAppDeletion(id: any, realm: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_undoAppDeletion";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get my deleted apps
   *
   * @param {any} id appuser id
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{any}` -
   */
  public a_getMyDeletedApps(id: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getMyDeletedApps";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get registered users of given app
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @param {string} sectionid
   *
   * @param {string} usermodkey
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{any}` -
   */
  public a_getRegisteredAppUsers(id: any, realm: any, sectionid: any = {}, usermodkey: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getRegisteredAppUsers";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof usermodkey !== 'undefined' && usermodkey !== null) _urlParams.usermodkey = usermodkey;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get registered users of given app
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @param {string} sectionid
   *
   * @param {string} usermodkey
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{any}` -
   */
  public a_getRecipientUsers(id: any, realm: any, sectionid: any = {}, usermodkey: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getRecipientUsers";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof usermodkey !== 'undefined' && usermodkey !== null) _urlParams.usermodkey = usermodkey;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get registered users count of given app
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_getRegisteredAppUsersCount(id: any, realm: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getRegisteredAppUsersCount";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get app info
   *
   * @param {string} realm
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public getAppInfo(realm: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/getAppInfo";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get app booking section details
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @param {string} sectionid
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public getBookingSectionWithServiceDetails(id: any, realm: any, sectionid: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/getBookingSectionWithServiceDetails";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get list of pics from image repository
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_getFromImageRepositoryListObjects(id: any, realm: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getFromImageRepositoryListObjects";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get splash screen image url
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_getAppSplashScreenSignedUrl(id: any, realm: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getAppSplashScreenSignedUrl";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get app icon image url
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_getAppIconImageSignedUrl(id: any, realm: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getAppIconImageSignedUrl";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get signed urls for pics
   *
   * @param {string} realm
   *
   * @param {any} fullkeys
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public getFromImageRepositorySignedUrl(realm: any, fullkeys: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/getFromImageRepositorySignedUrl";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof fullkeys !== 'undefined' && fullkeys !== null) _urlParams.fullkeys = fullkeys;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get data for given static section
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @param {string} sectionid
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public getStaticSectionData(id: any, realm: any = {}, sectionid: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/getStaticSectionData";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Update Static Section Data - For use in 1 record per section type sections
   *
   * @param {any} id appuser id
   *
   * @param {object} data Request data.
   *
   *  - `realm` – `{string}` -
   *
   *  - `sectionid` – `{string}` -
   *
   *  - `sectiondata` – `{any}` -
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public a_updateStaticSectionData(id: any, realm: any, sectionid: any, sectiondata: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_updateStaticSectionData";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof sectionid !== 'undefined' && sectionid !== null) _urlParams.sectionid = sectionid;
    if (typeof sectiondata !== 'undefined' && sectiondata !== null) _urlParams.sectiondata = sectiondata;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get Data for Dashboard
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @param {string} pushsectionid
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{any}` -
   */
  public a_getDashboardDetails(id: any, realm: any, pushsectionid: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/a_getDashboardDetails";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof pushsectionid !== 'undefined' && pushsectionid !== null) _urlParams.pushsectionid = pushsectionid;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Get Data for Dashboard
   *
   * @param {any} id appuser id
   *
   * @param {string} realm
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public getAppBadgeInfo(id: any, realm: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/:id/getAppBadgeInfo";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Test function to get verification token
   *
   * @param {string} realm
   *
   * @param {string} email
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` -
   */
  public test_getVerifyToken(realm: any, email: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/appusers/test_getVerifyToken";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof email !== 'undefined' && email !== null) _urlParams.email = email;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }
  /**
   * @ngdoc method
   * @name sdk.Appuser#getCurrent
   * @methodOf sdk.Appuser
   *
   * @description
   *
   * Get data of the currently logged user. Fail with HTTP result 401
   * when there is no user logged in.
   *
   * @returns object An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   */
  public getCurrent(filter: LoopBackFilter = {}): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() + "/appusers" + "/:id";
    let id: any = this.auth.getCurrentUserId();
    if (id == null)
    id = '__anonymous__';
    let _routeParams: any = { id: id };
    let _urlParams: any = {};
    let _postBody: any = {};
    if (filter) _urlParams.filter = filter;
    return this.request(_method, _url, _routeParams, _urlParams, _postBody);
  }
  /**
   * Get data of the currently logged user that was returned by the last
   * call to {@link sdk.Appuser#login} or
   * {@link sdk.Appuser#getCurrent}. Return null when there
   * is no user logged in or the data of the current user were not fetched
   * yet.
   *
   * @returns object An Account instance.
   */
  public getCachedCurrent() {
    return this.auth.getCurrentUserData();
  }
  /**
   * Get data of the currently logged access tokern that was returned by the last
   * call to {@link sdk.Appuser#login}
   *
   * @returns object An AccessToken instance.
   */
  public getCurrentToken(): AccessToken {
    return this.auth.getToken();
  }
  /**
   * @name sdk.Appuser#isAuthenticated
   *
   * @returns {boolean} True if the current user is authenticated (logged in).
   */
  public isAuthenticated() {
    return !(this.getCurrentId() === '' || this.getCurrentId() == null || this.getCurrentId() == 'null');
  }

  /**
   * @name sdk.Appuser#getCurrentId
   *
   * @returns object Id of the currently logged-in user or null.
   */
  public getCurrentId() {
    return this.auth.getCurrentUserId();
  }

  /**
   * The name of the model represented by this $resource,
   * i.e. `Appuser`.
   */
  public getModelName() {
    return "Appuser";
  }
}
