/* tslint:disable */

declare var Object: any;
export interface AppuserInterface {
  "firstname"?: string;
  "lastname"?: string;
  "realm": any;
  "apptimezone": string;
  "adminof"?: Array<any>;
  "staffof"?: Array<any>;
  "customers"?: Array<any>;
  "stripecustomerid"?: string;
  "devices"?: Array<any>;
  "username"?: string;
  "email": string;
  "emailVerified"?: boolean;
  "id"?: any;
  "password"?: string;
  accessTokens?: any[];
  apprealm?: any;
  adminofapps?: any[];
  createdappservices?: any[];
  createdappresources?: any[];
  createdappsrbookings?: any[];
  relatedcustomers?: any[];
}

export class Appuser implements AppuserInterface {
  "firstname": string;
  "lastname": string;
  "realm": any;
  "apptimezone": string;
  "adminof": Array<any>;
  "staffof": Array<any>;
  "customers": Array<any>;
  "stripecustomerid": string;
  "devices": Array<any>;
  "username": string;
  "email": string;
  "emailVerified": boolean;
  "id": any;
  "password": string;
  accessTokens: any[];
  apprealm: any;
  adminofapps: any[];
  createdappservices: any[];
  createdappresources: any[];
  createdappsrbookings: any[];
  relatedcustomers: any[];
  constructor(data?: AppuserInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Appuser`.
   */
  public static getModelName() {
    return "Appuser";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Appuser for dynamic purposes.
  **/
  public static factory(data: AppuserInterface): Appuser{
    return new Appuser(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Appuser',
      plural: 'Appusers',
      path: 'Appusers',
      idName: 'id',
      properties: {
        "firstname": {
          name: 'firstname',
          type: 'string'
        },
        "lastname": {
          name: 'lastname',
          type: 'string'
        },
        "realm": {
          name: 'realm',
          type: 'any'
        },
        "apptimezone": {
          name: 'apptimezone',
          type: 'string'
        },
        "adminof": {
          name: 'adminof',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "staffof": {
          name: 'staffof',
          type: 'Array&lt;any&gt;'
        },
        "customers": {
          name: 'customers',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "stripecustomerid": {
          name: 'stripecustomerid',
          type: 'string'
        },
        "devices": {
          name: 'devices',
          type: 'Array&lt;any&gt;'
        },
        "username": {
          name: 'username',
          type: 'string'
        },
        "email": {
          name: 'email',
          type: 'string'
        },
        "emailVerified": {
          name: 'emailVerified',
          type: 'boolean'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "password": {
          name: 'password',
          type: 'string'
        },
      },
      relations: {
        accessTokens: {
          name: 'accessTokens',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'userId'
        },
        apprealm: {
          name: 'apprealm',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'realm',
          keyTo: 'id'
        },
        adminofapps: {
          name: 'adminofapps',
          type: 'any[]',
          model: '',
          relationType: 'referencesMany',
                  keyFrom: 'adminof',
          keyTo: 'id'
        },
        createdappservices: {
          name: 'createdappservices',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'createdby'
        },
        createdappresources: {
          name: 'createdappresources',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'createdby'
        },
        createdappsrbookings: {
          name: 'createdappsrbookings',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'bookedby'
        },
        relatedcustomers: {
          name: 'relatedcustomers',
          type: 'any[]',
          model: '',
          relationType: 'referencesMany',
                  keyFrom: 'customers',
          keyTo: 'id'
        },
      }
    }
  }
}
