import { Injectable } from '@angular/core';

@Injectable()
export class ThemeColorConfig {

  constructor () {

  }

  //theme colors
  public themeColors = {

      primary: {
        base: '#387ef5',
        contrast: '#ffffff',
        dark: '#2c64c4',
        rgb: {r: '56', g: '126', b: '245'},
        defaultstatusbar: 'light',
        allowedasthemecolor: true
      },
      secondary: {
        base: '#32db64',
        contrast: '#ffffff',
        dark: '#28af50',
        rgb: {r: '50', g: '219', b: '100'},
        defaultstatusbar: 'light',
        allowedasthemecolor: true
      },
      danger: {
        base: '#f53d3d',
        contrast: '#ffffff',
        dark: '#c43030',
        rgb: {r: '245', g: '61', b: '61'},
        defaultstatusbar: 'light',
        allowedasthemecolor: true
      },
      light: {
        base: '#f4f4f4',
        contrast: '#222',
        dark: '#c3c3c3',
        rgb: {r: '244', g: '244', b: '244'},
        defaultstatusbar: 'dark',
        allowedasthemecolor: false
      },
      dark: {
        base: '#222',
        contrast: '#ffffff',
        dark: '#1b1b1b',
        rgb: {r: '34', g: '34', b: '34'},
        defaultstatusbar: 'light',
        allowedasthemecolor: true
      },
      assertive: {
        base: '#F44336',
        contrast: '#ffffff',
        dark: '#B71C1C',
        rgb: {r: '244', g: '67', b: '44'},
        defaultstatusbar: 'light',
        allowedasthemecolor: true
      },
      fresh: {
        base: '#4ABDAC',
        contrast: '#ffffff',
        dark: '#2c7167',
        rgb: {r: '74', g: '189', b: '172'},
        defaultstatusbar: 'light',
        allowedasthemecolor: true
      },
      dusty: {
        base: '#96858F',
        contrast: '#ffffff',
        dark: '#695d64',
        rgb: {r: '150', g: '133', b: '143'},
        defaultstatusbar: 'light',
        allowedasthemecolor: false
      },
      warm: {
        base: '#d7cec7',
        contrast: '#2b2927',
        dark: '#96908b',
        rgb: {r: '215', g: '206', b: '199'},
        defaultstatusbar: 'light',
        allowedasthemecolor: false
      },
      terracotta: {
        base: '#945D60',
        contrast: '#ffffff',
        dark: '#583739',
        rgb: {r: '148', g: '93', b: '96'},
        defaultstatusbar: 'light',
        allowedasthemecolor: true
      },
      black: {
        base: '#000000',
        contrast: '#ffffff',
        dark: '#000000',
        rgb: {r: '0', g: '0', b: '0'},
        defaultstatusbar: 'light',
        allowedasthemecolor: true
      },
      white: {
        base: '#ffffff',
        contrast: '#000000',
        dark: '#ffffff',
        rgb: {r: '255', g: '255', b: '255'},
        defaultstatusbar: 'dark',
        allowedasthemecolor: false
      }
    }

  //get rgba function - useful while setting background color css below
  public getrgba (theme: string, opacity: string): string {
    return ('rgba(' + this.themeColors[theme].rgb.r + ',' +
                      this.themeColors[theme].rgb.g + ',' +
                      this.themeColors[theme].rgb.b + ',' + opacity + ')');
  }

}
