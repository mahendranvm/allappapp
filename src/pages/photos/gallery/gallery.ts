import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { GalleryService, AppConfigInfo, ErrorToaster, SpinnerWrapper } from '../../../shared/services';
import { TranslateService } from '@ngx-translate/core';

import { SliderPage  } from './slider/slider'


@Component({
  selector: 'page-gallery',
  templateUrl: 'gallery.html'
})

export class GalleryPage {

  public pics: Array<any> = [];
  public picsDisp: Array<any> = [];
  public album: any;
  public section: any;
  private showEmpty: boolean = false;
  private emptyErrorString: string = "Empty";
  private loadFailErrorString: string = "Error";

  //pagination variables
  private pagination: any = {
    paginationLimit: 30,
    bookmarkIndex: 0
  }

  constructor(public navCtrl: NavController,
              public modalCtrl: ModalController,
              public params: NavParams,
              public galleryService: GalleryService,
              private errorToaster: ErrorToaster,
              public spinnerWrapper: SpinnerWrapper,
              public translateService: TranslateService,
              public appConfig: AppConfigInfo) {

    //get the empty and error string
    this.translateService.get(['EMPTY_ERROR','LOAD_FAIL_ERROR']).subscribe((value) => {
      this.emptyErrorString = value['EMPTY_ERROR'];
      this.loadFailErrorString = value['LOAD_FAIL_ERROR'];
    })
    //get the section and album
    this.section = this.params.get("section");
    this.album = this.params.get("album");
    //get Pic keys
    this.galleryService.getPicKeys (this.section.id, this.album.id).subscribe((resp)=>{
      if (resp && resp.success && resp.success.keys && resp.success.keys.length && resp.success.keys.length > 0){
        this.pics = resp.success.keys;
        this.pics.sort((a,b)=>{
          if (a.LastModified > b.LastModified) return -1
          else return 1
        })
        //first load, call doInfinite without infiniteScroll element
        this.doInfinite(null);
      } else {
        this.showEmpty = true;
        this.spinnerWrapper.hide();
      }
    }, (err)=>{
      this.spinnerWrapper.hide();
      this.errorToaster.toastError(err, this.loadFailErrorString)
    })

  }

  doInfinite(infiniteScroll) {

    //call loadMoreUrls only if bookmarkIndex is less than pics length
    if (this.pagination.bookmarkIndex < this.pics.length) {
      this.galleryService.loadMoreUrls (this.pagination, this.pics, this.picsDisp, this.section.id, this.album.id).subscribe((resp)=>{
        //stop infinite scroll or spinner
        if (infiniteScroll) infiniteScroll.complete();
        else this.spinnerWrapper.hide();
        //end infinite scroll if end reached
        if (this.pagination.bookmarkIndex >= this.pics.length && infiniteScroll) infiniteScroll.enable(false);
      }, (err)=>{
        //stop infinite scroll or spinner
        if (infiniteScroll) infiniteScroll.complete();
        else this.spinnerWrapper.hide();
        this.errorToaster.toastError(err, this.loadFailErrorString, this.navCtrl);
      });
    } else if (this.pagination.bookmarkIndex >= this.pics.length) {
      //if bookmark is greater than pics length, end infinite scroll
      if (infiniteScroll) {
        infiniteScroll.complete();
        infiniteScroll.enable(false);
      } else {
        this.spinnerWrapper.hide();
      }
    }

  }

  showSliderMode (currentIndex) {
    let modal = this.modalCtrl.create(SliderPage, {section: this.section, album: this.album, pics: this.pics, picsDisp: this.picsDisp, loadFailErrorString: this.loadFailErrorString, pagination: this.pagination, currentIndex: currentIndex});
    modal.present();
  }

}
