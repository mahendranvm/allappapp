import { Component, ViewChild } from '@angular/core';
import { ViewController, NavParams, ActionSheetController, Slides, Platform } from 'ionic-angular';

import { GalleryService, FileService, ErrorToaster, SpinnerWrapper, AppConfigInfo  } from '../../../../shared/services'
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'page-slider',
  templateUrl: 'slider.html'
})

export class SliderPage {

  @ViewChild(Slides) slides: Slides;

  public pics: Array<any> = [];
  public picsDisp: Array<any> = [];
  public album: any;
  public section: any;
  private loadFailErrorString: string = "Error";
  private pagination: any;
  private currentIndex: number = 0;
  private initialSlide: number = 0;
  private saveErrorString: string;
  private shareErrorString: string;

  private loadMoreThreshold: number = 0.75;

  constructor(public viewCtrl: ViewController,
              public params: NavParams,
              public actionSheetCtrl: ActionSheetController,
              public platform: Platform,
              public galleryService: GalleryService,
              public fileService: FileService,
              private errorToaster: ErrorToaster,
              public spinnerWrapper: SpinnerWrapper,
              public translateService: TranslateService,
              public appConfig: AppConfigInfo) {

    //get the empty and error string
    this.translateService.get(['SAVE_FAILED','SOCIAL_SHARE_FAILED']).subscribe((value) => {
      this.saveErrorString = value['SAVE_FAILED'];
      this.shareErrorString = value['SOCIAL_SHARE_FAILED'];
    })
    //get the section and album
    this.section = this.params.get("section");
    this.album = this.params.get("album");
    this.pics = this.params.get("pics");
    this.picsDisp = this.params.get("picsDisp");
    this.loadFailErrorString = this.params.get("loadFailErrorString");
    this.pagination = this.params.get("pagination");
    this.currentIndex = this.params.get("currentIndex");
    this.initialSlide = this.params.get("currentIndex");
  }

  ngAfterViewInit() {
    this.slides.initialSlide = this.initialSlide;
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  slideChanged() {
    this.currentIndex = this.slides.getActiveIndex();
    //load more if in bottom 25% and still unloaded URLs left
    if (this.currentIndex >= this.picsDisp.length*this.loadMoreThreshold &&
        this.pagination.bookmarkIndex < this.pics.length) {
          this.galleryService.loadMoreUrls (this.pagination, this.pics, this.picsDisp, this.section.id, this.album.id).subscribe((res)=>{
            //nothing needs to be done, all arrays are already updated
          }, (err)=>{
            //do nothing, if urls cannot be loaded, no error need to be displayed while in slider mode..
          });
    }
  }

  openMenu() {

     let actionSheet = this.actionSheetCtrl.create({
       title: 'Options',
       buttons: [
         {
           text: 'Save',
           handler: () => {this.save(this.picsDisp[this.currentIndex]);},
           icon: !this.platform.is('ios') ? 'download': null
         },
         {
           text: 'Share',
           handler: () => {this.share(this.picsDisp[this.currentIndex]);},
           icon: !this.platform.is('ios') ? 'share': null
         },
         {
           text: 'Cancel',
           role: 'cancel',
           icon: !this.platform.is('ios') ? 'close': null
         }
       ]
     });

     actionSheet.present();
  }

  save (pic) {
    this.spinnerWrapper.show();
    this.fileService.savePicToGallery(pic.src, pic.Key)
    .then (()=>{
      this.spinnerWrapper.hide()
    })
    .catch ((err)=>{
      this.spinnerWrapper.hide()
      this.errorToaster.toastError(err, this.saveErrorString);
    })
  }

  share (pic) {
    this.spinnerWrapper.show();
    this.fileService.downloadToCache (pic.src, pic.Key)
    .then ((fileEntry)=>{
      this.spinnerWrapper.hide();
      this.fileService.socialShareFromCache (fileEntry.toURL())
      .catch((err)=>{
          this.errorToaster.toastError(err, this.shareErrorString);
      })
    }, (err)=>{
      //using then reject handler instead of catch, as spinner should be hidden only if download fails
      //using catch, we will not be able to distinguish where error originated from
      this.spinnerWrapper.hide();
      this.errorToaster.toastError(err, this.shareErrorString);
    })
  }

}
