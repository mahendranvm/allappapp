import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { GalleryService, AppConfigInfo, ErrorToaster, SpinnerWrapper } from '../../../shared/services';
import { TranslateService } from '@ngx-translate/core';

import {  GalleryPage } from '../gallery/gallery'


@Component({
  selector: 'page-albums',
  templateUrl: 'albums.html'
})

export class AlbumsPage {

  public albums: Array<any> = [];
  public section: any;
  private showEmpty: boolean = false;
  private emptyErrorString: string = "Empty";
  private loadFailErrorString: string = "Error";

  constructor(public navCtrl: NavController,
              public params: NavParams,
              public galleryService: GalleryService,
              private errorToaster: ErrorToaster,
              private spinnerWrapper: SpinnerWrapper,
              public translateService: TranslateService,
              public appConfig: AppConfigInfo) {
    //get the empty and error string
    this.translateService.get(['EMPTY_ERROR','LOAD_FAIL_ERROR']).subscribe((value) => {
      this.emptyErrorString = value['EMPTY_ERROR'];
      this.loadFailErrorString = value['LOAD_FAIL_ERROR'];
    })
    //get the section and albums list
    this.section = this.params.get("section");
    this.albums = this.params.get("albums");
    if (!this.albums || !(this.albums.length>0)) this.showEmpty = true;
    this.spinnerWrapper.hide();
  }

  doRefresh(refresher) {
    this.galleryService.getAlbums (this.section.id).subscribe((resp)=>{
      this.albums = resp.success;
      if (!this.albums || !(this.albums.length>0)) this.showEmpty = true;
      refresher.complete();
    }, (err)=>{
      refresher.complete();
      this.errorToaster.toastError(err, this.loadFailErrorString, this.navCtrl);
    });
  }

  openAlbum(album) {
    this.spinnerWrapper.show(); //showing spinner here, to avoid flickering when deciding between gallery and album page in home page
    this.navCtrl.push(GalleryPage, {section: this.section, album: album});
  }

}
