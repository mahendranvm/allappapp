import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { StaticSectionService, AppConfigInfo, ErrorToaster, SpinnerWrapper } from '../../shared/services';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html'
})

export class FAQPage {

  public faqs: Array<any> = [];
  public filteredFAQs: Array<any> = [];
  public section: any;
  private showEmpty: boolean = false;
  private emptyErrorString: string = "Empty";
  private loadFailErrorString: string = "Error";

  constructor(public navCtrl: NavController,
              public params: NavParams,
              public staticSectionService: StaticSectionService,
              private errorToaster: ErrorToaster,
              private spinnerWrapper: SpinnerWrapper,
              public translateService: TranslateService,
              public appConfig: AppConfigInfo) {
    //get the empty and error string
    this.translateService.get(['EMPTY_ERROR','LOAD_FAIL_ERROR']).subscribe((value) => {
      this.emptyErrorString = value['EMPTY_ERROR'];
      this.loadFailErrorString = value['LOAD_FAIL_ERROR'];
    })

    //get the section and albums list
    this.section = this.params.get("section");
    this.faqs = this.params.get("faqs");
    this.filteredFAQs = this.faqs;
    if (!this.faqs || !(this.faqs.length>0)) this.showEmpty = true;
    this.spinnerWrapper.hide();

  }

  doRefresh(refresher) {
    this.staticSectionService.getStaticSectionData (this.section.id).subscribe((resp)=>{
      if (resp && resp.success && resp.success.length && resp.success[0].sectiondata) this.faqs = resp.success[0].sectiondata;
      if (!this.faqs || !(this.faqs.length>0)) this.showEmpty = true;
      refresher.complete();
    }, (err)=>{
      refresher.complete();
      this.errorToaster.toastError(err, this.loadFailErrorString, this.navCtrl);
    });
  }

  filterFAQs(ev: any) {

    // Reset items back to all of the items
    this.filteredFAQs = this.faqs;

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.filteredFAQs = this.filteredFAQs.filter((faq) => {
        if (faq.question.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
            faq.answer.toLowerCase().indexOf(val.toLowerCase()) > -1) return true
        else return false;
      })
    }

  }


}
