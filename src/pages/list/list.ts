import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { InfoPage } from '../info/info'
import { AppConfigInfo, SpinnerWrapper, StaticSectionService, ErrorToaster, AppNavService } from '../../shared/services';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})

export class ListPage {

  public listdata: any = {};
  public section: any;
  private showEmpty: boolean = false;
  private emptyErrorString: string = "Empty";
  private loadFailErrorString: string = "Error";

  constructor(public navCtrl: NavController,
              public params: NavParams,
              private spinnerWrapper: SpinnerWrapper,
              public staticSectionService: StaticSectionService,
              private errorToaster: ErrorToaster,
              public translateService: TranslateService,
              public appNavService: AppNavService,
              public appConfig: AppConfigInfo) {
    //get the empty and error string
    this.translateService.get(['EMPTY_ERROR','LOAD_FAIL_ERROR']).subscribe((value) => {
      this.emptyErrorString = value['EMPTY_ERROR'];
      this.loadFailErrorString = value['LOAD_FAIL_ERROR'];
    })

    //get the section and albums list
    this.section = this.params.get("section");
    this.listdata = this.params.get("listdata");
    if (!this.listdata || !this.listdata.list || !this.listdata.list.length || this.listdata.list.length<=0) this.showEmpty = true;
    else {
      //loop through to find if there are images for which imageurl should be retrieved
      for (let i=0; i<this.listdata.list.length; i++) {
        if (this.listdata.list[i].img) {
          this.staticSectionService.getImageUrl (this.listdata.list[i].img).subscribe((url)=>{
            this.listdata.list[i].url = url;
          }, (err)=>{
            this.errorToaster.toastError(err, this.loadFailErrorString, this.navCtrl);
          });
        }
      }
    }
    this.spinnerWrapper.hide();

  }

  openItem (sectionid) {
    let targetSection = null;
    this.appConfig.appAllSections.forEach((appsection)=>{
      if (appsection.id == sectionid && appsection.module=='Info') targetSection = appsection;
    });
    if (targetSection) {
      this.spinnerWrapper.show();
      this.appNavService.openSection ({InfoPage: InfoPage}, targetSection, this.navCtrl, null, false, null);
    }
  }


}
