import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { AppConfigInfo, SpinnerWrapper, StaticSectionService, ErrorToaster } from '../../shared/services';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-info',
  templateUrl: 'info.html'
})

export class InfoPage {

  public infosectiondata: Array<any> = [];
  public section: any;
  private showEmpty: boolean = false;
  private emptyErrorString: string = "Empty";
  private loadFailErrorString: string = "Error";

  constructor(public navCtrl: NavController,
              public params: NavParams,
              private spinnerWrapper: SpinnerWrapper,
              public staticSectionService: StaticSectionService,
              private errorToaster: ErrorToaster,
              public translateService: TranslateService,
              public appConfig: AppConfigInfo) {
    //get the empty and error string
    this.translateService.get(['EMPTY_ERROR','LOAD_FAIL_ERROR']).subscribe((value) => {
      this.emptyErrorString = value['EMPTY_ERROR'];
      this.loadFailErrorString = value['LOAD_FAIL_ERROR'];
    })

    //get the section and albums list
    this.section = this.params.get("section");
    this.infosectiondata = this.params.get("infosectiondata");
    if (!this.infosectiondata || !(this.infosectiondata.length>0)) this.showEmpty = true;
    else {
      //loop through to find if there are images for which imageurl should be retrieved
      for (let i=0; i<this.infosectiondata.length; i++) {
        if (this.infosectiondata[i].type=="One Image") {
          this.staticSectionService.getImageUrl (this.infosectiondata[i].infodata).subscribe((url)=>{
            this.infosectiondata[i].url = url;
          }, (err)=>{
            this.errorToaster.toastError(err, this.loadFailErrorString, this.navCtrl);
          });
        }
      }
    }
    this.spinnerWrapper.hide();

  }

}
