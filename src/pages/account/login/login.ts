import { Component } from '@angular/core';
import { NavController, ToastController, NavParams } from 'ionic-angular';

import { TranslateService } from '@ngx-translate/core';

import { HomePage } from '../../home/home';
import { ForgotPasswordPage } from '../forgot-password/forgot-password';

import { AppuserApi } from '../../../shared/lbsdk/services';
import { AppConfigInfo, DeviceInfoCollector, ErrorToaster, SpinnerWrapper, PushHandler, StatusBarSettings } from '../../../shared/services';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  // The account fields for the login form.
  account: {realm: string, email: string, password: string, ttl: number} = {
    realm: this.appConfig.realm,
    email: '',
    password: '',
    ttl: this.appConfig.tokenTTL
  };

  private popBack: boolean = false;

  // Our translated text strings
  private loginErrorString: string;

  constructor(public spinnerWrapper: SpinnerWrapper,
              public navCtrl: NavController,
              public params: NavParams,
              public appuserApi: AppuserApi,
              public errorToaster: ErrorToaster,
              public deviceInfoCollector: DeviceInfoCollector,
              public toastCtrl: ToastController,
              public translateService: TranslateService,
              public pushHandler: PushHandler,
              public statusBarSettings: StatusBarSettings,
              public appConfig: AppConfigInfo) {
    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    })
    if (this.params.get("popBack")) this.popBack = true;
  }

  // Attempt to login in through our User service
  doLogin() {
    this.spinnerWrapper.show();
    //ensure email is in lower case
    this.account.email = this.account.email.toLowerCase();
    this.appuserApi.login(this.account).subscribe((resp) => {
      //after every login refresh app sections, status bar and push init
      this.appConfig.loadAppSettings().subscribe ((resp)=>{
        //Set StatusBar color
        this.statusBarSettings.setStatusBarColor();
        //Initialize push
        this.pushHandler.initialize();
        this.spinnerWrapper.hide();
        //popBack or go to HomePage
        if (this.popBack) this.navCtrl.pop();
        else this.navCtrl.setRoot(HomePage);
      }, (err)=> {
        this.spinnerWrapper.hide();
        this.errorToaster.toastError(err, this.loginErrorString);
      });
    }, (err) => {
      this.spinnerWrapper.hide();
      this.errorToaster.toastError(err, this.loginErrorString);
    });
  }

  //navigate to Forgot Password Page
  forgotPassword() {
    this.navCtrl.push(ForgotPasswordPage);
  }
}
