import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { LoginPage } from '../login/login';
import { SignupPage } from '../signup/signup';

import { AppConfigInfo } from '../../../shared/services';


/**
 * The Welcome Page is a splash page that quickly describes the app,
 * and then directs the user to create an account or log in.
 * If you'd like to immediately put the user onto a login/signup page,
 * we recommend not using the Welcome page.
*/
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html'
})
export class WelcomePage {

  private greetingsList: string[] = ["Hello!", "Hola!", "Welcome!", "Howdy!",
  "Hi!", "G'day!", "Hallo!", "OLÀ", "Aloha"];

  private greeting: string;

  constructor(public navCtrl: NavController,
    public appConfig: AppConfigInfo) {
    //select a random greeting
    this.greeting = this.greetingsList[Math.floor(Math.random()*this.greetingsList.length)];
  }

  login() {
    this.navCtrl.push(LoginPage);
  }

  signup() {
    this.navCtrl.push(SignupPage);
  }
}
