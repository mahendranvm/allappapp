import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';

import { TranslateService } from '@ngx-translate/core';

import { AccountVerificationPage } from '../account-verification/account-verification';
import { AppuserApi } from '../../../shared/lbsdk/services';
import { AppConfigInfo, ErrorToaster, SpinnerWrapper } from '../../../shared/services';


@Component({
  selector: 'page-new-verification-token',
  templateUrl: 'new-verification-token.html'
})
export class NewVerificationTokenPage {
  // The fields for password reset request.
  account: {realm: string, email: string} = {
    realm: this.appConfig.realm,
    email: ''
  };

  // Our translated text strings
  private newVerificationTokenErrorString: string;

  constructor(public spinnerWrapper: SpinnerWrapper,
              public navCtrl: NavController,
              public appuserApi: AppuserApi,
              public errorToaster: ErrorToaster,
              public toastCtrl: ToastController,
              public translateService: TranslateService,
              public appConfig: AppConfigInfo) {

    this.translateService.get('NEW_ACCOUNT_VERIFICATION_TOKEN_ERROR').subscribe((value) => {
      this.newVerificationTokenErrorString = value;
    })
  }

  // Attempt to login in through our User service
  doNewVerificationToken() {
    this.spinnerWrapper.show();
    this.appuserApi.resendVerify(this.account.realm, this.account.email).subscribe((resp) => {
      this.spinnerWrapper.hide();
      this.navCtrl.push(AccountVerificationPage);
    }, (err) => {
      this.spinnerWrapper.hide();
      this.errorToaster.toastError(err, this.newVerificationTokenErrorString);
    });
  }

}
