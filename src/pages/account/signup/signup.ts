import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';

import { TranslateService } from '@ngx-translate/core';

import { AccountVerificationPage } from '../account-verification/account-verification';
import { NewVerificationTokenPage } from '../new-verification-token/new-verification-token';
import { AppuserApi } from '../../../shared/lbsdk/services';
import { AppConfigInfo, ErrorToaster, SpinnerWrapper } from '../../../shared/services';

/*
  Generated class for the Signup page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: {realm: string, firstname: string, lastname: string, email: string, password: string} = {
    realm: this.appConfig.realm,
    firstname: '',
    lastname: '',
    email: '',
    password: ''
  };
  //private confirmPassword: {password: ''} ;

  // Our translated text strings
  private signupErrorString: string;
  private pwdMatchErrorString: string;

  constructor(public spinnerWrapper: SpinnerWrapper,
              public navCtrl: NavController,
              public appuserApi: AppuserApi,
              public errorToaster: ErrorToaster,
              public toastCtrl: ToastController,
              public translateService: TranslateService,
              public appConfig: AppConfigInfo) {

    this.translateService.get(['SIGNUP_ERROR', 'PASSWORD_MATCH_ERROR']).subscribe((value) => {
      this.signupErrorString = value['SIGNUP_ERROR'];
      this.pwdMatchErrorString = value ['PASSWORD_MATCH_ERROR'];
    })
  }

  doSignup() {
    this.spinnerWrapper.show();
    // Attempt to login in through our User service
    this.appuserApi.createUser(this.account).subscribe((resp) => {
      this.spinnerWrapper.hide();
      this.navCtrl.push(AccountVerificationPage);
    }, (err) => {
      this.spinnerWrapper.hide();
      this.errorToaster.toastError(err, this.signupErrorString);
    });
  }

  //navigate to Account Verification Page
  verifyAccount() {
    this.navCtrl.push(AccountVerificationPage);
  }

  //navigate to Account Verification Page
  newVerificationCode() {
    this.navCtrl.push(NewVerificationTokenPage);
  }
}
