import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';

import { TranslateService } from '@ngx-translate/core';

import { LoginPage } from '../login/login';
import { AppuserApi } from '../../../shared/lbsdk/services';
import { AppConfigInfo, ErrorToaster, SpinnerWrapper } from '../../../shared/services';


@Component({
  selector: 'page-account-verification',
  templateUrl: 'account-verification.html'
})
export class AccountVerificationPage {
  // The fields for password reset.
  account: {realm: string, email: string, token: string} = {
    realm: this.appConfig.realm,
    email: '',
    token: ''
  };

  // Our translated text strings
  private verifyAccountErrorString: string;

  constructor(public spinnerWrapper: SpinnerWrapper,
              public navCtrl: NavController,
              public appuserApi: AppuserApi,
              public errorToaster: ErrorToaster,
              public toastCtrl: ToastController,
              public translateService: TranslateService,
              public appConfig: AppConfigInfo) {

    this.translateService.get('VERIFY_ACCOUNT_ERROR').subscribe((value) => {
      this.verifyAccountErrorString = value;
    })
  }

  // Attempt to login in through our User service
  doVerifyAccount() {
    this.spinnerWrapper.show();
    this.appuserApi.confirmToken(this.account.realm, this.account.email, this.account.token).subscribe((resp) => {
      this.spinnerWrapper.hide();
      this.navCtrl.push(LoginPage);
    }, (err) => {
      this.spinnerWrapper.hide();
      this.errorToaster.toastError(err, this.verifyAccountErrorString);
    });
  }
}
