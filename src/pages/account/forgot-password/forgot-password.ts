import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';

import { TranslateService } from '@ngx-translate/core';

import { ResetPasswordPage } from '../reset-password/reset-password';
import { AppuserApi } from '../../../shared/lbsdk/services';
import { AppConfigInfo, ErrorToaster, SpinnerWrapper } from '../../../shared/services';


@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html'
})
export class ForgotPasswordPage {
  // The fields for password reset request.
  account: {realm: string, email: string} = {
    realm: this.appConfig.realm,
    email: ''
  };

  // Our translated text strings
  private forgotPasswordErrorString: string;

  constructor(public spinnerWrapper: SpinnerWrapper,
              public navCtrl: NavController,
              public appuserApi: AppuserApi,
              public errorToaster: ErrorToaster,
              public toastCtrl: ToastController,
              public translateService: TranslateService,
              public appConfig: AppConfigInfo) {

    this.translateService.get('FORGOT_PASSWORD_ERROR').subscribe((value) => {
      this.forgotPasswordErrorString = value;
    })
  }

  // Attempt to login in through our User service
  doForgotPassword() {
    this.spinnerWrapper.show();
    this.appuserApi.pwdResetreq(this.account.realm, this.account.email).subscribe((resp) => {
      this.spinnerWrapper.hide();
      this.navCtrl.push(ResetPasswordPage);
    }, (err) => {
      this.spinnerWrapper.hide();
      this.errorToaster.toastError(err, this.forgotPasswordErrorString);
    });
  }

  //navigate to Reset Password Page
  resetPassword() {
    this.navCtrl.push(ResetPasswordPage);
  }
}
