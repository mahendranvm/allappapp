import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { BookingService, AppConfigInfo, ErrorToaster, SpinnerWrapper } from '../../../../shared/services';
import { TranslateService } from '@ngx-translate/core';

import { ViewBookingDetailsPage  } from '../view-booking-details/view-booking-details'



@Component({
  selector: 'page-list-booking',
  templateUrl: 'list-booking.html'
})

export class ListBookingPage {

  public bookings: Array<any> = [];
  public section: any;
  public history: boolean = false;
  public bookinghistoryind: string = "Current";
  private showEmpty: boolean = false;
  private loadFailErrorString: string = "Error";

  constructor(public navCtrl: NavController,
              public params: NavParams,
              public bookingService: BookingService,
              private errorToaster: ErrorToaster,
              private spinnerWrapper: SpinnerWrapper,
              public translateService: TranslateService,
              public appConfig: AppConfigInfo) {
    //get the empty and error string
    this.translateService.get(['LOAD_FAIL_ERROR']).subscribe((value) => {
      this.loadFailErrorString = value['LOAD_FAIL_ERROR'];
    })
    //get the section and albums list
    this.section = this.params.get("section");
    //get bookings
    this.bookings = this.params.get("bookings");
    if (!this.bookings || !this.bookings.length || this.bookings.length<=0) this.showEmpty = true;
    else this.showEmpty = false;
  }

  doRefresh(refresher) {
    this.bookingService.viewMyBookings (this.history).subscribe((resp)=>{
      if (resp && resp.success) this.bookings = resp.success;
      if (!this.bookings || !this.bookings.length || this.bookings.length<=0) this.showEmpty = true
      else this.showEmpty = false;
      refresher.complete();
    }, (err)=>{
      refresher.complete();
      this.errorToaster.toastError(err, this.loadFailErrorString);
    });
  }

  showHistory () {
    this.history = true;
    this.spinnerWrapper.show();
    this.bookingService.viewMyBookings (this.history).subscribe((resp)=>{
      if (resp && resp.success) this.bookings = resp.success;
      if (!this.bookings || !this.bookings.length || this.bookings.length<=0) this.showEmpty = true
      else this.showEmpty = false;
      this.spinnerWrapper.hide();
    }, (err)=>{
      this.spinnerWrapper.hide();
      this.errorToaster.toastError(err, this.loadFailErrorString);
    });
  }

  showCurrent () {
    this.history = false;
    this.spinnerWrapper.show();
    this.bookingService.viewMyBookings (this.history).subscribe((resp)=>{
      if (resp && resp.success) this.bookings = resp.success;
      if (!this.bookings || !this.bookings.length || this.bookings.length<=0) this.showEmpty = true
      else this.showEmpty = false;
      this.spinnerWrapper.hide();
    }, (err)=>{
      this.spinnerWrapper.hide();
      this.errorToaster.toastError(err, this.loadFailErrorString);
    });
  }

  openBooking (booking) {
    this.navCtrl.push(ViewBookingDetailsPage, {section: this.section, booking: booking, bookings: this.bookings, history: this.history});
  }

}
