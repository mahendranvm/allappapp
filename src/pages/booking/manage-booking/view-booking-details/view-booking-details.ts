import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, Platform } from 'ionic-angular';

import { BookingService, AppConfigInfo, ErrorToaster, SpinnerWrapper } from '../../../../shared/services';
import { TranslateService } from '@ngx-translate/core';



@Component({
  selector: 'page-view-booking-details',
  templateUrl: 'view-booking-details.html'
})

export class ViewBookingDetailsPage {

  public booking: any;
  public bookings: Array<any> = [];
  public section: any;
  public history: boolean = false;
  public bookingSection: any;
  public loadFailErrorString: string = "Error";
  private cancelErrorString: string = "Error";
  private cancelString: string = "Cancel";
  private cancelPromptString: string = "Are you sure you want to cancel?";
  private cancelReasonPlaceHolder: string = "Reason for Cancellation";
  private cancelPromptYesString: string = "Yes";
  private cancelPromptNoString: string = "No";

  public successMessage: string = null;

  constructor(public navCtrl: NavController,
              public params: NavParams,
              public alertCtrl: AlertController,
              public platform: Platform,
              public bookingService: BookingService,
              private errorToaster: ErrorToaster,
              private spinnerWrapper: SpinnerWrapper,
              public translateService: TranslateService,
              public appConfig: AppConfigInfo) {
    //get the empty and error string
    this.translateService.get(['LOAD_FAIL_ERROR', 'BOOKING_CANCEL_ERROR','BOOKING_CANCEL_LABEL', 'BOOKING_CANCEL_PROMPT',
    'BOOKING_CANCEL_REASON_PLACEHOLDER', 'BOOKING_CANCEL_PROMPT_YES_LABEL', 'BOOKING_CANCEL_PROMPT_NO_LABEL']).subscribe((value) => {
      this.loadFailErrorString = value ['LOAD_FAIL_ERROR'];
      this.cancelErrorString = value['BOOKING_CANCEL_ERROR'];
      this.cancelString = value['BOOKING_CANCEL_LABEL'];
      this.cancelPromptString = value['BOOKING_CANCEL_PROMPT'];
      this.cancelReasonPlaceHolder = value['BOOKING_CANCEL_REASON_PLACEHOLDER'];
      this.cancelPromptYesString = value['BOOKING_CANCEL_PROMPT_YES_LABEL'];
      this.cancelPromptNoString = value['BOOKING_CANCEL_PROMPT_NO_LABEL'];
    })
    //get the section and albums list
    this.section = this.params.get("section");
    this.booking = this.params.get("booking");
    this.history = this.params.get("history");
    this.bookings = this.params.get("bookings");
    //get success message to be displayed if any
    this.successMessage = this.params.get("successMessage");
  }

  confirmCancel () {
    let prompt = this.alertCtrl.create({
       title: this.cancelString,
       message: this.cancelPromptString,
       inputs: [
         {
           name: 'reason',
           placeholder: this.cancelReasonPlaceHolder
         },
       ],
       buttons: [
         {
           text: this.cancelPromptNoString,
           handler: data => {},
           role: 'cancel'
         },
         {
           text: this.cancelPromptYesString,
           handler: data => { this.cancelBooking(data.reason); }
         }
       ]
     });
     prompt.present();
  }

  cancelBooking (reason) {
    this.spinnerWrapper.show();
    this.bookingService.cancelBooking(this.booking.id, reason).subscribe((res)=>{
      //refresh bookings and pop back
      this.bookingService.viewMyBookings (this.history).subscribe((resp)=>{
        if (resp && resp.success) {
          //empty bookings array and reload on success
          this.bookings.length = 0;
          Array.prototype.push.apply(this.bookings, resp.success);
        }
        this.spinnerWrapper.hide();
        this.navCtrl.pop();
      }, (err)=>{
        this.spinnerWrapper.hide();
        this.errorToaster.toastError(err, this.loadFailErrorString);
      });
    }, (err)=>{
      this.spinnerWrapper.hide();
      this.errorToaster.toastError(err, this.cancelErrorString)
    })
  }

}
