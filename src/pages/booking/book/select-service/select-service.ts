import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { AppConfigInfo, SpinnerWrapper } from '../../../../shared/services';
import { TranslateService } from '@ngx-translate/core';

import { SearchAvailabilityPage  } from '../search-availability/search-availability'


@Component({
  selector: 'page-select-service',
  templateUrl: 'select-service.html'
})

export class SelectServicePage {

  public services: Array<any> = [];
  public section: any;
  private showEmpty: boolean = false;
  private emptyErrorString: string = "Empty";
  private loadFailErrorString: string = "Error";

  constructor(public navCtrl: NavController,
              public params: NavParams,
              private spinnerWrapper: SpinnerWrapper,
              public translateService: TranslateService,
              public appConfig: AppConfigInfo) {
    //get the empty and error string
    this.translateService.get(['EMPTY_ERROR','LOAD_FAIL_ERROR']).subscribe((value) => {
      this.emptyErrorString = value['EMPTY_ERROR'];
      this.loadFailErrorString = value['LOAD_FAIL_ERROR'];
    })
    //get the section and albums list
    this.section = this.params.get("section");
    this.services = this.section.activeservicedetails;
    if (!this.services || !(this.services.length>0)) this.showEmpty = true;
    this.spinnerWrapper.hide();
  }

  search (service) {
    this.spinnerWrapper.show();
    this.navCtrl.push(SearchAvailabilityPage, {section: this.section, service: service, availPgTitle: service.name});
  }

}
