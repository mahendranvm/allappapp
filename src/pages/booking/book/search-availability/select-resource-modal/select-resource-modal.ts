import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { AppConfigInfo } from '../../../../../shared/services';
import { TranslateService } from '@ngx-translate/core';




@Component({
  selector: 'page-select-resource-modal',
  templateUrl: 'select-resource-modal.html'
})

export class SelectResourceModalPage {

  private selectFromRs: Array<any> = [];
  private pgTitle: string;
  private selR: any;
  private anyR: any;

  constructor(public viewCtrl: ViewController,
              public params: NavParams,
              public appConfig: AppConfigInfo,
              public translateService: TranslateService) {

    //get the empty and error string
    this.translateService.get(['BOOKING_RESOURCE_LABEL']).subscribe((value) => {
      this.pgTitle = value['BOOKING_RESOURCE_LABEL'];
    })

    this.selectFromRs = this.params.get('selectFromRs');
    if (this.params.get('pgTitle')) this.pgTitle = this.params.get('pgTitle');
    this.selR = this.params.get('selR');
    this.anyR = this.params.get('anyR');
  }

  dismiss() {
    this.viewCtrl.dismiss({changed: false});
  }

  resourceChange(newR) {
    if (newR.rid == this.anyR.rid){
      this.selR.name = this.anyR.name
      this.selR.rid = this.anyR.rid
    }
    else {
      this.selectFromRs.forEach((srcR)=>{
        if (newR.rid == srcR.rid){
          this.selR.name = srcR.name;
          this.selR.rid = srcR.rid;
        }
      })
    }
    this.viewCtrl.dismiss({changed: true});
  }

}
