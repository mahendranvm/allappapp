import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { AppConfigInfo } from '../../../../../shared/services';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-select-quantity-modal',
  templateUrl: 'select-quantity-modal.html'
})

export class SelectQuantityModalPage {

  private pgTitle: string;
  private quantityRange: Array<number>;

  constructor(public viewCtrl: ViewController,
              public params: NavParams,
              public appConfig: AppConfigInfo,
              public translateService: TranslateService) {

    //get the empty and error string
    this.translateService.get(['BOOKING_QUANTITY_LABEL']).subscribe((value) => {
      this.pgTitle = value['BOOKING_QUANTITY_LABEL'];
    });

    this.quantityRange = this.params.get('quantityRange');
  }

  dismiss() {
    this.viewCtrl.dismiss({quantity: null});
  }

  select(i) {
    this.viewCtrl.dismiss({quantity: this.quantityRange[i]});
  }

}
