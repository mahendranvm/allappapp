import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, Platform } from 'ionic-angular';

import { AppConfigInfo, ErrorToaster, SpinnerWrapper, BookingService } from '../../../../shared/services';
import { TranslateService } from '@ngx-translate/core';

import { ConfirmBookingPage  } from '../confirm-booking/confirm-booking'
import { SelectResourceModalPage } from './select-resource-modal/select-resource-modal'
import { SelectQuantityModalPage } from './select-quantity-modal/select-quantity-modal'

//moment.js
import * as moment from 'moment-timezone';


@Component({
  selector: 'page-search-availability',
  templateUrl: 'search-availability.html'
})

export class SearchAvailabilityPage {

  public service: any;
  public section: any;
  public availPgTitle: string = "";

  private anyR: any;
  private userSelectedRs: Array<any> = []; //Array containing the IDs of user selected resources. will be used only if resource selection is enabled.
  private quantity: number = 1; //The number of service units being booked. default to 1.
  private quantityRange: Array<number> = [1]; //Array containing integers until max units to be booked.
  private queryDate: any = {
    date: new Date((new Date()).setHours(0,0,0,0)), //default search query date to today
    dp: new Date((new Date()).setHours(0,0,0,0)).toISOString() //ISO String equivalent, used in ionic date picker
  }
  private minQueryDate: any = {
    date: new Date((new Date()).setHours(0,0,0,0)), //default min search query date to today
    dp: new Date((new Date()).setHours(0,0,0,0)).toISOString() //ISO String equivalent, used in ionic date picker
  }
  private maxQueryDate: any = {
    date: new Date(2099,11,31,0,0,0,0), //default max search query date to future infinite
    dp: new Date((new Date()).setHours(0,0,0,0)).toISOString() //ISO String equivalent, used in ionic date picker
  }
  private validSRsAvailability: Array<any> = []; //Array contining availability objects of all valid SRs
  private selectFromRs: Array<any> = []; //Array containing the Rs associated with the selected service.
  private timeslots: Array<any> = []; //Array containing the available timeslots

  private slotsAvailable: boolean = false;
  private loadFailErrorString: string = "Error";
  private anyOtherString: string = "Any Other";

  constructor(public navCtrl: NavController,
              public params: NavParams,
              public modalCtrl: ModalController,
              private errorToaster: ErrorToaster,
              private spinnerWrapper: SpinnerWrapper,
              public translateService: TranslateService,
              public bookingService: BookingService,
              public platform: Platform,
              public appConfig: AppConfigInfo) {
    //get the empty and error string
    this.translateService.get(['LOAD_FAIL_ERROR','BOOKING_ANY_OTHER']).subscribe((value) => {
      this.loadFailErrorString = value['LOAD_FAIL_ERROR'];
      this.anyOtherString = value['BOOKING_ANY_OTHER'];
    })
    //get the section and albums list
    this.section = this.params.get("section");
    this.service = this.params.get("service");
    this.availPgTitle = this.params.get("availPgTitle");
    if (!this.availPgTitle) this.availPgTitle = this.service.name;

    //get default values into user selected Rs
    this.anyR = this.bookingService.getAnyR();
    if (this.service.srperbooking > 1) this.bookingService.alignUserSelectedRsToQuantity (this.userSelectedRs, this.service.srperbooking);
    else this.bookingService.alignUserSelectedRsToQuantity (this.userSelectedRs, this.quantity);

    //fill quantityRange if multiple units can be booked
    if (this.service.multiunitbooking) {
      for (let i=2; i<=this.service.maxunitsperbooking; i++) this.quantityRange.push(i);
    }

    //set default querydate to today (take now's date in apptimezone and form date in current timezone and store it in query date)
    this.minQueryDate.date = new Date (moment.tz((new Date()), this.section.apptimezone).year(), moment.tz((new Date()), this.section.apptimezone).month(), moment.tz((new Date()), this.section.apptimezone).date(), 0, 0, 0, 0);
    this.minQueryDate.dp = this.minQueryDate.date.toISOString();
    this.queryDate.date = new Date (this.minQueryDate.date);
    this.queryDate.dp = this.queryDate.date.toISOString();

    //set the max query date based on advance booking period
    if (this.service.advancebookingperiod) {
      this.maxQueryDate.date = new Date(this.minQueryDate.date);
      this.maxQueryDate.date.setDate(this.maxQueryDate.date.getDate()+this.service.advancebookingperiod);
      this.maxQueryDate.dp = this.maxQueryDate.date.toISOString()
    }

    //now get the search results for queryDate
    this.bookingService.searchForDate (this.queryDate.date, this.service, this.validSRsAvailability, this.selectFromRs, this.userSelectedRs, this.timeslots).subscribe ((resp)=>{
      this.slotsAvailable = this.bookingService.noAvailabilityCheck (this.timeslots);
      this.spinnerWrapper.hide();
    }, (err)=>{
      this.spinnerWrapper.hide();
      this.errorToaster.toastError(err, this.loadFailErrorString);
    });

  }

  dateChange() {
    this.spinnerWrapper.show();
    let oldDate = this.queryDate.date;
    this.queryDate.date = new Date (this.queryDate.dp);
    //now get the search results for queryDate
    this.bookingService.searchForDate (this.queryDate.date, this.service, this.validSRsAvailability, this.selectFromRs, this.userSelectedRs, this.timeslots).subscribe ((resp)=>{
      this.slotsAvailable = this.bookingService.noAvailabilityCheck (this.timeslots);
      this.spinnerWrapper.hide();
    }, (err)=>{
      this.queryDate.date = new Date (oldDate);
      this.queryDate.dp = this.queryDate.date.toISOString();
      this.spinnerWrapper.hide();
      this.errorToaster.toastError(err, this.loadFailErrorString);
    });
  }

  openQuantitySelect () {
    //open modal
    let modal = this.modalCtrl.create(SelectQuantityModalPage, {quantityRange: this.quantityRange});
    modal.onDidDismiss (data => {
      if (data && data.quantity) {
        this.quantity = data.quantity;
        this.quantityChange();
      }
    })
    modal.present();
  }

  quantityChange() {
    if (this.service.srperbooking == 1) {
      this.bookingService.alignUserSelectedRsToQuantity (this.userSelectedRs, this.quantity);
      let userSelectedRsIDOnly = [];
      this.userSelectedRs.forEach((selection)=>{
        userSelectedRsIDOnly.push(selection.rid);
      })
      this.bookingService.refreshTimeslotAvailability (this.service, userSelectedRsIDOnly, this.timeslots);
      this.slotsAvailable = this.bookingService.noAvailabilityCheck (this.timeslots);
    } else {
      this.quantity = 1;
    }
  }

  openResourceSelect (selR, index) {
    let pgTitle: string;
    //create page title for modal window
    if (this.service.resourcelabel){
      pgTitle = this.service.resourcelabel;
      if (this.userSelectedRs.length > 1) pgTitle = pgTitle + ' ' + index;
    }
    //open modal
    let modal = this.modalCtrl.create(SelectResourceModalPage, {selectFromRs: this.selectFromRs, pgTitle: pgTitle, selR: selR, anyR: this.anyR});
    modal.onDidDismiss (data => {
      if (data && data.changed) {
        let userSelectedRsIDOnly = [];
        this.userSelectedRs.forEach((selection)=>{
          userSelectedRsIDOnly.push(selection.rid);
        });
        this.bookingService.refreshTimeslotAvailability (this.service, userSelectedRsIDOnly, this.timeslots);
        this.bookingService.refreshSelectFromRs (this.selectFromRs, this.service, this.validSRsAvailability, userSelectedRsIDOnly);
        this.slotsAvailable = this.bookingService.noAvailabilityCheck (this.timeslots);
      }
    })
    modal.present();
  }

  selectForBooking (timeslot) {
    let serviceBy: string = '';
    if (this.userSelectedRs.length == 1) {serviceBy=this.userSelectedRs[0].name;}
    else {
      var anycount = 0;
      for (var i=0; i<this.userSelectedRs.length; i++) {
        if (this.userSelectedRs[i].name == this.anyR.name) {
          anycount++;
        } else {
          if (serviceBy == '') {serviceBy=this.userSelectedRs[i].name}
          else {serviceBy = serviceBy + ", " + this.userSelectedRs[i].name}
        }
      }
      if (anycount == this.userSelectedRs.length) {serviceBy = this.anyR.name;}
      else if (anycount > 0) {serviceBy = serviceBy + ", " + this.anyOtherString}
    }

    let userSelectedRsIDOnly = [];
    this.userSelectedRs.forEach((selection)=>{
      userSelectedRsIDOnly.push(selection.rid);
    })

    //go to confirm page
    this.navCtrl.push(ConfirmBookingPage, {section: this.section, timeslot: timeslot, userSelectedRsIDOnly: userSelectedRsIDOnly,
       service: this.service, validSRsAvailability:this.validSRsAvailability, queryDate:this.queryDate.date,
       quantity: this.quantity, serviceBy: serviceBy})
  }

}
