import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';

import { BookingService, AppConfigInfo, ErrorToaster, SpinnerWrapper, AppNavService } from '../../../../shared/services';
import { TranslateService } from '@ngx-translate/core';

import { HomePage } from '../../../home/home'
import { ListBookingPage } from '../../manage-booking/list-booking/list-booking';
import { ViewBookingDetailsPage } from '../../manage-booking/view-booking-details/view-booking-details';



@Component({
  selector: 'page-confirm-booking',
  templateUrl: 'confirm-booking.html'
})

export class ConfirmBookingPage {

  private bookingErrorString: string = "Error";

  public service: any;
  public section: any;
  private userSelectedRsIDOnly: Array<string> = [];
  private quantity: number;
  private queryDate: Date;
  private validSRsAvailability: Array<any> = [];
  private timeslot: any;
  private serviceBy: string;
  private loadFailErrorString: string = "Error";

  private bookingSuccessMessage: string = null;
  private bookingRequestSuccessMessage: string = null;


  constructor(public navCtrl: NavController,
              public params: NavParams,
              public platform: Platform,
              public bookingService: BookingService,
              public appNavService: AppNavService,
              private errorToaster: ErrorToaster,
              private spinnerWrapper: SpinnerWrapper,
              public translateService: TranslateService,
              public appConfig: AppConfigInfo) {
    //get the empty and error string
    this.translateService.get(['BOOKING_ERROR', 'LOAD_FAIL_ERROR', 'BOOKING_SUCCESS_MESSAGE', 'BOOKING_REQUEST_SUCCESS_MESSAGE']).subscribe((value) => {
      this.bookingErrorString = value['BOOKING_ERROR'];
      this.loadFailErrorString = value['LOAD_FAIL_ERROR'];
      this.bookingSuccessMessage = value ['BOOKING_SUCCESS_MESSAGE'];
      this.bookingRequestSuccessMessage = value ['BOOKING_REQUEST_SUCCESS_MESSAGE'];
    })

    //get input params
    this.section = this.params.get("section");
    this.service = this.params.get("service");
    this.userSelectedRsIDOnly = this.params.get("userSelectedRsIDOnly");
    this.quantity = this.params.get("quantity");
    this.queryDate = this.params.get("queryDate");
    this.validSRsAvailability = this.params.get("validSRsAvailability");
    this.timeslot = this.params.get("timeslot");
    this.serviceBy = this.params.get("serviceBy");
  }

  book() {
    this.spinnerWrapper.show();
    this.bookingService.bookonly(this.timeslot, this.userSelectedRsIDOnly, this.service,
    this.validSRsAvailability, this.queryDate, this.quantity, this.serviceBy, this.section.sectionservicelabel).subscribe((res)=>{
      let gotoBookingId = null;
      if (res && res.success && res.success.id) gotoBookingId = res.success.id;
      let successMessage = null;
      if (this.service.calendarbooking) successMessage = this.bookingSuccessMessage;
      else successMessage = this.bookingRequestSuccessMessage;
      this.appNavService.openSection ({HomePage: HomePage, ListBookingPage: ListBookingPage, ViewBookingDetailsPage: ViewBookingDetailsPage}, this.appConfig.appManageBookingSection, this.navCtrl, gotoBookingId, null, successMessage)
    }, (err)=>{
      this.spinnerWrapper.hide();
      this.errorToaster.toastError(err, this.bookingErrorString);
    })
  }

}
