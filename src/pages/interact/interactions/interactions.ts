import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { AppConfigInfo, InteractService, ErrorToaster, SpinnerWrapper } from '../../../shared/services';
import { TranslateService } from '@ngx-translate/core';
import { InteractionMessagesPage } from '../interaction-messages/interaction-messages'
import { NewInteractionMessagePage } from '../new-interaction-message/new-interaction-message'


@Component({
  selector: 'page-interactions',
  templateUrl: 'interactions.html'
})

export class InteractionsPage {

  public interactions: Array<any> = [];
  public section: any;
  private showEmpty: boolean = false;
  private autoRefreshSubscription: any;
  private emptyErrorString: string = "Empty";
  private loadFailErrorString: string = "Error";
  private delFailErrorString: string = "Failed to Delete";
  private delString: string = "Delete";
  private openString: string = "Open";
  private newInteractionError: string;

  constructor(public navCtrl: NavController,
              public params: NavParams,
              public interactService: InteractService,
              private errorToaster: ErrorToaster,
              public spinnerWrapper: SpinnerWrapper,
              public translateService: TranslateService,
              public appConfig: AppConfigInfo) {
    //get the section, load interactions
    this.section = this.params.get("section");
    //get the empty and error string
    this.translateService.get(['EMPTY_ERROR','OUTGOING_INTERACTION_EMPTY_ERROR','LOAD_FAIL_ERROR','DELETE_FAIL_ERROR', 'DELETE', 'OPEN', 'NEW_INTERACTION_ERROR']).subscribe((value) => {
      this.emptyErrorString = value['EMPTY_ERROR'];
      if (this.section && this.section.initiatedby=='User') {
        this.emptyErrorString = value['OUTGOING_INTERACTION_EMPTY_ERROR'];
      }
      this.loadFailErrorString = value['LOAD_FAIL_ERROR'];
      this.delFailErrorString = value ['DELETE_FAIL_ERROR'];
      this.delString = value ['DELETE'];
      this.openString = value ['OPEN'];
      this.newInteractionError = value ['NEW_INTERACTION_ERROR'];
    })
    //load interactions
    this.interactions = this.params.get("interactions");
    if (!this.interactions || !(this.interactions.length>0)) this.showEmpty = true;
    this.spinnerWrapper.hide()
    //subscribe to auto refresh
    this.autoRefreshSubscription = this.interactService.autoRefreshInteractions (this.section.id, this.interactions);
  }

  //cancel refresh subscription on page unload
  ionViewWillUnload () {
    this.autoRefreshSubscription.unsubscribe();
  }

  doRefresh(refresher) {
    this.interactService.refreshInteractions(this.section.id, this.interactions).subscribe((resp)=>{
      refresher.complete();
    }, (err)=>{
      refresher.complete();
      this.errorToaster.toastError(err, this.loadFailErrorString);
    });
  }

  doInfinite(infiniteScroll) {
    this.interactService.loadMoreInteractions(this.section.id, this.interactions).subscribe((resp)=>{
      infiniteScroll.complete();
      if (resp.success && resp.success.interactions && resp.success.limit
         && resp.success.interactions.length<resp.success.limit) infiniteScroll.enable(false);
    }, (err)=>{
      infiniteScroll.complete();
      this.errorToaster.toastError(err, this.loadFailErrorString);
    });
  }

  deleteInteraction(interaction) {
    this.spinnerWrapper.show();
    this.interactService.deleteInteraction(interaction, this.interactions).subscribe((resp)=>{
      this.spinnerWrapper.hide();
      if (!this.interactions || !(this.interactions.length>0)) this.showEmpty = true;
    }, (err)=>{
      this.spinnerWrapper.hide();
      this.errorToaster.toastError(err, this.delFailErrorString);
    });
  }

  openInteractionMessage (interaction) {
    this.navCtrl.push(InteractionMessagesPage, {section: this.section, interaction: interaction, interactions: this.interactions});
  }

  newInteraction () {
    //check if new Interaction requires a form
    if (this.section.firstmessageformindicator) {
      this.navCtrl.push(NewInteractionMessagePage, {section: this.section, interactions: this.interactions, form: this.section.firstmessageform});
    } else {
      //else just load new Interaction Message page
      this.navCtrl.push(NewInteractionMessagePage, {section: this.section, interactions: this.interactions});
    }
  }

}
