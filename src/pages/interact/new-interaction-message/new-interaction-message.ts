import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { AppConfigInfo, InteractService, ErrorToaster, SpinnerWrapper, CameraService } from '../../../shared/services';
import { TranslateService } from '@ngx-translate/core';
import { LoopBackAuth } from '../../../shared/lbsdk/services';

import { MessageFormsModal } from './message-forms/message-forms'

//moment.js
import * as moment from 'moment-timezone';



@Component({
  selector: 'page-new-interaction-message',
  templateUrl: 'new-interaction-message.html'
})

export class NewInteractionMessagePage {

  public interaction: any = null;
  public interactions: Array<any> = [];
  public messages: Array<any> = [];
  public section: any;
  public messageForms: Array<any> = [];
  public newMessageForm: any = {form: {}}; //creating form as a child of newMessageForm to help modalComponent manipulate the form (pass by reference, can simply replace form in modal. changing form in modal instead of returning selected form to this class will help prevent screen flicker)

  private currentUserId: string;

  private newInteractionInd: boolean = true;
  private simpleMessageInd: boolean = true;

  private requiredErrorString: string;
  private requiredString: string;
  private interactionErrorString: string;
  private messageErrorString: string;
  private loadFailErrorString: string;
  private titleLabelString: string;
  private messageLabelString: string;
  private cannotBeEmpty: string;
  private andString: string;
  private cameraErrorString: string;
  private albumErrorString: string;

  private defaultMaxDate: string = new Date(new Date().setFullYear(new Date().getFullYear() + 100)).toISOString();
  private defaultMinDate: string = new Date(new Date().setFullYear(new Date().getFullYear() - 100)).toISOString();

  constructor(public navCtrl: NavController,
              public params: NavParams,
              public modalCtrl: ModalController,
              public interactService: InteractService,
              private errorToaster: ErrorToaster,
              public spinnerWrapper: SpinnerWrapper,
              public translateService: TranslateService,
              public cameraService: CameraService,
              public loopBackAuth: LoopBackAuth,
              public appConfig: AppConfigInfo) {
                //get the empty and error string
                this.translateService.get(['NEW_INTERACTION_MESSAGE_ERROR','NEW_INTERACTION_ERROR', 'LOAD_FAIL_ERROR',
                'INTERACTION_TITLE_LABEL', 'INTERACTION_MESSAGE_LABEL', 'CANNOT_BE_EMPTY', 'AND', 'REQUIRED',
                'CAMERA_LOAD_ERROR', 'ALBUM_LOAD_ERROR']).subscribe((value) => {
                  this.interactionErrorString = value['NEW_INTERACTION_ERROR'];
                  this.messageErrorString = value['NEW_INTERACTION_MESSAGE_ERROR']
                  this.loadFailErrorString = value['LOAD_FAIL_ERROR'];
                  this.titleLabelString = value ['INTERACTION_TITLE_LABEL'];
                  this.messageLabelString = value ['INTERACTION_MESSAGE_LABEL'];
                  this.cannotBeEmpty = value ['CANNOT_BE_EMPTY'];
                  this.andString = value['AND'];
                  this.requiredString = value['REQUIRED'];
                  this.albumErrorString = value['ALBUM_LOAD_ERROR'];
                  this.cameraErrorString = value['CAMERA_LOAD_ERROR'];
                })
                //get current user Id
                this.currentUserId = this.loopBackAuth.getCurrentUserId();
                //get the section and interaction details for this message
                this.section = this.params.get("section"); //section is needed for creating both a new interaction and message

                //determine if mode is new interaction or new message and set interaction accordingly
                if (this.params.get("interaction")) {
                  this.interaction = this.params.get("interaction");
                  this.messages = this.params.get("messages"); //this is required to refresh messages after creating new message
                  this.newInteractionInd = false;
                } else {
                  this.interactions = this.params.get("interactions"); //this is required to invoke the refreshinteractions method after creating new interaction
                  this.interaction = {title: 'Created on ' + moment(new Date()).format("Do MMM YYYY")} //setting title inside intereaction object. using object instead of simple var as the same could be reused for reply messages.
                  this.newInteractionInd = true;
                }

                //load new message form if present, else load standardMessageFormFields as default
                if (this.params.get("form")) {
                  this.newMessageForm.form = this.params.get("form");
                  this.simpleMessageInd = false;
                }
                else {
                  this.newMessageForm.form.fields = this.interactService.getStandardMessageFormFields();
                  this.simpleMessageInd = true;
                }

                //call initFields
                this.initFields();
              }


  initFields () {
    //now add fieldvalue and fileurl fields to ensure databinding happens while rendering
    this.newMessageForm.form.fields.forEach(field => {
      if (field.fieldtype=='Image') field.fileurl = null
      else field.fieldvalue = null;
    });
  }

  sendMessage () {
    if (this.requiredValidation()) {
      this.spinnerWrapper.show();
      if (this.newInteractionInd) {
        //call new interaction service for creating a new interaction
        this.interactService.createNewInteraction (this.simpleMessageInd, this.section.id, this.interaction.title, this.newMessageForm.form.fields).subscribe((resp)=>{
          //refresh and pop on success
          this.interactService.refreshInteractions(this.section.id, this.interactions).subscribe((resp)=>{
            //pop back
            this.spinnerWrapper.hide();
            this.navCtrl.pop();
          }, (err)=>{
            this.spinnerWrapper.hide();
            this.navCtrl.pop();
          });
        }, (err)=>{
          //remove pics if any on error and stay here
          this.spinnerWrapper.hide();
          this.errorToaster.toastError(err, this.interactionErrorString, this.navCtrl);
        });
      } else {
        //for new messages in existing interaction, call new interaction message service
        //call new interaction service for creating a new interaction
        this.interactService.createNewInteractionMessage (this.simpleMessageInd, this.section.id, this.interaction.id, this.newMessageForm.form.fields).subscribe((resp)=>{
          //refresh and pop on success
          this.interactService.refreshInteractionMesages (this.interaction, this.messages).subscribe((resp)=>{
            //pop back
            this.spinnerWrapper.hide();
            this.navCtrl.pop();
          }, (err)=>{
            this.spinnerWrapper.hide();
            this.navCtrl.pop();
          });
        }, (err)=>{
          //remove pics if any on error and stay here
          this.spinnerWrapper.hide();
          this.errorToaster.toastError(err, this.interactionErrorString, this.navCtrl);
        });
      }
    }
  }

  requiredValidation ():boolean {
    let missingFields: Array<string> = [];
    //check if all required fields are available
    if (this.newInteractionInd && (!this.interaction.title || this.interaction.title=='')) missingFields.push(this.titleLabelString);
    if (this.simpleMessageInd && !this.newMessageForm.form.fields[0].fieldvalue && !this.newMessageForm.form.fields[1].fileurl) missingFields.push(this.messageLabelString);
    if (!this.simpleMessageInd) {
      this.newMessageForm.form.fields.forEach(field => {
        console.log (field);
        if (field.fieldrequired && (!field.fieldvalue || field.fieldvalue=='')) missingFields.push(field.fieldname);
      });
    }
    //now string the strings together to form error string
    if (missingFields.length==0) {
      this.requiredErrorString = null;
      return true;
    } else {
      let err: string = missingFields[0];
      for (let i=1; i<missingFields.length-1; i++) {
        err = err + ", " + missingFields[i];
      }
      if (missingFields.length>1) err = err + " " + this.andString + " " + missingFields[missingFields.length-1];
      err = err + " cannot be empty."
      this.requiredErrorString = err;
      this.errorToaster.toastError(null, this.requiredErrorString);
      return false;
    }
  }

  loadCamera(formfield) {
    this.cameraService.options.sourceType = 1; //sourceType 0=PhotoLibrary, 1=Camera, 2=SavedPhotoAlbum
    this.cameraService.getPicture().then((imageURI) => {
      formfield.fileurl = imageURI;
    }, (err) => {
      this.errorToaster.toastError(err, this.cameraErrorString)
    });
  }

  loadGallery(formfield) {
    this.cameraService.options.sourceType = 0; //sourceType 0=PhotoLibrary, 1=Camera, 2=SavedPhotoAlbum
    this.cameraService.getPicture().then((imageURI) => {
      formfield.fileurl = imageURI;
    }, (err) => {
      this.errorToaster.toastError(err, this.albumErrorString)
    });
  }

}
