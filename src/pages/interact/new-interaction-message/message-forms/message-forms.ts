import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { AppConfigInfo } from '../../../../shared/services';


@Component({
  selector: 'page-message-forms',
  templateUrl: 'message-forms.html'
})

export class MessageFormsModal {

  public messageForms: Array<any> = [];
  public currentForm: any;

  constructor(public params: NavParams,
              public viewCtrl: ViewController,
              public translateService: TranslateService,
              public appConfig: AppConfigInfo) {

                //get the forms list
                this.messageForms = this.params.get("forms");
                this.currentForm = this.params.get("currentForm");
              }

  dismiss(form) {
   if (form) this.currentForm.form = form;
   this.viewCtrl.dismiss({form: form});
  }

}
