import { Component } from '@angular/core';
import { ModalController, NavController, NavParams } from 'ionic-angular';

import { AppConfigInfo, InteractService, ErrorToaster, SpinnerWrapper, AppNavService } from '../../../shared/services';
import { TranslateService } from '@ngx-translate/core';
import { LoopBackAuth } from '../../../shared/lbsdk/services';

import { NewInteractionMessagePage } from '../new-interaction-message/new-interaction-message'
import { ImageModalPage } from './image-modal/image-modal'

import { HomePage } from '../../home/home'
import { InteractionsPage  } from '../../interact/interactions/interactions'
import { GalleryPage  } from '../../photos/gallery/gallery'
import { AlbumsPage  } from '../../photos/albums/albums'
import { SelectServicePage  } from '../../booking/book/select-service/select-service'
import { SearchAvailabilityPage  } from '../../booking/book/search-availability/search-availability'
import { ListBookingPage  } from '../../booking/manage-booking/list-booking/list-booking'
import { ViewBookingDetailsPage } from '../../booking/manage-booking/view-booking-details/view-booking-details'
import { FAQPage } from '../../faq/faq'
import { LocationPage } from '../../location/location'
import { InfoPage } from '../../info/info'
import { ListPage } from '../../list/list'


@Component({
  selector: 'page-interaction-messages',
  templateUrl: 'interaction-messages.html'
})

export class InteractionMessagesPage {

  public messages: Array<any> = [];
  public interactions: Array<any> = [];
  public interaction: any;
  public section: any;

  private unreadCtMsgLoaded: boolean = false;
  private unreadCtViewLoaded: boolean = false;
  private unreadCtNotSet: boolean = true;

  public showEmpty: boolean = false;

  private autoRefreshSubscription: any;
  private emptyErrorString: string = "Empty";
  private loadFailErrorString: string = "Error";
  private delFailErrorString: string = "Failed to Delete";
  private delString: string = "Delete";
  private openString: string = "Open";
  private currentUserId: string;

  constructor(public navCtrl: NavController,
              public params: NavParams,
              public interactService: InteractService,
              public appNavService: AppNavService,
              private errorToaster: ErrorToaster,
              public spinnerWrapper: SpinnerWrapper,
              public translateService: TranslateService,
              public modalCtrl: ModalController,
              public loopBackAuth: LoopBackAuth,
              public appConfig: AppConfigInfo) {
    this.spinnerWrapper.show()
    //get the empty and error string
    this.translateService.get(['EMPTY_ERROR','LOAD_FAIL_ERROR','DELETE_FAIL_ERROR', 'DELETE', 'OPEN']).subscribe((value) => {
      this.emptyErrorString = value['EMPTY_ERROR'];
      this.loadFailErrorString = value['LOAD_FAIL_ERROR'];
      this.delFailErrorString = value ['DELETE_FAIL_ERROR'];
      this.delString = value ['DELETE'];
      this.openString = value ['OPEN'];
    })
    //get current user Id
    this.currentUserId = this.loopBackAuth.getCurrentUserId();
    //get the section, interaction and load interaction messages
    this.section = this.params.get("section");
    this.interaction = this.params.get("interaction");
    this.interactions = this.params.get("interactions");
    this.interactService.loadMoreInteractionMesages (this.interaction, this.messages).subscribe((resp)=>{
      this.spinnerWrapper.hide();
      //update interaction with latest info, needed to update unread count
      if (resp.success && resp.success.interaction) this.updateInteraction (resp.success.interaction);
      if (!this.messages || !(this.messages.length>0)) this.showEmpty = true;
      else {
        //mark as read if both msg loaded and view loaded
        this.unreadCtMsgLoaded = true;
        if (this.unreadCtViewLoaded && this.unreadCtNotSet) {
          this.unreadCtNotSet =false;
          this.interactService.markAsRead (this.section.id, this.interaction, this.messages);
        }
      }
    }, (err)=>{
      this.spinnerWrapper.hide()
      this.errorToaster.toastError(err, this.loadFailErrorString);
    });
    //subscribe to auto refresh
    this.autoRefreshSubscription = this.interactService.autoRefreshInteractionMessages (this.interaction, this.messages);

  }

  //cancel refresh subscription on page unload and refresh interactions
  ionViewWillUnload () {
    this.autoRefreshSubscription.unsubscribe();
    //refresh and pop on success
    this.interactService.refreshInteractions(this.section.id, this.interactions).subscribe((resp)=>{
      //nothing to do..
    }, (err)=>{
      //nothing to do.. not a critical error..
    });
  }

  ionViewDidEnter () {
    //mark as read if both msg loaded and view loaded
    this.unreadCtViewLoaded = true;
    if (this.unreadCtMsgLoaded && this.unreadCtNotSet) {
      this.unreadCtNotSet =false;
      this.interactService.markAsRead (this.section.id, this.interaction, this.messages);
    }
  }

  updateInteraction (newInteraction) {
    //by default get latest unreadcount, interactionusertrackers and lastmessagepreview - these dont have an impact on interactions page position or bookmarkdate
    this.interaction.myunreadcount = newInteraction.myunreadcount;
    this.interaction.interactionusertrackers = newInteraction.interactionusertrackers;
    this.interaction.lastmessagepreview = newInteraction.lastmessagepreview;

    //now check if last message date has changed to be lesser than what is currently known (i.e. known last message has been deleted)
    if (this.interaction.lastmessagedate && newInteraction.lastmessagedate && this.interaction.lastmessagedate > newInteraction.lastmessagedate) {
      //now check if the new last message date puts it out of bounds of current array
      //if yes, loadmore will fetch it at some point and we can slice it now
      //if not, i.e. new lastmessagedate will just move the interaction within existing array then do so.
      if (newInteraction.lastmessagedate >= this.interactions[this.interactions.length-1].lastmessagedate) {
        //copy new Interaction to existing interaction
        for (let key in this.interaction) {
          this.interaction[key] = newInteraction[key];
        }
        //sort array
        this.interactions.sort((a,b)=>{
          if (a.lastmessagedate > b.lastmessagedate) return -1;
          else return 1;
        })
      } else {
        //get index of this interaction and splice it
        for (let i=0; i<this.interactions.length; i++) {
          if (this.interactions[i].id == this.interaction.id) {
            this.interactions.splice(i,1);
            break;
          }
        }
      }
    }
  }

  doRefresh(refresher) {
    if (!refresher) this.spinnerWrapper.show();
    this.interactService.refreshInteractionMesages(this.interaction, this.messages).subscribe((resp)=>{
      if (refresher) refresher.complete()
      else this.spinnerWrapper.hide();
      //update interaction with latest info, needed to update unread count
      if (resp.success && resp.success.interaction) this.updateInteraction (resp.success.interaction);
      this.interactService.markAsRead (this.section.id, this.interaction, this.messages);
    }, (err)=>{
      if (refresher) refresher.complete()
      else this.spinnerWrapper.hide();
      this.errorToaster.toastError(err, this.loadFailErrorString);
    });
  }

  doInfinite(infiniteScroll) {
    this.interactService.loadMoreInteractionMesages(this.interaction, this.messages).subscribe((resp)=>{
      infiniteScroll.complete();
      if (resp.success && resp.success.interactionmessages && resp.success.limit
         && resp.success.interactionmessages.length<resp.success.limit) infiniteScroll.enable(false);
    }, (err)=>{
      infiniteScroll.complete();
      this.errorToaster.toastError(err, this.loadFailErrorString);
    });
  }

  deleteInteractionMessage(message) {
    this.spinnerWrapper.show();
    this.interactService.deleteInteractionMessage(this.interaction, message, this.messages).subscribe((resp)=>{
      this.spinnerWrapper.hide();
      if (!this.messages || !(this.messages.length>0)) this.showEmpty = true;
    }, (err)=>{
      this.spinnerWrapper.hide();
      this.errorToaster.toastError(err, this.delFailErrorString);
    });
  }

  newMessage () {
    this.navCtrl.push(NewInteractionMessagePage, {section: this.section, interaction: this.interaction, messages: this.messages});
  }

  openImage (field) {
    let modal = this.modalCtrl.create(ImageModalPage, {field: field});
    modal.present();
  }

  pushGoToTarget (message) {
    let targetSection = null;
    message.forEach ((messagefield)=>{
      if (messagefield.fieldname == 'apptargetsectionid') {
        this.appConfig.appAllSections.forEach((appsection)=>{
          if (appsection.id == messagefield.fieldvalue) targetSection = appsection;
        })
      }
    })
    if (targetSection) {
      this.spinnerWrapper.show()
      if (targetSection.module=="Interact" || targetSection.module=="Push") this.appNavService.openSection ({HomePage: HomePage, InteractionsPage: InteractionsPage}, targetSection, this.navCtrl, null, true);
      else if (targetSection.module=="Photos") this.appNavService.openSection ({HomePage: HomePage, GalleryPage: GalleryPage, AlbumsPage: AlbumsPage}, targetSection, this.navCtrl, null, true);
      else if (targetSection.module=="Booking") this.appNavService.openSection ({HomePage: HomePage, SelectServicePage: SelectServicePage, SearchAvailabilityPage: SearchAvailabilityPage}, targetSection, this.navCtrl, null, true);
      else if (targetSection.module=="Manage Booking") this.appNavService.openSection ({HomePage: HomePage, ListBookingPage: ListBookingPage, ViewBookingDetailsPage: ViewBookingDetailsPage}, targetSection, this.navCtrl, null, true);
      else if (targetSection.module=="FAQ") this.appNavService.openSection ({HomePage: HomePage, FAQPage: FAQPage}, targetSection, this.navCtrl, null, true);
      else if (targetSection.module=="Location") this.appNavService.openSection ({HomePage: HomePage, LocationPage: LocationPage}, targetSection, this.navCtrl, null, true);
      else if (targetSection.module=="Info") this.appNavService.openSection ({HomePage: HomePage, InfoPage: InfoPage}, targetSection, this.navCtrl, null, true);
      else if (targetSection.module=="List") this.appNavService.openSection ({HomePage: HomePage, ListPage: ListPage}, targetSection, this.navCtrl, null, true);
    }
  }

}
