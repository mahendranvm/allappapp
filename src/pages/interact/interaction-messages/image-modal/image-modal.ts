import { Component } from '@angular/core';
import { ViewController, NavParams, ActionSheetController, Platform } from 'ionic-angular';

import { FileService, ErrorToaster, SpinnerWrapper } from '../../../../shared/services';
import { TranslateService } from '@ngx-translate/core';




@Component({
  selector: 'page-image-modal',
  templateUrl: 'image-modal.html'
})

export class ImageModalPage {

  private field: any = {};
  private saveErrorString: string = null;
  private shareErrorString: string = null;

  constructor(public viewCtrl: ViewController,
              public params: NavParams,
              public actionSheetCtrl: ActionSheetController,
              public platform: Platform,
              private fileService: FileService,
              private errorToaster: ErrorToaster,
              public spinnerWrapper: SpinnerWrapper,
              public translateService: TranslateService) {

    //get the empty and error string
    this.translateService.get(['SAVE_FAILED', 'SOCIAL_SHARE_FAILED']).subscribe((value) => {
      this.saveErrorString = value['SAVE_FAILED'];
      this.shareErrorString = value['SOCIAL_SHARE_FAILED'];
    })

    this.field = this.params.get('field');

  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  openMenu() {

     let actionSheet = this.actionSheetCtrl.create({
       title: 'Options',
       buttons: [
         {
           text: 'Save',
           handler: () => {this.save();},
           icon: !this.platform.is('ios') ? 'download': null
         },
         {
           text: 'Share',
           handler: () => {this.share();},
           icon: !this.platform.is('ios') ? 'share': null
         },
         {
           text: 'Cancel',
           role: 'cancel',
           icon: !this.platform.is('ios') ? 'close': null
         }
       ]
     });

     actionSheet.present();
  }

  save () {
    this.spinnerWrapper.show();
    this.fileService.savePicToGallery(this.field.fieldurl, this.field.fieldvalue)
    .then (()=>{
      this.spinnerWrapper.hide()
    })
    .catch ((err)=>{
      this.spinnerWrapper.hide()
      this.errorToaster.toastError(err, this.saveErrorString);
    })
  }

  share () {
    this.spinnerWrapper.show();
    this.fileService.downloadToCache (this.field.fieldurl, this.field.fieldvalue)
    .then ((fileEntry)=>{
      this.spinnerWrapper.hide();
      this.fileService.socialShareFromCache (fileEntry.toURL())
      .catch((err)=>{
          this.errorToaster.toastError(err, this.shareErrorString);
      })
    }, (err)=>{
      //using then reject handler instead of catch, as spinner should be hidden only if download fails
      //using catch, we will not be able to distinguish where error originated from
      this.spinnerWrapper.hide();
      this.errorToaster.toastError(err, this.shareErrorString);
    })
  }

}
