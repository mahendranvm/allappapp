import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { AppConfigInfo, AppNavService, SpinnerWrapper } from '../../shared/services';

import { InteractionsPage  } from '../interact/interactions/interactions'
import { GalleryPage  } from '../photos/gallery/gallery'
import { AlbumsPage  } from '../photos/albums/albums'
import { SelectServicePage  } from '../booking/book/select-service/select-service'
import { SearchAvailabilityPage  } from '../booking/book/search-availability/search-availability'
import { ListBookingPage  } from '../booking/manage-booking/list-booking/list-booking'
import { ViewBookingDetailsPage } from '../booking/manage-booking/view-booking-details/view-booking-details'
import { FAQPage } from '../faq/faq'
import { LocationPage } from '../location/location'
import { InfoPage } from '../info/info'
import { ListPage } from '../list/list'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  constructor(public navCtrl: NavController,
              public appNavService: AppNavService,
              public spinnerWrapper: SpinnerWrapper,
              public appConfig: AppConfigInfo) {


  }

  ionViewWillEnter () {
    //refresh badge info whenever home screen is loaded
    this.appConfig.subscribeAutoRefreshBadgeInfo();
  }

  ionViewWillLeave () {
    //unsubscribe refresh badge info whenever home screen is loaded
    this.appConfig.unsubscribeAutoRefreshBadgeInfo();
  }

  openPage (section) {
    if (section) {
      this.spinnerWrapper.show()
      if (section.module=="Interact" || section.module=="Push") this.appNavService.openSection ({InteractionsPage: InteractionsPage}, section, this.navCtrl, null);
      else if (section.module=="Photos") this.appNavService.openSection ({GalleryPage: GalleryPage, AlbumsPage: AlbumsPage}, section, this.navCtrl, null);
      else if (section.module=="Booking") this.appNavService.openSection ({SelectServicePage: SelectServicePage, SearchAvailabilityPage: SearchAvailabilityPage}, section, this.navCtrl, null);
      else if (section.module=="Manage Booking") this.appNavService.openSection ({ListBookingPage: ListBookingPage, ViewBookingDetailsPage: ViewBookingDetailsPage}, section, this.navCtrl, null);
      else if (section.module=="FAQ") this.appNavService.openSection({FAQPage: FAQPage}, section, this.navCtrl, null);
      else if (section.module=="Location") this.appNavService.openSection({LocationPage: LocationPage}, section, this.navCtrl, null);
      else if (section.module=="Info") this.appNavService.openSection({InfoPage: InfoPage}, section, this.navCtrl, null);
      else if (section.module=="List") this.appNavService.openSection({ListPage: ListPage}, section, this.navCtrl, null);
    }
  }


}
