import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { AppConfigInfo, SpinnerWrapper } from '../../shared/services';
import { TranslateService } from '@ngx-translate/core';
import { LaunchNavigator } from '@ionic-native/launch-navigator';

@Component({
  selector: 'page-location',
  templateUrl: 'location.html'
})

export class LocationPage {

  public locations: Array<any> = [];
  public section: any;
  private showEmpty: boolean = false;
  private emptyErrorString: string = "Empty";

  constructor(public navCtrl: NavController,
              public params: NavParams,
              private spinnerWrapper: SpinnerWrapper,
              public translateService: TranslateService,
              public launchNavigator: LaunchNavigator,
              public appConfig: AppConfigInfo) {
    //get the empty and error string
    this.translateService.get(['EMPTY_ERROR']).subscribe((value) => {
      this.emptyErrorString = value['EMPTY_ERROR'];
    })

    //get the section and albums list
    this.section = this.params.get("section");
    this.locations = this.params.get("locations");
    if (!this.locations || !(this.locations.length>0)) this.showEmpty = true;
    this.spinnerWrapper.hide();

  }

  navigateto (loc) {
    let address = "";
    if (loc.address1) address = address + loc.address1 + ", "
    if (loc.address2) address = address + loc.address2 + ", "
    if (loc.city) address = address + loc.city + ", "
    if (loc.state) address = address + loc.state + ", "
    if (loc.zip) address = address + loc.zip + ", "
    if (loc.country) address = address + loc.country

    this.launchNavigator.navigate(address)
    .then(
      success => console.log('Launched navigator'),
      error => console.log('Error launching navigator', error)
    );
  }


}
